package com.fabriciooliveira.lojavirtual.comum;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class Constantes {

    public static final String DB_CONFIG_PROPS = "database_config";

    public static final String DB_CONFIG_NOME = "config.database.nome";

    public static final String DB_CONFIG_VERSAO = "config.database.versao";
}
