package com.fabriciooliveira.lojavirtual.bo;

import android.content.Context;

import com.fabriciooliveira.lojavirtual.dominio.ValidacaoMock;
import com.fabriciooliveira.lojavirtual.dto.PessoaDTO;
import com.fabriciooliveira.lojavirtual.sqlite.MockOpenHelper;

import java.util.List;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class MockBO {

    private MockOpenHelper mockOpenHelper;

    public MockBO(Context context) {
        mockOpenHelper = new MockOpenHelper(context);
    }

    public ValidacaoMock cadastrarPessoa(PessoaDTO pessoaDTO){
        ValidacaoMock retorno = new ValidacaoMock();

        mockOpenHelper.cadastrar(pessoaDTO);
        retorno.setValido(true);
        retorno.setMensagem("Cadastro de pessoa efetuado com sucesso");

        return retorno;
    }

    public List<PessoaDTO> listarPessoas(){
        return mockOpenHelper.listar();
    }

    public PessoaDTO consultarPessoaPorId(Integer idPessoa){
        return mockOpenHelper.consultarPessoaPorID(idPessoa);
    }

    public void removerPessoaPorId(Integer idPessoa){
        mockOpenHelper.removerPessoaPorId(idPessoa);
    }

    public void atualizarPessoa(PessoaDTO pessoaDTO){
        mockOpenHelper.atualizarPessoaPorID(pessoaDTO);
    }
}
