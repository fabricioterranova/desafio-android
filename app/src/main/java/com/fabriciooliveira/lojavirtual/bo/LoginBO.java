package com.fabriciooliveira.lojavirtual.bo;

import android.content.Context;

import com.fabriciooliveira.lojavirtual.R;
import com.fabriciooliveira.lojavirtual.custom.model.Usuario;
import com.fabriciooliveira.lojavirtual.dominio.ValidacaoLogin;
import com.fabriciooliveira.lojavirtual.util.WebServiceUtil;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class LoginBO {

    private Context context;

    //private LoginOpenHelper loginOpenHelper;

    public LoginBO(Context context) {
        this.context = context;
        //loginOpenHelper = new LoginOpenHelper(context);
    }

    public ValidacaoLogin validarLogin(String login, String senha){
        ValidacaoLogin retorno = new ValidacaoLogin();
        Usuario usuario = null;

        if(login == null || login.equals("")){
            retorno.setValido(false);
            retorno.setMensagem(context.getString(R.string.msg_login_obrigatorio));
        }else if(senha == null || senha.equals("")){
            retorno.setValido(false);
            retorno.setMensagem(context.getString(R.string.msg_senha_obrigatoria));
        }else if((usuario = WebServiceUtil.validarLoginRest(login, senha)) != null &&
                 usuario.isLogado()){
            retorno.setValido(true);
            retorno.setMensagem(context.getString(R.string.msg_login_sucesso));
        }else{
            retorno.setValido(false);
            retorno.setMensagem(context.getString(R.string.msg_login_invalido));
        }
        retorno.setUsuario(usuario);

        return retorno;
    }
}
