package com.fabriciooliveira.lojavirtual;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.fabriciooliveira.lojavirtual.bo.MockBO;
import com.fabriciooliveira.lojavirtual.comum.Profissao;
import com.fabriciooliveira.lojavirtual.dto.PessoaDTO;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/10/15.
 */
public class MockListActivity extends ActionBarActivity {

    private MockBO mockBO;
    private List<PessoaDTO> lista;
    private ListView lstPessoas;
    private int posicao;

    public int getPosicao() {
        return posicao;
    }

    public void setPosicao(int posicao) {
        this.posicao = posicao;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_mock);

        mockBO = new MockBO(this);

        listarPessoas();
        consultarPorId();

    }

    public void novoCadastro(View view){
        Intent intent = new Intent(MockListActivity.this, MockActivity.class);
        startActivity(intent);
        finish();
    }

    private void listarPessoas(){

        lstPessoas = (ListView) findViewById(R.id.lst_pessoas);

        lista = mockBO.listarPessoas();

        List<CharSequence> valores = new ArrayList<CharSequence>();

        for(PessoaDTO pessoaDTO : lista){
            valores.add(pessoaDTO.getNome());
        }

        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_list_item_1, valores);
        lstPessoas.setAdapter(adapter);

        lstPessoas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long id) {
                setPosicao(position);
                return false;
            }
        });

        lstPessoas.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.add(0,1,1,"Editar");
                menu.add(0,2,2,"Deletar");
            }
        });
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case 1:
                PessoaDTO pessoaDTO = lista.get(posicao);
                Intent i = new Intent(MockListActivity.this, MockEditActivity.class);
                i.putExtra("pessoa", pessoaDTO);
                startActivity(i);
                finish();
                break;
            case 2:
                MensagemUtil.addMsgConfirm(this, "Alerta", "Deseja realmente remover essa pessoa?", R.drawable.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PessoaDTO pessoaDTO = lista.get(posicao);
                        mockBO.removerPessoaPorId(pessoaDTO.getIdPessoa());
                        MensagemUtil.addMsg(MockListActivity.this, "Pessoa removida com sucesso");
                        Intent i = new Intent(MockListActivity.this, MockListActivity.class);
                        startActivity(i);
                        finish();
                    }
                });


                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
          //menu.add(0, 1, 1, "Lista");
//        menu.add(0, 2, 2, "info");
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void consultarPorId(){
        lstPessoas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PessoaDTO pessoa = lista.get(position);
                PessoaDTO pessoaDTO = mockBO.consultarPessoaPorId(pessoa.getIdPessoa());

                String msg = "Nome: " + pessoaDTO.getNome() +
                        "\nEndereço: " + pessoaDTO.getEndereco() +
                        "\nCPF: " + pessoaDTO.getCpf() +
                        "\nSexo: " + ((pessoaDTO.getSexo() == 'M') ? "MASCULINO" : "FEMININO");

                String profissao = null;
                        if(pessoaDTO.getProfissao() != 0){
                            profissao = Profissao.getProfissao(pessoaDTO.getProfissao()).getDescricao();
                        }
                msg += "\nProfissao: " + (profissao != null ?  profissao : "Codigo errado!");


                MensagemUtil.addMsgOk(MockListActivity.this, "Info", msg, R.drawable.about);
            }
        });
    }
}
