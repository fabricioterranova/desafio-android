package com.fabriciooliveira.lojavirtual.custom;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class ItemDash {

    private int idImg;
    private String titulo;

    public ItemDash(int idImg, String titulo) {
        this.idImg = idImg;
        this.titulo = titulo;
    }

    public void setIdImg(int idImg) {
        this.idImg = idImg;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIdImg() {
        return idImg;
    }

    public String getTitulo() {
        return titulo;
    }
}
