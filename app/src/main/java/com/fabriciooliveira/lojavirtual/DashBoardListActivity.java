package com.fabriciooliveira.lojavirtual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.fabriciooliveira.lojavirtual.custom.AdapterListViewCustom;
import com.fabriciooliveira.lojavirtual.custom.ItemDash;
import com.fabriciooliveira.lojavirtual.dominio.DashBoardListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class DashBoardListActivity extends ActionBarActivity {

    private ListView lstDash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard_list);

        lstDash = (ListView)findViewById(R.id.lstDash);

        //FORMA PELO JAVA
//        List<CharSequence> lista = new ArrayList<CharSequence>();
//        lista.add("Consultar");
//        lista.add("Pesquisa");
//        lista.add("Perfil");
//
//
//        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(DashBoardListActivity.this, android.R.layout.simple_list_item_1, lista);
//        lstDash.setAdapter(adapter);



        //FORMA PELO XML
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.array_opcoes_menu, android.R.layout.simple_list_item_1);
//        lstDash.setAdapter(adapter);


        List<ItemDash> listaItems = new ArrayList<ItemDash>();

        for(DashBoardListItem boardListItem : DashBoardListItem.values()){
            ItemDash itemDash = new ItemDash(boardListItem.getIdImg(), boardListItem.getTitulo());
            listaItems.add(itemDash);
        }

        AdapterListViewCustom adapter = new AdapterListViewCustom(this, R.layout.item_listview_dash, listaItems);
        lstDash.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
          menu.add(0, 1, 1, "Em quadros");
//        menu.add(0, 2, 2, "info");
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        switch(id){
            case 1:
                Intent intent = new Intent(DashBoardListActivity.this, DashBoardActivity.class);
                startActivity(intent);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
