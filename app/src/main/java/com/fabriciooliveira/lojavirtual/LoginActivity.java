package com.fabriciooliveira.lojavirtual;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fabriciooliveira.lojavirtual.bo.LoginBO;
import com.fabriciooliveira.lojavirtual.dominio.ValidacaoLogin;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;


public class LoginActivity extends ActionBarActivity {

    private EditText edtLogin;
    private EditText edtSenha;
    private Button btnLogar;
    private Button btnLoginFacebook;

    private LoginBO loginBO;
    private static String APP_ID = "989455341083061";
    private Facebook facebook = new Facebook(APP_ID);
    private SharedPreferences preferences;

    private AsyncFacebookRunner facebookRunner = new AsyncFacebookRunner(facebook);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtLogin = (EditText)findViewById(R.id.edt_login);
        edtSenha = (EditText)findViewById(R.id.edt_senha);
        btnLogar = (Button)findViewById(R.id.btn_logar);

        btnLoginFacebook = (Button)findViewById(R.id.btn_fbLogin);

        //Verificando se token ja existe para levar direto para Dashboard
        preferences = getPreferences(MODE_PRIVATE);
        String token = preferences.getString("access_token", null);
        long expires = preferences.getLong("access_expires", 0);
        int usuario = preferences.getInt("usuario_id", 0);

        if((token != null && expires != 0) || usuario != 0){
            Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
            startActivity(i);
            finish();
        }


    }

    public void loginFacebook(View view){
        preferences = getPreferences(MODE_PRIVATE);

        String token = preferences.getString("access_token", null);
        long expires = preferences.getLong("access_expires", 0);

        if(token != null){
            facebook.setAccessToken(token);

            btnLoginFacebook.setVisibility(View.INVISIBLE);
            Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
            startActivity(i);
            finish();
        }

        if(expires != 0){
            facebook.setAccessExpires(expires);
        }

        if(!facebook.isSessionValid()){
            facebook.authorize(LoginActivity.this, new String[]{"email", "publish_stream"}, new Facebook.DialogListener() {
                @Override
                public void onComplete(Bundle bundle) {
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("access_token", facebook.getAccessToken());
                    editor.putLong("access_expires", facebook.getAccessExpires());
                    editor.commit();

                    btnLoginFacebook.setVisibility(View.INVISIBLE);
                    Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                    startActivity(i);
                    finish();
                }

                @Override
                public void onFacebookError(FacebookError facebookError) {
                    Log.e("ERRO==>> ", facebookError.toString());
                }

                @Override
                public void onError(DialogError dialogError) {
                    Log.e("ERRO==>> ", dialogError.toString());
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    public void verPerfil(View view){
        facebookRunner.request("me", new AsyncFacebookRunner.RequestListener() {
            @Override
            public void onComplete(String s, Object o) {
                Log.d("Informacoes do perfil", s);

                try{
                    JSONObject perfil = new JSONObject(s);

                    final String nome = perfil.getString("name");
                    final String email = perfil.getString("email");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String msg = "Nome: " + nome + "\nEmail: " + email;

                            MensagemUtil.addMsg(LoginActivity.this, msg);
                        }
                    });
                }catch(JSONException e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onIOException(IOException e, Object o) {

            }

            @Override
            public void onFileNotFoundException(FileNotFoundException e, Object o) {

            }

            @Override
            public void onMalformedURLException(MalformedURLException e, Object o) {

            }

            @Override
            public void onFacebookError(FacebookError facebookError, Object o) {
                Log.e("ERRO==>> ", facebookError.toString());
            }
        });
    }

    public void logar(View view){
        new LoadingAsync().execute();
    }

    private class LoadingAsync extends AsyncTask<Void, Void, ValidacaoLogin>{

        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute(){
            //super.onPreExecute();

            progressDialog.setMessage("Carregando...");
            progressDialog.show();

        }

        @Override
        protected ValidacaoLogin doInBackground(Void...params){

            String login = edtLogin.getText().toString();
            String senha = edtSenha.getText().toString();

            loginBO = new LoginBO(LoginActivity.this);

            return loginBO.validarLogin(login, senha);

        }

        @Override
        protected void onPostExecute(ValidacaoLogin validacao){
            //super.onPostExecute(validacao);
            progressDialog.dismiss();

            if(validacao.isValido()){
                preferences = getPreferences(MODE_PRIVATE);

                if(validacao.getUsuario() != null){
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("usuario_id", validacao.getUsuario().getId());
                    editor.commit();
                }

                Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                i.putExtra("msg", validacao.getMensagem());
                startActivity(i);
                finish();
            }else{
                MensagemUtil.addMsg(LoginActivity.this, validacao.getMensagem());
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menu.add(1, 1, 1, "Sair");
//        menu.add(0, 2, 2, "info");
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        switch (id){
            case R.id.menuSair:
                MensagemUtil.addMsgConfirm(LoginActivity.this, getString(R.string.lbl_sair), getString(R.string.msg_logout), R.drawable.logout, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                break;
            case R.id.menuSobre:
                //MensagemUtil.addMsg(LoginActivity.this, "Loja Virtual DevMedia 1.0 - Copyright");
                MensagemUtil.addMsgOk(LoginActivity.this, getString(R.string.lbl_sobre),
                        getString(R.string.lbl_desc_sobre), R.drawable.about);
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
