package com.fabriciooliveira.lojavirtual;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.fabriciooliveira.lojavirtual.custom.UTF8ParseJson;
import com.fabriciooliveira.lojavirtual.custom.adapter.CustomListAdapter;
import com.fabriciooliveira.lojavirtual.custom.adapter.ListAdapterListenerProduto;
import com.fabriciooliveira.lojavirtual.custom.app.AppController;
import com.fabriciooliveira.lojavirtual.custom.model.Produto;
import com.fabriciooliveira.lojavirtual.paypal.PayPalConfig;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by fabriciooliveira on 2/10/15.
 */
public class ListaProdutoActivity extends ActionBarActivity implements SearchView.OnQueryTextListener, ListAdapterListenerProduto {

    private static final String TAG = ListaProdutoActivity.class.getSimpleName();

//    private static final String URL_JSON = "https://raw.githubusercontent.com/DiogoSouza/devmedia/master/produtos.json";
////    private static final String URL_JSON2 = "http://192.168.3.101:9306/LoginRestful/produto/produtos";
//    private static final String URL_JSON2 = "http://192.168.3.101:8080/LoginRestful/produto/produtos";


    private SearchView searchView;
    private ListView lstProd;
    private CustomListAdapter adapter;
    private List<Produto> produtos = new ArrayList<Produto>();

    private List<PayPalItem> carrinho = new ArrayList<PayPalItem>();

    private ProgressDialog progressDialog;

    private Button btnCheckout;

    private static final int CODIGO_PAGTO = 1;

    private static PayPalConfiguration payPalConfig = new PayPalConfiguration()
            .environment(PayPalConfig.PAYPAL_ENVIRONMENT)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.listagem_produtos);

        lstProd = (ListView) findViewById(R.id.listaProd);
        adapter = new CustomListAdapter(this, produtos, this);

        btnCheckout = (Button) findViewById(R.id.btnCheckout);

        lstProd.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Carregando...");
        progressDialog.show();

        //getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1b1b1b")));

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, payPalConfig);
        startService(intent);

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(carrinho.isEmpty()){
                    MensagemUtil.addMsg(ListaProdutoActivity.this, "Carrinho vazio! Favor add produtos!");
                }else{
                    executarPagtoPayPal();
                }
            }
        });

        executarRequestProdutos(null);

    }


    private void executarRequestProdutos(String param) {
        produtos.clear();

        //evitar espaços em branco na url
        if(param != null){
            try {
                param = URLEncoder.encode(param, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        JsonArrayRequest requestProds = new UTF8ParseJson(param != null ? PayPalConfig.URL_PRODUTOS + "?str="+param : PayPalConfig.URL_PRODUTOS, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray jsonArray) {
                esconderDialog();

                for(int i = 0; i<jsonArray.length(); i++){
                    try{
                        JSONObject jsonObj = jsonArray.getJSONObject(i);

                        Gson gson = new Gson();
                        Produto produto = gson.fromJson(jsonObj.toString(), Produto.class);

                        produtos.add(produto);

                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                }

                adapter.notifyDataSetChanged();

            }
        },

        new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Erro: " + error.getMessage());
                esconderDialog();
            }

        });

        AppController.getInstance().addToRequestQueue(requestProds);
    }

    private void esconderDialog(){
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        esconderDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.produtos, menu);

        MenuItem item = menu.findItem(R.id.pesquisaProdutos);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setIconifiedByDefault(true);

        configurarWidgetSearch();

        return true;
    }

    private void configurarWidgetSearch(){

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if(manager != null){
            List<SearchableInfo> global = manager.getSearchablesInGlobalSearch();

            SearchableInfo info = manager.getSearchableInfo(getComponentName());
            for(SearchableInfo i : global){
                if(i.getSuggestAuthority() != null && i.getSuggestAuthority().startsWith("applications")){
                    info = i;
                }
            }

            searchView.setSearchableInfo(info);
        }
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if(s != null){
            executarRequestProdutos(s);
        }
        return false;
    }

    private void verificarPagtoServidor(final String idPagto, final String jsonClientePagto){
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Verificando pagamento...");
        progressDialog.show();

        StringRequest verificaReq = new StringRequest(Request.Method.POST, PayPalConfig.URL_VERIFICA_PAGTO, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                JSONObject res = null;

                try {
                    res = new JSONObject(response);
                    boolean erro = res.getBoolean("erro");
                    String msg = res.getString("msg");

                    MensagemUtil.addMsg(ListaProdutoActivity.this, msg);

                    if(!erro){
                        carrinho.clear();
                    }
                    esconderDialog();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                MensagemUtil.addMsg(ListaProdutoActivity.this, error.getMessage());
                esconderDialog();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<String,String>();

                SharedPreferences pref = getPreferences(MODE_PRIVATE);
                int idUsuario = pref.getInt("usuario_id", 0);

                params.put("idPagto", idPagto);
                params.put("jsonClientePagto", jsonClientePagto);
                params.put("idUsuario", idUsuario != 0 ? String.valueOf(idUsuario) : "1");

                return params;
            }
        };

        //1 minuto
        int socketTimeOut = 60000;

        RetryPolicy policy = new DefaultRetryPolicy(socketTimeOut
                , DefaultRetryPolicy.DEFAULT_MAX_RETRIES
                , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        verificaReq.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(verificaReq);

    }


    @Override
    public void onAddCarrinhoPressed(Produto produto) {
        PayPalItem item = new PayPalItem(produto.getTitulo()
                , 1
                , new BigDecimal(produto.getValor()).setScale(2, RoundingMode.CEILING)
                , PayPalConfig.MOEDA_PADRAO
                , produto.getSku());

        carrinho.add(item);

        MensagemUtil.addMsg(ListaProdutoActivity.this, item.getName() + " adicionado ao carrinho!");

    }


    private PayPalPayment prepararCarrinhoFinal(){
        PayPalItem[] itens = new PayPalItem[carrinho.size()];
        itens = carrinho.toArray(itens);

        //Quantia total
        BigDecimal total = PayPalItem.getItemTotal(itens);

        //Frete
        BigDecimal frete = new BigDecimal("0.0");

        //Frete
        BigDecimal taxa = new BigDecimal("0.0");

        PayPalPaymentDetails detalhesPagto = new PayPalPaymentDetails(frete, total, taxa);

        BigDecimal quantia = total.add(taxa).add(frete);

        PayPalPayment pagto = new PayPalPayment(quantia, PayPalConfig.MOEDA_PADRAO, "Loja Virtual Devmedia", PayPalConfig.PAYMENT_INTENT);

        pagto.items(itens).paymentDetails(detalhesPagto);

        pagto.custom("Loja Virtual DevMedia");


        return pagto;
    }


    private void executarPagtoPayPal(){
        PayPalPayment coisasAComprar = prepararCarrinhoFinal();

        Intent intent = new Intent(ListaProdutoActivity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, payPalConfig);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, coisasAComprar);

        startActivityForResult(intent, CODIGO_PAGTO);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CODIGO_PAGTO){
            if(resultCode == Activity.RESULT_OK){
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if(confirm != null){
                    try {
                        String pagtoID = confirm.toJSONObject().getJSONObject("response").getString("id");

                        String jsonClientePagto = confirm.getPayment().toJSONObject().toString();

                        verificarPagtoServidor(pagtoID, jsonClientePagto);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
