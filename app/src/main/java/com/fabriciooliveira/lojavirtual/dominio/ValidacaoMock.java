package com.fabriciooliveira.lojavirtual.dominio;

/**
 * Created by fabriciooliveira on 2/12/15.
 */
public class ValidacaoMock {

    private boolean valido;
    private String mensagem;

    public boolean isValido() {
        return valido;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

}
