package com.fabriciooliveira.lojavirtual.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class LoginOpenHelper extends SQLiteOpenHelper{

    //cria aqui pois nao deve ser recuperado antes do construtor
    //private static ResourceBundle config = ResourceBundle.getBundle(Constantes.DB_CONFIG_PROPS, Locale.getDefault());

    public LoginOpenHelper(Context context){
        //Context context, String name, CursorFactory factory, int version
//        super(context, config.getString(Constantes.DB_CONFIG_NOME),
//                null, Integer.parseInt(config.getString(Constantes.DB_CONFIG_VERSAO)));
        super(context, "db_lojavirtual", null, 5);

    }
    //Ser'a chamado somente uma vez
    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE TB_USUARIO (");
        sql.append(" ID_USUARIO INTEGER PRIMARY KEY AUTOINCREMENT,");
        sql.append(" LOGIN TEXT NOT NULL,");
        sql.append(" SENHA TEXT NOT NULL)");

        db.execSQL(sql.toString());
        mockPopulaUsuarios(db);
        //db.close();

        sql = new StringBuilder();
        sql.append("CREATE TABLE TB_PESSOA (");
        sql.append(" ID_PESSOA INTEGER PRIMARY KEY AUTOINCREMENT,");
        sql.append(" NOME TEXT NOT NULL,");
        sql.append(" ENDERECO TEXT NOT NULL,");
        sql.append(" CPF TEXT NOT NULL,");
        sql.append(" PROFISSAO INT NOT NULL,");
        sql.append(" SEXO CHAR NOT NULL)");

        db.execSQL(sql.toString());
        //db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public boolean validarLogin(String usuario, String senha){
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query("TB_USUARIO", null, "LOGIN=? AND SENHA=?", new String[]{usuario, senha}, null, null, null);

        if(cursor.moveToFirst()){
            return true;
        }

        return false;
    }

    private void mockPopulaUsuarios(SQLiteDatabase db){
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO TB_USUARIO (LOGIN, SENHA) VALUES ('diogo','123')");
        db.execSQL(sql.toString());

        ContentValues values = new ContentValues();
        values.put("LOGIN", "admin");
        values.put("SENHA", "admin");

        db.insert("TB_USUARIO", null, values);
    }


}
