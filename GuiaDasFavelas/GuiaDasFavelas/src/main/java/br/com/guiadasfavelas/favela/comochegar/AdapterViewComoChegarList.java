package br.com.guiadasfavelas.favela.comochegar;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.componentes.OptimizedAdapterView;
import br.com.guiadasfavelas.entities.Acesso;
import br.com.guiadasfavelas.util.FontUtil;

//testeteste
public class AdapterViewComoChegarList extends OptimizedAdapterView {

	public AdapterViewComoChegarList(Context context, List<Acesso> itens) {
		
		super(context, itens);
	}

	

	public View getView(int position, View view) {

		view = hash.get(position);
		
		if(view == null) {
		
			// infla o layout para podermos preencher os dados
			view = mInflater.inflate(R.layout.item_onde_comer, null);
			
			// Pega o item de acordo com a posicao.
            Acesso item = (Acesso)itens.get(position);
			
			final ImageView iconeCurador = ((ImageView) view.findViewById(R.id.iconeCurador));
	
			Drawable d = null;

            AssetManager assetManager = view.getContext().getAssets();

            InputStream is = null;

            try {
                is = assetManager.open(item.getFotoPrincipal());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                Log.v("Erro","Image nao encontrada"+item.getFotoPrincipal());
            }


            try {


				d =  Drawable.createFromStream(is, "img");
			}catch (OutOfMemoryError e) {
				d = null;
				System.gc();
				
				try {
					d =  Drawable.createFromStream(is, "img");
				} catch (OutOfMemoryError ef) {
                    System.gc();
                    d =  Drawable.createFromStream(is, "img");
				}
			}
			
			try {
				
				int width = iconeCurador.getLayoutParams().width;
				int height = iconeCurador.getLayoutParams().height;
				
				int limit = width>height?width:height;
				
				int widthImage = d.getIntrinsicWidth();
				int heightImage = d.getIntrinsicHeight();
				
				int limitImage = widthImage>heightImage?widthImage:heightImage;
				
				if(limitImage > limit) {
					widthImage = (int)(widthImage / (double)(limitImage/limit));
					heightImage = (int)(heightImage / (double)(limitImage/limit));
				
					Bitmap bitmap = ((BitmapDrawable) d).getBitmap();			
					iconeCurador.setImageDrawable(new BitmapDrawable(Bitmap
							.createScaledBitmap(bitmap, widthImage, heightImage, true)));
					
					bitmap = null;
				} else {
					iconeCurador.setImageDrawable(d);
				}

                ((TextView) view.findViewById(R.id.itemNomeCurador)).setText(item.getNome());
                ((TextView) view.findViewById(R.id.itemNomeCurador)).setTypeface(FontUtil.HELVETICA_M);

				//((TextView) view.findViewById(R.id.itemDescricaoCurador)).setText(Html.fromHtml("<b>Rota: </b>" + item.getRota()));
                //((TextView) view.findViewById(R.id.itemDescricaoCurador)).setTypeface(FontUtil.HELVETICA_M);
		
			}catch (OutOfMemoryError e) {
				System.gc();
			}catch (Exception e) {
				e.printStackTrace();
			}

	    	d = null;

			//ani = null;
		

			/*if(animationOk) {
				Animation a = AnimationUtils.loadAnimation(
						context, android.R.anim.fade_in);
				
				a.setDuration(500);
				a.setStartOffset(300);
		    	((ImageView) view.findViewById(R.id.iconeCurador)).
		    	setAnimation(a);
			}*/
			
			hash.put(position, view);
			
		} /*else {
			if(animationOk) {
				Animation a = AnimationUtils.loadAnimation(
						context, android.R.anim.fade_in);
				
				a.setDuration(500);
				a.setStartOffset(300);
		    	((ImageView) view.findViewById(R.id.iconeCurador)).setAnimation(a);
			}
		}*/
    	
		return view;
	}
	
}
