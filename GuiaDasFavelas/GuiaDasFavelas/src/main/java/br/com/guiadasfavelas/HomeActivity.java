package br.com.guiadasfavelas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.guiadasfavelas.dao.DaoFactory;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.favela.TelaFavela;
import br.com.guiadasfavelas.favela.arquitetura.TelaListArquitetura;
import br.com.guiadasfavelas.favela.arte.TelaListArte;
import br.com.guiadasfavelas.favela.comochegar.TelaListComoChegar;
import br.com.guiadasfavelas.favela.favoritos.TelaListFavoritos;
import br.com.guiadasfavelas.favela.guiaturistico.TelaListGuiasTuristicos;
import br.com.guiadasfavelas.favela.historia.TelaHistoria;
import br.com.guiadasfavelas.favela.mirantes.TelaListMirantes;
import br.com.guiadasfavelas.favela.ondecomer.TelaListOndeComer;
import br.com.guiadasfavelas.favela.ondecomprar.TelaListOndeComprar;
import br.com.guiadasfavelas.favela.ondedancar.TelaListOndeDancar;
import br.com.guiadasfavelas.favela.vivafavela.TelaListVivaFavela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.SimpleViewPagerIndicator;


public class HomeActivity extends FragmentActivity {

//    private static ImageView bgFavelaHome1;
//    private static ImageView bgFavelaHome2;
	
	private static List<Favela> listaFavelas;
    public List<Drawable> drawableList;

    private SimpleViewPagerIndicator pageIndicator;
    //guarda o id atual da favela atual
    private static int idAtual;

    private int gerenciadorTela;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
     * will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link android.support.v4.view.ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onResume() {

        carregaTelaPrincipal();


        super.onResume();
    }

    public void carregaTelaPrincipal() {
        setContentView(R.layout.favelas_slider);

        gerenciadorTela = Constantes.TELA_PRINCIPAL;

        idAtual = 0;

       // bgFavelaHome1 = (ImageView) findViewById(R.id.bgFavelaHome1);
      //  bgFavelaHome2 = (ImageView) findViewById(R.id.bgFavelaHome2);
       // bgFavelaHome2.setVisibility(View.INVISIBLE);
       // bgFavelaHome1.setVisibility(View.INVISIBLE);

        listaFavelas = DaoFactory.getFavelas(this).getFavelas(this);

        drawableList = new ArrayList<Drawable>();


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());



        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        //mViewPager.beginFakeDrag();

        pageIndicator = (SimpleViewPagerIndicator) findViewById(R.id.page_indicator);
        pageIndicator.setViewPager(mViewPager);
        pageIndicator.notifyDataSetChanged();

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {

                idAtual = i;

                pageIndicator.notifyDataSetChanged();

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onLowMemory() {

        System.gc();

        super.onLowMemory();
    }
    

    /**
     * A {@link android.support.v4.app.FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {


            // getItem is called to instantiate the fragment for the given page.
            // Return a DummySectionFragment (defined as a static inner class
            // below) with the page number as its lone argument.
            Fragment fragment = new DummySectionFragment();

            Favela favela = listaFavelas.get(position);
            Bundle args = new Bundle();



            String foto = favela.getFotoPrincipal().replace("foto_principal@2x.png","fundo.jpg");

            //args.putString("imgFavela",favela.getFotoPrincipal());
            args.putString("imgFavela", foto);
            args.putInt("position", position);
            args.putString("nomeFavela",favela.getNome());

            List<ItemFavela> temp = new ArrayList<ItemFavela>();
            //listItemFavela.addAll(favela.getAcessos());
            temp.addAll(favela.getGuiasTuristicos());
            temp.addAll(favela.getMirantes());
            temp.addAll(favela.getOndeComerList());
            temp.addAll(favela.getOndeComprarList());
            temp.addAll(favela.getOndeDancarList());
            temp.addAll(favela.getVivaFavelaList());

            args.putInt("totalAtracoes", temp.size());


            fragment.setArguments(args);

            temp = null;
            args = null;
            favela = null;
            foto = null;

            return fragment;
        }

        @Override
        public int getCount() {
            return listaFavelas.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }

        @Override
        public void destroyItem(View container, int position, Object object) {
            Log.v("Destruindo","Destruindo: "+position);
            super.destroyItem(container, position, object);
        }
    }

    /**
     * A dummy fragment representing a section of the app, but that simply
     * displays dummy text.
     */
    public class DummySectionFragment extends Fragment implements OnClickListener{

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        public static final String ARG_SECTION_NUMBER = "section_number";

        public DummySectionFragment() {
        }




        @Override
        public void onPause() {
            super.onPause();
            try {
                DummySectionFragment.this.finalize();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

            View rootView = null;

            rootView = inflater.inflate(R.layout.fragment_main_dummy, container, false);
//          TextView dummyTextView = (TextView) rootView.findViewById(R.id.section_label);

            rootView.setOnClickListener(this);
            TextView imgFavela = (TextView) rootView.findViewById(R.id.imgFavela);
            TextView nomeFavela = (TextView) rootView.findViewById(R.id.nomeFavela);
            TextView totalAtracoes = (TextView) rootView.findViewById(R.id.totalAtracoes);

            nomeFavela.setTypeface(FontUtil.HELVETICA_B);
            totalAtracoes.setTypeface(FontUtil.HELVETICA_B);

            //nome da favela
            nomeFavela.setText(getArguments().getString("nomeFavela"));
            String imgFavelaArg = getArguments().getString("imgFavela");
            int nTotalAtracoes = getArguments().getInt("totalAtracoes");
            int position = getArguments().getInt("position");

            totalAtracoes.setText(nTotalAtracoes+" Atrações");


            AssetManager assetManager = this.getActivity().getAssets();
            InputStream is = null;

            try {
                is = assetManager.open(imgFavelaArg);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //imagem da favela
            Drawable d = null;

            try {

                d = drawableList.get(position);

            } catch (IndexOutOfBoundsException e){

                try {
                    d = Drawable.createFromStream(is, "img");

                    drawableList.add(d);

                }catch (OutOfMemoryError o) {

                    for(Drawable tv:drawableList) {
                        //Bitmap bitmap = ((BitmapDrawable) tv.getBackground()).getBitmap();
                        //tv.setBackgroundDrawable(null);
                        //bitmap.recycle();
                        //bitmap = null;
                    }

                    System.gc();

                    try {
                        d = Drawable.createFromStream(is, "img");

                        drawableList.add(d);

                    }catch (OutOfMemoryError or) {

                    }
                }
            }



            //imgFavela.setImageDrawable(d);
            imgFavela.setBackgroundDrawable(d);


            imgFavela.setOnClickListener(this);

            d = null;
            assetManager = null;
            is = null;



            imgFavela = null;
            nomeFavela = null;
            totalAtracoes = null;



            return rootView;
        }

		@Override
		public void onClick(View v) {

            for(Drawable tv:drawableList) {
                BitmapDrawable bd = ((BitmapDrawable) tv);

                if(bd != null) {
                    Bitmap bitmap = bd.getBitmap();
                    //tv.setBackgroundDrawable(null);
                    bitmap.recycle();
                    bitmap = null;
                }

                bd = null;
            }

            drawableList = null;

            System.gc();

            carregTelaFavela();
		}
    }

    // ---------- metodos chamados diretamente da view 'home' ----------------------

    public void carregaTelaListComoChegar(View view) {
        carregaTelaComoChegar(listaFavelas.get(idAtual));
    }

    public void  carregaTelaListGuia(View view) {
        carregaTelaListGuiaTuristico(listaFavelas.get(idAtual));
    }

    public void carregaTelaListMirantes(View view) {
        carregaTelaListMirantes(listaFavelas.get(idAtual));
    }

    public void carregaTelaListOndeComer(View view) {
        carregaTelaListOndeComer(listaFavelas.get(idAtual));
    }

    public void carregaTelaListOndeComprar(View view) {
        carregaTelaListOndeComprar(listaFavelas.get(idAtual));
    }

    public void carregaTelaListOndeDancar(View view) {
        carregaTelaListOndeDancar(listaFavelas.get(idAtual));
    }

    public void carregaTelaListVivaFavela(View view) {
        carregaTelaListVivaFavela(listaFavelas.get(idAtual));
    }

    public void carregaTelaListArquitetura(View view) {
        carregaTelaListArquitetura(listaFavelas.get(idAtual));
    }

    public void carregaTelaArte(View view) {
        carregaTelaListArte(listaFavelas.get(idAtual));
    }

    public void carregaTelaHistoria(View view) {
        carregaTelaHistoria(listaFavelas.get(idAtual));
    }

    // ------------------------ fim -------------------------------


    public void carregTelaFavela() {
        new TelaFavela(HomeActivity.this).carregTelaFavela(listaFavelas.get(idAtual));
    }

    public void carregaTelaListOndeComer(Favela favela) {
        new TelaListOndeComer(this).carregaTelaListOndeComer(favela);
    }

    public void carregaTelaListOndeDancar(Favela favela) {
        new TelaListOndeDancar(this).carregaTelaListOndeDancar(favela);
    }

    public void carregaTelaListMirantes(Favela favela) {
        new TelaListMirantes(this).carregaTelaListOndeDancar(favela);
    }

    public void carregaTelaListGuiaTuristico(Favela favela) {
        new TelaListGuiasTuristicos(this).carregaTelaListGuiaTuristico(favela);
    }

    public void carregaTelaListOndeComprar(Favela favela) {
        new TelaListOndeComprar(this).carregaTelaListOndeComprar(favela);
    }

    public void carregaTelaListVivaFavela(Favela favela) {
        new TelaListVivaFavela(this).carregaTelaListVivaFavela(favela);
    }

    public void carregaTelaListArte(Favela favela) {
        new TelaListArte(this).carregaTelaListArte(favela);
    }

    public void carregaTelaListArquitetura(Favela favela) {
        new TelaListArquitetura(this).carregaTelaListArquitetura(favela);
    }

    public void carregaTelaFavoritos(Favela favela) {
        new TelaListFavoritos(this).carregaTelaListArte(favela);
    }

    public void carregaTelaComoChegar(Favela favela) {
        new TelaListComoChegar(this).carregaTelaListOndeComer(favela);
    }

    public void carregaTelaHistoria(Favela favela) {
        new TelaHistoria(this).carregaTelaHistoria(favela);
    }


    public boolean voltar() {
        boolean retorno = true;

        switch(gerenciadorTela) {
            case Constantes.TELA_LOCA:carregaTelaPrincipal();break;
            case Constantes.TELA_ITEM_LOCA:carregTelaFavela();break;
            case Constantes.TELA_LIST:carregTelaFavela();break;
            case Constantes.TELA_ITEM_LIST_ONDE_COMER:carregaTelaListOndeComer(listaFavelas.get(idAtual));break;
            case Constantes.TELA_ITEM_LIST_ONDE_COMPRAR:carregaTelaListOndeComprar(listaFavelas.get(idAtual));break;
            case Constantes.TELA_ITEM_LIST_VIVA_FAVELA:carregaTelaListVivaFavela(listaFavelas.get(idAtual));break;
            case Constantes.TELA_ITEM_LIST_MIRANTES:carregaTelaListMirantes(listaFavelas.get(idAtual));break;
            case Constantes.TELA_ITEM_LIST_ONDE_DANCAR:carregaTelaListOndeDancar(listaFavelas.get(idAtual));break;
            case Constantes.TELA_ITEM_LIST_GUIA:carregaTelaListGuiaTuristico(listaFavelas.get(idAtual));break;
            case Constantes.TELA_PRINCIPAL:fecharAplicativo();break;
            default:retorno = fecharAplicativo();break;
        }

        return retorno;
    }

    public void setGerenciadorTela(int gerenciadorTela) {
        this.gerenciadorTela = gerenciadorTela;
    }

    @Override
    public void onContentChanged() {

        try {
            ViewGroup ll = (ViewGroup) findViewById(R.id.corpo);

            ll.setAnimation(AnimationUtils.loadAnimation(this,
                    R.anim.slide_in_right));
        } catch (Exception e) {
            try {
                //ViewGroup ll = (ViewGroup) findViewById(R.id.corpo2);

                //ll.setAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_in));
            } catch (Exception ef) {}
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        boolean retorno = true;
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            retorno = voltar();
        }

        return retorno;
    }

    public boolean fecharAplicativo() {
        //Handle the back button

            //Ask the user if they want to quit
            new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.sair)
            .setMessage(R.string.desejaSair)
            .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {


                public void onClick(DialogInterface dialog, int which) {

                    //Stop the activity
                    HomeActivity.this.finish();
                }

            })
            .setNegativeButton(R.string.nao, null)
            .show();

        return true;

    }

	

}
