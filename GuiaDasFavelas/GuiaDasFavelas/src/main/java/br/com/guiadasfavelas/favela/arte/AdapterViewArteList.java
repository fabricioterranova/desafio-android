package br.com.guiadasfavelas.favela.arte;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.componentes.OptimizedAdapterView;
import br.com.guiadasfavelas.entities.Arte;
import br.com.guiadasfavelas.util.FavelaUtil;
import br.com.guiadasfavelas.util.FontUtil;

public class AdapterViewArteList extends OptimizedAdapterView {

	public AdapterViewArteList(Context context, List<Arte> itens) {
		
		super(context, itens);
	}
	

	public View getView(int position, View view) {

		view = hash.get(position);
		
		if(view == null) {
		
			// infla o layout para podermos preencher os dados
			view = mInflater.inflate(R.layout.item_onde_comer, null);
			
			// Pega o item de acordo com a posicao.
            Arte item = (Arte)itens.get(position);
			
			final ImageView iconeCurador = ((ImageView) view.findViewById(R.id.iconeCurador));

            iconeCurador.setImageDrawable(FavelaUtil.parseAssetImagePathToDrawableToList(iconeCurador, item.getFotoPrincipal(), R.drawable.sem_foto_arte));

            ((TextView) view.findViewById(R.id.itemNomeCurador)).setText(item.getNome());
            ((TextView) view.findViewById(R.id.itemNomeCurador)).setTypeface(FontUtil.HELVETICA_M);
			
			hash.put(position, view);
			
		}
    	
		return view;
	}
	
}
