package br.com.guiadasfavelas.dao;

import java.util.List;

import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.ItemFavela;

public interface DaoFavoritos {

	public List<ItemFavela> getFavoritos(Favela favela);

	public void insertFavorito(ItemFavela itemFavela, String tipo, Favela favela);

	public void deleteFavorito(ItemFavela palestrante, String tipo);

	public boolean isFavorito(ItemFavela palestrante, String tipo);
}
