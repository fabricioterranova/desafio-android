package br.com.guiadasfavelas.favela.vivafavela;

import android.graphics.Color;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.VivaFavela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FavelaUtil;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaVivaFavela extends Tela {

    public TelaVivaFavela(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBarra(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.viva_favela);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    public void carregaTelaVivaFavela(VivaFavela vivaFavela, Favela favela, boolean voltarTudo) {
        homeActivity.setContentView(R.layout.vivafavela);

        //para carregar a barra superior
        super.carregaTela(favela, true, vivaFavela, Constantes.TIPO_VIVA_FAVELA);

        if(voltarTudo) {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LOCA);
        } else {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LIST_VIVA_FAVELA);
        }

        carregaBarra(favela, R.drawable.icone_interno_vivafavela);

        TextView especialidadeItem = (TextView) homeActivity.findViewById(R.id.especialidadeItem);
        TextView nomeItem = (TextView) homeActivity.findViewById(R.id.nomeItem);
        TextView enderecoItem = (TextView) homeActivity.findViewById(R.id.enderecoItem);
        TextView comoChegarItem = (TextView) homeActivity.findViewById(R.id.comoChegarItem);
        TextView diferencialItem= (TextView) homeActivity.findViewById(R.id.diferencialItem);
        ImageView imagemItem = (ImageView) homeActivity.findViewById(R.id.imagemItem);
        ImageView imagemSecundariaItem = (ImageView) homeActivity.findViewById(R.id.imagemSecundariaItem);

        especialidadeItem.setTypeface(FontUtil.HELVETICA_M);
        nomeItem.setTypeface(FontUtil.HELVETICA_M);
        enderecoItem.setTypeface(FontUtil.HELVETICA_M);
        comoChegarItem.setTypeface(FontUtil.HELVETICA_M);
        diferencialItem.setTypeface(FontUtil.HELVETICA_M);

        TextView especialidadeTitulo = (TextView) homeActivity.findViewById(R.id.especialidadeTitulo);
        TextView servicoTitulo = (TextView) homeActivity.findViewById(R.id.servicoTitulo);
        TextView comoChegarTitulo = (TextView) homeActivity.findViewById(R.id.comoChegarTitulo);
        TextView diferencialTitulo = (TextView) homeActivity.findViewById(R.id.diferencialTitulo);
        especialidadeTitulo.setTypeface(FontUtil.HELVETICA_M);
        servicoTitulo.setTypeface(FontUtil.HELVETICA_M);
        comoChegarTitulo.setTypeface(FontUtil.HELVETICA_M);
        diferencialTitulo.setTypeface(FontUtil.HELVETICA_M);
        especialidadeTitulo.setText(R.string.atracao);
        servicoTitulo.setText(R.string.experiencia);
        comoChegarTitulo.setText(R.string.servico);
        //diferencialTitulo.setText("Experiência:");

        especialidadeItem.setText(Html.fromHtml(vivaFavela.getAtracao()));
        nomeItem.setText(vivaFavela.getNome());
        enderecoItem.setText(Html.fromHtml(vivaFavela.getExperiencia()));
        comoChegarItem.setText(Html.fromHtml(vivaFavela.getEndereco()));
        //diferencialItem.setText(Html.fromHtml("<b>Atrações: </b>"+vivaFavela.getAtracoes()));

        imagemItem.setImageDrawable(FavelaUtil.parseAssetImagePathToDrawable(imagemItem, vivaFavela.getFotoPrincipal(), R.drawable.sem_foto_vivafavela));
    }
}
