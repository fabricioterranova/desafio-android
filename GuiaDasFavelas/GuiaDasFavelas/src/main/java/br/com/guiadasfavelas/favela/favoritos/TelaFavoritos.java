package br.com.guiadasfavelas.favela.favoritos;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaFavoritos extends Tela {

    public TelaFavoritos(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBarra(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.favoritos);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    public void carregaTelaFavoritos(ItemFavela itemFavela, Favela favela, boolean voltarTudo) {
        homeActivity.setContentView(R.layout.onde_comer);

        //para carregar a barra superior
        super.carregaTela(favela, true, itemFavela, Constantes.TIPO_FAVORITO);

        if(voltarTudo) {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LOCA);
        } else {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LIST_FAVORITOS);
        }

        carregaBarra(favela, R.drawable.icone_interno_favoritos);

        TextView especialidadeItem = (TextView) homeActivity.findViewById(R.id.especialidadeItem);
        TextView nomeItem = (TextView) homeActivity.findViewById(R.id.nomeItem);
        TextView enderecoItem = (TextView) homeActivity.findViewById(R.id.enderecoItem);
        TextView comoChegarItem = (TextView) homeActivity.findViewById(R.id.comoChegarItem);
        TextView diferencialItem= (TextView) homeActivity.findViewById(R.id.diferencialItem);
        ImageView imagemItem = (ImageView) homeActivity.findViewById(R.id.imagemItem);
        ImageView imagemSecundariaItem = (ImageView) homeActivity.findViewById(R.id.imagemSecundariaItem);

        especialidadeItem.setTypeface(FontUtil.HELVETICA_M);
        nomeItem.setTypeface(FontUtil.HELVETICA_M);
        enderecoItem.setTypeface(FontUtil.HELVETICA_M);
        comoChegarItem.setTypeface(FontUtil.HELVETICA_M);
        diferencialItem.setTypeface(FontUtil.HELVETICA_M);

        //especialidadeItem.setText(Html.fromHtml("<b>Nome: </b>"+arte.getNome()));
        //nomeItem.setText(arte.getNome());
        //enderecoItem.setText(Html.fromHtml("<b>Atração: </b>"+arte.getAtracao()));
        //comoChegarItem.setText(Html.fromHtml("<b>Localização: </b>"+arte.getLocalizacao()));
        //diferencialItem.setText(Html.fromHtml("<b>Diferencial: </b>"+arte.getDiferencial()));


        AssetManager assetManager = homeActivity.getAssets();

        InputStream is = null;


        try {
            is = assetManager.open(itemFavela.getFotoPrincipal());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + itemFavela.getFotoPrincipal());
        }



        imagemItem.setImageDrawable(montarImagem(is));

        // imagem secundaria

        InputStream iss = null;

        imagemSecundariaItem.setVisibility(View.INVISIBLE);
        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 0);
        imagemSecundariaItem.setLayoutParams(ll);

        try {

            iss = assetManager.open(itemFavela.getFotoSecundaria());

            imagemSecundariaItem.setImageDrawable(montarImagem(iss));

            imagemSecundariaItem.setVisibility(View.VISIBLE);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + itemFavela.getFotoSecundaria());
        }

    }

    private Drawable montarImagem(InputStream is) {

        Drawable d = null;
        try {

            d =  Drawable.createFromStream(is, "img");
        } catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        return d;
    }
}
