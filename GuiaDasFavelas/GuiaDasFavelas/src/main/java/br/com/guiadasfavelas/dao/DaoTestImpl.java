package br.com.guiadasfavelas.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;

import br.com.guiadasfavelas.util.Constantes;

public class DaoTestImpl extends Dao implements DaoTest {

	public DaoTestImpl(Context context) {
		super(context);
	}

	public void startDatabase() throws SQLiteException {
		
		Cursor c = getReadableDatabase().rawQuery(Constantes.QUERY_TEST, null);
		
		if(!(c.getCount() > 0)) {
			c.close();
			throw new SQLiteException();
		}
		
		c.close();
		
		getReadableDatabase().close();
	}
}
