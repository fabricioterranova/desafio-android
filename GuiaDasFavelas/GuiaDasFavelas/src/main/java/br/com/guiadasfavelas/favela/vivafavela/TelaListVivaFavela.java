package br.com.guiadasfavelas.favela.vivafavela;

import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 26/06/13.
 */
public class TelaListVivaFavela extends Tela implements AdapterView.OnItemClickListener {

    private Favela favela;

    public TelaListVivaFavela(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBara(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);

        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.viva_favela);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }


    public void carregaTelaListVivaFavela(Favela favela) {
        this.favela = favela;

        homeActivity.setGerenciadorTela(Constantes.TELA_LIST);

        homeActivity.setContentView(R.layout.list_onde_comer);

        //para carregar a barra superior
        super.carregaTela(favela, false, null, null);

        carregaBara(favela, R.drawable.icone_interno_vivafavela);

        ListView listaOndeComer = (ListView) homeActivity.findViewById(R.id.listaOndeComer);

        AdapterViewVivaFavelaList adapterViewOndeComerList = null;

        System.gc();

        adapterViewOndeComerList = new AdapterViewVivaFavelaList(homeActivity.getApplicationContext(),favela.getVivaFavelaList());

        listaOndeComer.setOnItemClickListener(this);
        listaOndeComer.setAdapter(adapterViewOndeComerList);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        new TelaVivaFavela(homeActivity).carregaTelaVivaFavela(favela.getVivaFavelaList().get(i), favela, false);
    }
}
