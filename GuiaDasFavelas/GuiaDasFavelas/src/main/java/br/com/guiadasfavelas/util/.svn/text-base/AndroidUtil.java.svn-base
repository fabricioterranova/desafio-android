package br.com.guiadasfavelas.util;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;


public class AndroidUtil {

	// gps
	private Location localizacao;
	private static LocationManager lm;

	private Context context;

	public AndroidUtil(Context context) {
		this.context = context;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	public String getAddressByLocation() {
		
		if(lm == null)
			return null;
		
		if(localizacao == null)
			localizacao = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		if(localizacao == null)
			localizacao = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		
		if(localizacao == null) {
			return null;
		}
			
		
		Geocoder geocoder = new Geocoder(context,Locale.getDefault());
		StringBuilder strReturnedAddress = new StringBuilder("Endereço:\n");

		try {
			
			List<Address> addresses = geocoder.getFromLocation(localizacao.getLatitude(), localizacao.getLongitude(),1);

			if (addresses != null && addresses.size()> 0) {
				Address returnedAddress = addresses.get(0);
				strReturnedAddress.append(returnedAddress.getAddressLine(0));
				
			} else {
			}
		} catch (Exception e) {
		}
		
		return strReturnedAddress.toString();
	}

	public Location getLocation() {
		return localizacao;
	}

	public boolean startGPSReader() {
		lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		if (lm == null)
			return false;

		
		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				localizacao = location;
			}

			public void onStatusChanged(String provider, int status,Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};

		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

		return true;
	}

	public Intent getRecognizeSpeech() {

		PackageManager pm = context.getPackageManager();

		Intent speech = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

		if (pm.queryIntentActivities(speech, 0).size() != 0) {
			return speech;
		} else {
			return null;
		}
	}

	public static void exibeMensagem(Context context, String texto) {
		
		AlertDialog alert = new AlertDialog.Builder(context).setMessage(texto)
		.setPositiveButton(android.R.string.ok,null).create();
		alert.show();
	}
	
	public static void exibeMensagem(Context context, int stringId) {
		
		String mensagem = context.getResources().getString(stringId);
		
		AlertDialog alert = new AlertDialog.Builder(context).setMessage(mensagem)
		.setPositiveButton(android.R.string.ok,null).create();
		alert.show();
	}
	
	public static void unbindInnerDrawables(View view) {
		
		if(view != null) {
		    if (view.getBackground() != null) {
		        view.getBackground().setCallback(null);
		    }
		    if (view instanceof ViewGroup) {
		        for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
	
		        	unbindInnerDrawables(((ViewGroup) view).getChildAt(i));
		        }
		        try {
		            ((ViewGroup) view).removeAllViews();
		        } catch (UnsupportedOperationException  e) {
	
		        }
		    }
		}
	}
	
	public static void unbindDrawables(View view) {
		if(view != null) {
		    if (view.getBackground() != null) {
		        view.getBackground().setCallback(null);
		    }
		    
		    if (view instanceof ViewGroup) {
		       
		        try {
		            ((ViewGroup) view).removeAllViews();
		        } catch (UnsupportedOperationException  e) {
	
		        }
		    }
		}
	}

	//chama o navegador do aparelho passando uma url
	public static void carregaSite(Context context, String url) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		context.startActivity(browserIntent);
	}
	
	public static void hidenKeyboard(Activity activity) {
		//escondendo o teclado
		InputMethodManager inputManager = (InputMethodManager) activity.
		            getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
		        InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	public static void showDeviceLayoutInfo(Activity activity) {
		//Determine screen size
	    if ((activity.getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {     
	        Toast.makeText(activity, "Large screen",Toast.LENGTH_LONG).show();
	    }
	    else if ((activity.getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {     
	        Toast.makeText(activity, "Normal sized screen" , Toast.LENGTH_LONG).show();
	    } 
	    else if ((activity.getResources().getConfiguration().screenLayout &      Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {     
	        Toast.makeText(activity, "Small sized screen" , Toast.LENGTH_LONG).show();
	    }
	    else {
	        Toast.makeText(activity, "Screen size is neither large, normal or small" , Toast.LENGTH_LONG).show();
	    }

	    //Determine density
	    DisplayMetrics metrics = new DisplayMetrics();
	    activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
	        int density = metrics.densityDpi;

	        if (density==DisplayMetrics.DENSITY_HIGH) {
	            Toast.makeText(activity, "DENSITY_HIGH... Density is " + String.valueOf(density),  Toast.LENGTH_LONG).show();
	        }
	        else if (density==DisplayMetrics.DENSITY_MEDIUM) {
	            Toast.makeText(activity, "DENSITY_MEDIUM... Density is " + String.valueOf(density),  Toast.LENGTH_LONG).show();
	        }
	        else if (density==DisplayMetrics.DENSITY_LOW) {
	            Toast.makeText(activity, "DENSITY_LOW... Density is " + String.valueOf(density),  Toast.LENGTH_LONG).show();
	        }
	        else {
	            Toast.makeText(activity, "Density is neither HIGH, MEDIUM OR LOW.  Density is " + String.valueOf(density),  Toast.LENGTH_LONG).show();
	        }
	}
	
	/*
	 * exibe uma notificacao na barra de status do aparelho
	 */
	
	public static void showNotification(Context context, String titulo, String descricao, String textoBarra, Class <?> dsl) {
		int notificationID = (int) System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// Create the notification
		Notification notification = new Notification(android.R.drawable.bottom_bar, textoBarra, System.currentTimeMillis());
		// Create the notification's expanded message
		// When the user clicks on it, it opens your activity
		
		Intent intent = new Intent(context, dsl);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 , intent, 0);
		notification.setLatestEventInfo(context, titulo, descricao, pendingIntent);
		// Show notification
		notificationManager.notify(notificationID, notification);
	}

    public static void abrirImagem(Context context, String path) {

        File file = new File(path);
        String type = getType(file);
        Intent it = new Intent();
        it.setAction(android.content.Intent.ACTION_VIEW);
        it.setDataAndType(Uri.fromFile(file),"image/png");

        try {
            context.startActivity(it);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Aplicativo necessário não encontrado.", Toast.LENGTH_SHORT);
        }
    }

    private static String getType(File file) {
        String type = null;
        try {
            URL u = file.toURL();
            URLConnection uc = null;
            uc = u.openConnection();
            type = uc.getContentType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }

    public static Drawable montarImagem(InputStream is) {

        Drawable d = null;
        try {

            d =  Drawable.createFromStream(is, "img");
        } catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        return d;
    }

}

