package br.com.guiadasfavelas.util;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtil {
	
	private Context context;

	public static Typeface HELVETICA_B;
    public static Typeface HELVETICA_M;
	
	public FontUtil(Context context) {
		this.context = context;
	}
	
	public void carregarFontes() {

        HELVETICA_B = Typeface.createFromAsset(context.getAssets(),"fonts/HelveticaNeueLTStd-Bd.otf");
        HELVETICA_M = Typeface.createFromAsset(context.getAssets(),"fonts/HelveticaNeueLTStd-Md.otf");
	}

}

