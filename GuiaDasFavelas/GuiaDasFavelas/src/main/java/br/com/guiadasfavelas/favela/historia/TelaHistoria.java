package br.com.guiadasfavelas.favela.historia;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaHistoria extends Tela {

    public TelaHistoria(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBarra(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.historia);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    public void carregaTelaHistoria(Favela favela) {
        homeActivity.setContentView(R.layout.historia);

        //para carregar a barra superior
        super.carregaTela(favela, false, null, Constantes.TIPO_COMO_CHEGAR);


        homeActivity.setGerenciadorTela(Constantes.TELA_LIST);

        carregaBarra(favela, R.drawable.icone_interno_historia);

        TextView textHistoria= (TextView) homeActivity.findViewById(R.id.textHistoria);
        ImageView imagemHistoria = (ImageView) homeActivity.findViewById(R.id.imagemHistoria);

        textHistoria.setTypeface(FontUtil.HELVETICA_M);
        textHistoria.setText(Html.fromHtml(favela.getHistoria()));

       // especialidadeItem.setText(Html.fromHtml("<b>Especialidade: </b>"+ondeComer.getEspecialidade()));
       // nomeItem.setText(ondeComer.getNome());
       // enderecoItem.setText(Html.fromHtml("<b>Endereço: </b>"+ondeComer.getEndereco()));
       // comoChegarItem.setText(Html.fromHtml("<b>Como Chegar: </b>"+ondeComer.getComoChegar()));
       // diferencialItem.setText(Html.fromHtml("<b>Diferencial: </b>"+ondeComer.getDiferencial()));

        Drawable d = null;

        AssetManager assetManager = homeActivity.getAssets();

        InputStream is = null;

        try {
            is = assetManager.open(favela.getFotoPrincipal());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + favela.getFotoPrincipal());
        }


        try {


            d =  Drawable.createFromStream(is, "img");
        }catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        imagemHistoria.setImageDrawable(d);
    }
}
