package br.com.guiadasfavelas.sliding;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.guiadasfavelas.R;

public class SampleListFragment extends ListFragment {

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list, null);

	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		SampleAdapter adapter = new SampleAdapter(getActivity());

		for (int i = 0; i < 12; i++) {
			adapter.add(new SampleItem("Sample List", android.R.drawable.ic_menu_search));
		}

		setListAdapter(adapter);
	}

	private class SampleItem {
		public String tag;
		public int iconRes;
		public SampleItem(String tag, int iconRes) {
			this.tag = tag; 
			this.iconRes = iconRes;
		}
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {

        List<Integer> listIcons;
        List<String> listOptions;

        protected LayoutInflater mInflater;

		public SampleAdapter(Context context) {
			super(context, 0);

            listIcons = new ArrayList<Integer>();
            listOptions = new ArrayList<String>();

            listOptions.add(context.getResources().getString(R.string.perfil));
            listOptions.add(context.getResources().getString(R.string.favoritos));
            listOptions.add(context.getResources().getString(R.string.historia));
            listOptions.add(context.getResources().getString(R.string.como_chegar));
            listOptions.add(context.getResources().getString(R.string.guias_turisticos_locais));
            listOptions.add(context.getResources().getString(R.string.mirantes));
            listOptions.add(context.getResources().getString(R.string.onde_comer));
            listOptions.add(context.getResources().getString(R.string.onde_comprar));
            listOptions.add(context.getResources().getString(R.string.onde_dancar));
            listOptions.add(context.getResources().getString(R.string.viva_favela));
            listOptions.add(context.getResources().getString(R.string.arte));
            listOptions.add(context.getResources().getString(R.string.arquitetura));

            listIcons.add(R.drawable.icone_menulateral_perfil_favela);
            listIcons.add(R.drawable.icone_menulateral_favoritos);
            listIcons.add(R.drawable.icone_menulateral_arquitetura);
            listIcons.add(R.drawable.icone_menulateral_comochegar);
            listIcons.add(R.drawable.icone_menulateral_guiasturisticos);
            listIcons.add(R.drawable.icone_menulateral_mirantes);
            listIcons.add(R.drawable.icone_menulateral_ondecomer);
            listIcons.add(R.drawable.icone_menulateral_ondecomprar);
            listIcons.add(R.drawable.icone_menulateral_ondedancar);
            listIcons.add(R.drawable.icone_menulateral_vivafavela);
            listIcons.add(R.drawable.icone_menulateral_arte);
            listIcons.add(R.drawable.icone_menulateral_arquitetura);

            mInflater = LayoutInflater.from(context);
		}
		

		public View getView(int position, View view, ViewGroup parent) {

            // infla o layout para podermos preencher os dados
            view = mInflater.inflate(R.layout.item_menu_opcoes, null);


            ((TextView) view.findViewById(R.id.nomeOpcao)).setText(listOptions.get(position));

            if(position == 0 || position == 1) {
                ((TextView) view.findViewById(R.id.nomeOpcao)).setTextColor(Color.parseColor("#ffff6d2c"));
            }

                ((ImageView) view.findViewById(R.id.iconeOpcao)).setBackgroundResource(listIcons.get(position));

            /*
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
			}
			ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
			icon.setImageResource(getItem(position).iconRes);
			TextView title = (TextView) convertView.findViewById(R.id.row_title);
			title.setText(getItem(position).tag);

			return convertView;
			*/

            return view;
		}

	}
}
