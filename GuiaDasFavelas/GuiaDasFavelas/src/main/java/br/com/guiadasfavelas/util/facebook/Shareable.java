package br.com.guiadasfavelas.util.facebook;

public interface Shareable {

	FacebookPost getFacebookPost();
	void setFacebookPost(FacebookPost facebookPost);
}
