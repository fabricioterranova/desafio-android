package br.com.guiadasfavelas.dao;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import br.com.guiadasfavelas.entities.Favela;

public class DaoFavelasImpl implements DaoFavelas{
	
	public static final String DATA_ASSET_PATH = "data/CACHE_FAVELAS";
    public static final String DATA_ASSET_PATH_EN = "data/CACHE_FAVELAS_EN";
    public static final String DATA_ASSET_PATH_ES = "data/CACHE_FAVELAS_ES";
	
	protected DaoFavelasImpl() {}
	
	public List<Favela> getFavelas(Context context) {
		
		AssetManager assetManager = context.getAssets();
		
		InputStream is = null;

		try {

            String language = Locale.getDefault().getLanguage();

            if(language.contains("en")) {
                is = assetManager.open(DATA_ASSET_PATH_EN);
            } else if(language.contains("es")) {
                is = assetManager.open(DATA_ASSET_PATH_ES);
            } else {
                is = assetManager.open(DATA_ASSET_PATH);
            }

		} catch (IOException e) {
            try {
                is = assetManager.open(DATA_ASSET_PATH);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            //e.printStackTrace();
		}

		//Gson gson = new Gson();
		Gson gson = new Gson();

		Reader reader = new InputStreamReader(is);

		JsonParser parser = new JsonParser();
		JsonArray Jarray = parser.parse(reader).getAsJsonArray();

		List<Favela> lcs = new ArrayList<Favela>();

		for (JsonElement obj : Jarray) {
			Favela jse = gson.fromJson(obj, Favela.class);
			lcs.add(jse);
		}

		return lcs;
	}
}
