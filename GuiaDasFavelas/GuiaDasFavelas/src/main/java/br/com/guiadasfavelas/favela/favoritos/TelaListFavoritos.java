package br.com.guiadasfavelas.favela.favoritos;

import android.graphics.Color;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.dao.DaoFactory;
import br.com.guiadasfavelas.entities.Arquitetura;
import br.com.guiadasfavelas.entities.Arte;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.GuiaTuristico;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.entities.Mirante;
import br.com.guiadasfavelas.entities.OndeComer;
import br.com.guiadasfavelas.entities.OndeComprar;
import br.com.guiadasfavelas.entities.OndeDancar;
import br.com.guiadasfavelas.entities.VivaFavela;
import br.com.guiadasfavelas.favela.arquitetura.TelaArquitetura;
import br.com.guiadasfavelas.favela.arte.TelaArte;
import br.com.guiadasfavelas.favela.guiaturistico.TelaGuiasTuristicos;
import br.com.guiadasfavelas.favela.mirantes.TelaMirantes;
import br.com.guiadasfavelas.favela.ondecomer.TelaOndeComer;
import br.com.guiadasfavelas.favela.ondecomprar.TelaOndeComprar;
import br.com.guiadasfavelas.favela.ondedancar.TelaOndeDancar;
import br.com.guiadasfavelas.favela.vivafavela.TelaVivaFavela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 26/06/13.
 */
public class TelaListFavoritos extends Tela implements AdapterView.OnItemClickListener {

    private Favela favela;

    public TelaListFavoritos(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBara(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.favoritos);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }


    public void carregaTelaListArte(Favela favela) {
        this.favela = favela;

        homeActivity.setGerenciadorTela(Constantes.TELA_LIST);

        homeActivity.setContentView(R.layout.list_onde_comer);

        //para carregar a barra superior
        super.carregaTela(favela, false, null, null);

        carregaBara(favela, R.drawable.icone_interno_favoritos);

        ListView listaOndeComer = (ListView) homeActivity.findViewById(R.id.listaOndeComer);

        AdapterViewFavoritosList adapterViewOndeComerList = null;

        System.gc();

        List<ItemFavela> l = new ArrayList<ItemFavela>();
        l = DaoFactory.getFavoritos(homeActivity).getFavoritos(favela);

        adapterViewOndeComerList = new AdapterViewFavoritosList(homeActivity.getApplicationContext(),l);

        listaOndeComer.setOnItemClickListener(this);
        listaOndeComer.setAdapter(adapterViewOndeComerList);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        ItemFavela itemFavela = DaoFactory.getFavoritos(homeActivity).getFavoritos(favela).get(i);


        //carregando os itens clicados da tela loca
        if(itemFavela instanceof Mirante) {
            Mirante mirante = (Mirante) itemFavela;
            new TelaMirantes(homeActivity).carregaTelaMirantes(mirante, favela, true);
        } else if(itemFavela instanceof OndeComer) {
            OndeComer ondeComer = (OndeComer) itemFavela;
            new TelaOndeComer(homeActivity).carregaTelaOndeComer(ondeComer, favela, true);
        } else if(itemFavela instanceof OndeComprar) {
            OndeComprar ondeComprar = (OndeComprar) itemFavela;
            new TelaOndeComprar(homeActivity).carregaTelaOndeComprar(ondeComprar, favela, true);
        } else if(itemFavela instanceof VivaFavela) {
            VivaFavela vivaFavela = (VivaFavela) itemFavela;
            new TelaVivaFavela(homeActivity).carregaTelaVivaFavela(vivaFavela, favela, true);
        } else if(itemFavela instanceof OndeDancar) {
            OndeDancar ondeDancar = (OndeDancar) itemFavela;
            new TelaOndeDancar(homeActivity).carregaTelaOndeDancar(ondeDancar, favela, true);
        } else if(itemFavela instanceof GuiaTuristico) {
            GuiaTuristico guiaTuristico = (GuiaTuristico) itemFavela;
            new TelaGuiasTuristicos(homeActivity).carregaTelaGuiaTuristico(guiaTuristico, favela, true);
        } else if(itemFavela instanceof Arte) {
            Arte arte = (Arte) itemFavela;
            new TelaArte(homeActivity).carregaTelaArte(arte,favela, true);
        } else if(itemFavela instanceof Arquitetura) {
            Arquitetura arquitetura = (Arquitetura) itemFavela;
            new TelaArquitetura(homeActivity).carregaTelaArquitetura(arquitetura, favela, true);
        }
    }
}
