package br.com.guiadasfavelas.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class FileUtil {
	
	private Context ctx;
	
	public FileUtil(Context ctx) {
		this.ctx = ctx;
	}

	public List<String> readRawAsLineList(int resId) {
		
		Resources resources = ctx.getResources();
		InputStream inputStream = resources.openRawResource(resId);

		InputStreamReader inputreader = new InputStreamReader(inputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;

		List<String> temp = new ArrayList<String>();

		try {
			while ((line = buffreader.readLine()) != null) {
				temp.add(line);
			}
		} catch (IOException e) {
			return null;
		}
		
		return temp;
	}
	
	public static List<String> readISAsString(InputStream is) {

		InputStreamReader inputreader = new InputStreamReader(is);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;

		List<String> temp = new ArrayList<String>();

		try {
			while ((line = buffreader.readLine()) != null) {
				temp.add(line);
			}
		} catch (IOException e) {
			return null;
		}
		
		return temp;
	}
	
	public String readRawAsString(int resId) {
		
		Resources resources = ctx.getResources();
		InputStream inputStream = resources.openRawResource(resId);

		InputStreamReader inputreader = new InputStreamReader(inputStream);
		BufferedReader buffreader = new BufferedReader(inputreader);
		String line;


		StringBuilder sb = new StringBuilder();
		try {
			while ((line = buffreader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			return null;
		}
		
		return sb.toString();
	}
	
	public static void deleteFilesDir(String dir) {
		File files = new File(dir);
		
		for(File f:files.listFiles()) {
			if(!f.delete()) {
				Log.v("Teste", "nao deletado");
			}
		}
	}
	
	public static void deleteDir(String dir) {
		File file = new File(dir);
		if(!file.delete()) {
			Log.v("Teste", "dir nao deletado");
		}
	}
	
	public static void deleteFile(File file) {
		if(!file.delete()) {
			Log.v("Teste", "arquivo nao deletado");
		}
	}
	
	public static String getFileNameFromUrl(String url) {
		String[]temp = url.split("/");
		
		if(temp!=null) {
			return temp[temp.length-1];
		}
		
		return null;
	}
	
	public static void saveSerializableObject(Serializable serializable, String path) throws IOException {
		// Write to disk with FileOutputStream
		FileOutputStream f_out = new FileOutputStream(path);

		// Write object with ObjectOutputStream
		ObjectOutputStream obj_out = new ObjectOutputStream(f_out);

		// Write object out to disk
		obj_out.writeObject(serializable);
		
		obj_out.close();
	}
	
	public static Object loadSerializableObject(String path) throws StreamCorruptedException, IOException, ClassNotFoundException {
		// Read from disk using FileInputStream
		FileInputStream f_in = new FileInputStream(path);

		// Read object using ObjectInputStream
		ObjectInputStream obj_in = new ObjectInputStream(f_in);

		// Read an object
		Object obj = obj_in.readObject();
		
		obj_in.close();

		return obj;
	}
	
	public static void downloadFileFromUrl(String fileUrl, String pathAndfileName) {
		
        try {
        	
            URL url = new URL(fileUrl);
            File file = new File(pathAndfileName);
          
            URLConnection ucon = url.openConnection();

            InputStream is = ucon.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);

            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            int current = 0;
            while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
            }

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(baf.toByteArray());
            fos.close();
                
        } catch (IOException e) {}

	}
	
	public static void copyFile(String arquivo, String origem, String destino) {
		try {
			InputStream is = new FileInputStream(origem+"/"+arquivo);
				
	    	OutputStream out = new FileOutputStream(new File(destino+"/"+arquivo));
	     
	    	int read = 0;
	    	byte[] bytes = new byte[1024];
	     
	    	while ((read = is.read(bytes)) != -1) {
	    		out.write(bytes, 0, read);
	    	}
	     
	    	is.close();
	    	out.flush();
	    	out.close();
	    	
		} catch (Exception e) {

		}
	}
	
	public static File inputStreamToFile(InputStream is, String fileName) {
		
		File f = new File(fileName);
		try {
			  
			OutputStream out=new FileOutputStream(f);
			byte buf[]=new byte[1024];
			int len;
			while((len=is.read(buf))>0) {
				out.write(buf,0,len);
			}
			
			out.close();
			is.close();
			
		} catch (IOException e) {
		}
		
		return f;
	}
}
