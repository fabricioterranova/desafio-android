package br.com.guiadasfavelas.util.facebook;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import br.com.guiadasfavelas.util.facebook.android.DialogError;
import br.com.guiadasfavelas.util.facebook.android.Facebook;
import br.com.guiadasfavelas.util.facebook.android.FacebookError;

public class FacebookUtil {
	
	private Activity activity;
	
	private Facebook facebook;
	
	private SharedPreferences mPrefs;

	public FacebookUtil(Activity activity) {
		this.activity = activity;
		facebook = new Facebook("484475258296102");
	}
	
	public void requestAutenticaoFacebook(final String name, final String link, final String caption, final String description, final String picture) {
		
		mPrefs = activity.getPreferences(Activity.MODE_PRIVATE);
        String access_token = mPrefs.getString("access_token", null);
        long expires = mPrefs.getLong("access_expires", 0);
        
        if(access_token != null) {
            facebook.setAccessToken(access_token);
        } else {
        	
        }
        if(expires != 0) {
            facebook.setAccessExpires(expires);
        }
        
        
        if(!facebook.isSessionValid()) {
        
			facebook.authorize(activity,new String[]{"publish_stream"}, new Facebook.DialogListener() {
	            
	            public void onComplete(Bundle values) {
	            	
	            	SharedPreferences.Editor editor = mPrefs.edit();
	                editor.putString("access_token", facebook.getAccessToken());
	                editor.putLong("access_expires", facebook.getAccessExpires());
	                editor.commit();
	                
	                facebookar(name, link, caption, description, picture);
	            }
	
	            
	            public void onFacebookError(FacebookError error) {}
	
	            
	            public void onError(DialogError e) {}
	
	            
	            public void onCancel() {}
	        });
        } else {
        	facebookar(name, link, caption, description, picture);
        }
		
		//facebook.
	}
		
	public void facebookar(String name, String link, String caption, String description, String picture) {
	    
	    //post on friend's wall.
	    Bundle params = new Bundle();
	   /* params.putString("to", "");
	    params.putString("description", "Sogesp description");
	    params.putString("caption", "Sogesp caption");
	    params.putString("picture", "http://fbrell.com/f8.jpg");
	    params.putString("name", "Sogesp nome");
	    params.putString("link", "http://www.vertigo.com.br");*/
	    
	    params.putString("to", "");
	    params.putString("description", description);
	    params.putString("caption", caption);
	    //params.putString("picture", "http://media-cache-ec0.pinimg.com/236x/15/3a/40/153a40cb7c208743e9f3a4b3627e85a4.jpg");
        params.putString("picture", picture);
	    params.putString("name", name);
	    params.putString("link", link);
	    

	    facebook.dialog(activity, "feed", params, new Facebook.DialogListener() {

			public void onComplete(Bundle values) {
			}

			public void onFacebookError(FacebookError e) {
				Log.v("Teste", e.getErrorType());
				
			}

			public void onError(DialogError e) {
				Log.v("Teste", e.getMessage());
				
			}

			public void onCancel() {
				
			}});
	}

}
