package br.com.guiadasfavelas.util;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by prenovato on 21/08/13.
 */
public class FavelaUtil {

    public static Drawable parseAssetImagePathToDrawableToList(ImageView view, String path, int resId) {
        Drawable d = null;

        AssetManager assetManager = view.getContext().getAssets();

        InputStream is = null;

        try {
            is = assetManager.open(path);


        } catch (FileNotFoundException fnf) {

            try {
                is = assetManager.open(path.replace(".png",".jpg"));
            } catch (IOException e) {
                return view.getContext().getResources().getDrawable(resId);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + path);
        }


        try {


            d =  Drawable.createFromStream(is, "img");
        }catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        try {

            int width = view.getLayoutParams().width;
            int height = view.getLayoutParams().height;

            int limit = width>height?width:height;

            int widthImage = d.getIntrinsicWidth();
            int heightImage = d.getIntrinsicHeight();

            int limitImage = widthImage>heightImage?widthImage:heightImage;

            if(limitImage > limit) {
                widthImage = (int)(widthImage / (double)(limitImage/limit));
                heightImage = (int)(heightImage / (double)(limitImage/limit));

                Bitmap bitmap = ((BitmapDrawable) d).getBitmap();

                return new BitmapDrawable(Bitmap
                        .createScaledBitmap(bitmap, widthImage + 45, heightImage + 35, true));

            } else {
                return d;
            }

        }catch (OutOfMemoryError e) {
            System.gc();
        }catch (Exception e) {
            e.printStackTrace();
        }

        d = null;

        return d;
    }


    public static Drawable parseAssetImagePathToDrawable(ImageView view, String path, int resId) {

        AssetManager assetManager = view.getContext().getAssets();

        InputStream is = null;

        try {
            is = assetManager.open(path);

        } catch (FileNotFoundException fnf) {

            try {
                is = assetManager.open(path.replace(".png",".jpg"));
            } catch (IOException e) {
                return view.getContext().getResources().getDrawable(resId);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + path);
        }

        return AndroidUtil.montarImagem(is);
    }

    public static Drawable parseAssetImagePathToDrawableToListToGuias(ImageView view, String path, int resId) {
        Drawable d = null;

        AssetManager assetManager = view.getContext().getAssets();

        InputStream is = null;

        try {
            is = assetManager.open(path);


        } catch (FileNotFoundException fnf) {

            try {
                is = assetManager.open(path.replace(".png",".jpg"));
            } catch (IOException e) {
                return view.getContext().getResources().getDrawable(resId);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + path);
        }


        try {


            d =  Drawable.createFromStream(is, "img");
        }catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        try {

            int width = view.getLayoutParams().width;
            int height = view.getLayoutParams().height;

            int limit = width>height?width:height;

            int widthImage = d.getIntrinsicWidth();
            int heightImage = d.getIntrinsicHeight();

            int limitImage = widthImage>heightImage?widthImage:heightImage;

            if(limitImage > limit) {
                widthImage = (int)(widthImage / (double)(limitImage/limit));
                heightImage = (int)(heightImage / (double)(limitImage/limit));

                Bitmap bitmap = ((BitmapDrawable) d).getBitmap();

                return new BitmapDrawable(Bitmap
                        .createScaledBitmap(bitmap, widthImage, heightImage, true));

            } else {
                return d;
            }

        }catch (OutOfMemoryError e) {
            System.gc();
        }catch (Exception e) {
            e.printStackTrace();
        }

        d = null;

        return d;
    }


}
