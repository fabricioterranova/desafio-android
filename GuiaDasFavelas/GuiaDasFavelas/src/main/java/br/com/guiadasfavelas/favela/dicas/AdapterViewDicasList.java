package br.com.guiadasfavelas.favela.dicas;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.componentes.OptimizedAdapterView;
import br.com.guiadasfavelas.util.FontUtil;

public class AdapterViewDicasList extends OptimizedAdapterView {

    private List<Integer> listIcones;

	public AdapterViewDicasList(Context context, List<String> itens) {
		
		super(context, itens);
        preencheIcones();
	}

    private void preencheIcones() {
        listIcones = new ArrayList<Integer>();
        listIcones.add(R.drawable.dica_1);
        listIcones.add(R.drawable.dica_2);
        listIcones.add(R.drawable.dica_3);
        listIcones.add(R.drawable.dica_4);
        listIcones.add(R.drawable.dica_5);
        listIcones.add(R.drawable.dica_6);
        listIcones.add(R.drawable.dica_7);
        listIcones.add(R.drawable.dica_8);
        listIcones.add(R.drawable.dica_9);
        listIcones.add(R.drawable.dica_10);
        listIcones.add(R.drawable.dica_11);
        listIcones.add(R.drawable.dica_12);
        listIcones.add(R.drawable.dica_13);
        listIcones.add(R.drawable.dica_14);
    }

	public View getView(int position, View view) {

		view = hash.get(position);
		
		if(view == null) {

			// infla o layout para podermos preencher os dados
			view = mInflater.inflate(R.layout.item_dica, null);

			// Pega o item de acordo com a posicao.
            String item = (String)itens.get(position);

            ((TextView) view.findViewById(R.id.itemNomeCurador)).setText(item);
            ((TextView) view.findViewById(R.id.itemNomeCurador)).setTypeface(FontUtil.HELVETICA_M);

            ((TextView) view.findViewById(R.id.iconeCurador)).setBackgroundResource(listIcones.get(position));

            //((TextView) view.findViewById(R.id.itemDescricao)).setText(item);
            //((TextView) view.findViewById(R.id.itemDescricao)).setVisibility(View.INVISIBLE);

			hash.put(position, view);

		} /*else {
			if(animationOk) {
				Animation a = AnimationUtils.loadAnimation(
						context, android.R.anim.fade_in);
				
				a.setDuration(500);
				a.setStartOffset(300);
		    	((ImageView) view.findViewById(R.id.iconeCurador)).setAnimation(a);
			}
		}*/
    	
		return view;
	}
	
}
