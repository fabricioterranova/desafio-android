package br.com.guiadasfavelas.util;

public interface PropsConstants {

	String FOTO_BARRA = "foto.barra";

	String CREATE_DB_MINIMAL = "create.minimal";

	String VIDEO_SPLASH = "video.splash";

	String VIDEOS_FOLDER = "videos.folder";

	String VIDEO_SPLASH_NAME = "video.splash.name";

	String SERVER_URL = "server.url";

	String SERVER_PROJECT_NAME = "server.project.name";

	String SERVICE_SERVICE_EVENTO = "server.service.evento";

	String SERVICE_SERVICE_AGENDA = "server.service.agenda";

	String SERVICE_SERVICE_PALESTRANTES = "server.service.palestrantes";

	String SERVICE_SERVICE_TWITTER = "server.service.twitter";

	String SERVICE_SERVICE_CURADORES = "server.service.curadores";

	String SERVICE_SERVICE_DATAS = "server.service.datas";

	String SERVICE_SERVICE_VERSAO = "server.service.versao";
	
	String URL_APP_GOOGLE_PLAY = "url.app.google.play";
	
	String URL_SHORTER_SERVICE = "url.shorter.service";
	
	String URL_SHORTER_PARAM = "url.shorter.param";
	
	String URL_MAPA = "url.mapa";

}
