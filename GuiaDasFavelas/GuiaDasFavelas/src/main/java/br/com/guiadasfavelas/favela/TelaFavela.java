package br.com.guiadasfavelas.favela;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Arquitetura;
import br.com.guiadasfavelas.entities.Arte;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.GuiaTuristico;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.entities.Mirante;
import br.com.guiadasfavelas.entities.OndeComer;
import br.com.guiadasfavelas.entities.OndeComprar;
import br.com.guiadasfavelas.entities.OndeDancar;
import br.com.guiadasfavelas.entities.VivaFavela;
import br.com.guiadasfavelas.favela.arquitetura.TelaArquitetura;
import br.com.guiadasfavelas.favela.arte.TelaArte;
import br.com.guiadasfavelas.favela.guiaturistico.TelaGuiasTuristicos;
import br.com.guiadasfavelas.favela.mirantes.TelaMirantes;
import br.com.guiadasfavelas.favela.ondecomer.TelaOndeComer;
import br.com.guiadasfavelas.favela.ondecomprar.TelaOndeComprar;
import br.com.guiadasfavelas.favela.ondedancar.TelaOndeDancar;
import br.com.guiadasfavelas.favela.vivafavela.TelaVivaFavela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

public class TelaFavela extends Tela {

    private List<View> listView;
    //private List<Bitmap> listBitmap;

    private static Favela favela;
    private List<ItemFavela> listItemFavela;

	
	public TelaFavela(HomeActivity homeActivity) {
		super(homeActivity);

        listView = new ArrayList<View>();
        //listBitmap = new ArrayList<Bitmap>();
	}

    public void clearResource() {

        for(View v:listView) {
            if(v instanceof ImageView) {
                ImageView iv = (ImageView) v;
                BitmapDrawable bd = (BitmapDrawable) iv.getDrawable();

                if(bd != null) {
                    Bitmap bitmap = bd.getBitmap();

                    iv.setImageDrawable(null);
                    //bitmap.recycle();
                    bitmap = null;
                }

                bd = null;
            }

            v.setOnClickListener(null);
        }

        listView = null;
        System.gc();
    }

    private void carregaBarra(Favela favela) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
            nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(homeActivity.getResources().getString(R.string.onde_dancar));
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }

    }

	public void carregTelaFavela(final Favela favela) {
        this.favela = favela;

        homeActivity.setGerenciadorTela(Constantes.TELA_LOCA);

        homeActivity.setContentView(R.layout.home);

        //para carregar a barra superior
        super.carregaTela(favela, false, null, null);

       // carregaItensFavela();
       // carregaMiolo();
        carregaBotoes();
        carregaBarra(favela);
	}

    private void carregaBotoes() {
        TextView txtComoChegar = (TextView) homeActivity.findViewById(R.id.txtComoChegar);
        TextView txtAnfitrioes = (TextView) homeActivity.findViewById(R.id.txtAnfitrioes);
        TextView txtMirantes = (TextView) homeActivity.findViewById(R.id.txtMirantes);
        TextView txtOndeComer = (TextView) homeActivity.findViewById(R.id.txtOndeComer);
        TextView txtOndeComprar = (TextView) homeActivity.findViewById(R.id.txtOndeComprar);
        TextView txtOndeDancar = (TextView) homeActivity.findViewById(R.id.txtOndeDancar);
        TextView txtVivaFavela = (TextView) homeActivity.findViewById(R.id.txtVivaFavela);
        TextView txtArquitetura = (TextView) homeActivity.findViewById(R.id.txtArquitetura);
        TextView txtArte = (TextView) homeActivity.findViewById(R.id.txtArte);
        TextView txtAnfitrioes2 = (TextView) homeActivity.findViewById(R.id.txtAnfitrioes2);
        TextView txtHistoria = (TextView) homeActivity.findViewById(R.id.txtHistoria);


        txtComoChegar.setTypeface(FontUtil.HELVETICA_M);
        txtAnfitrioes.setTypeface(FontUtil.HELVETICA_M);
        txtMirantes.setTypeface(FontUtil.HELVETICA_M);
        txtOndeComer.setTypeface(FontUtil.HELVETICA_M);
        txtOndeComprar.setTypeface(FontUtil.HELVETICA_M);
        txtOndeDancar.setTypeface(FontUtil.HELVETICA_M);
        txtVivaFavela.setTypeface(FontUtil.HELVETICA_M);
        txtArquitetura.setTypeface(FontUtil.HELVETICA_M);
        txtArte.setTypeface(FontUtil.HELVETICA_M);
        txtAnfitrioes2.setTypeface(FontUtil.HELVETICA_M);
        txtHistoria.setTypeface(FontUtil.HELVETICA_M);

    }

    //transforma todos as atracoes em 'itemFavela'
    private void carregaItensFavela() {
        listItemFavela = new ArrayList<ItemFavela>();

        //listItemFavela.addAll(favela.getAcessos());
        listItemFavela.addAll(favela.getGuiasTuristicos());
        listItemFavela.addAll(favela.getMirantes());
        listItemFavela.addAll(favela.getOndeComerList());
        listItemFavela.addAll(favela.getOndeComprarList());
        listItemFavela.addAll(favela.getOndeDancarList());
        listItemFavela.addAll(favela.getVivaFavelaList());

        if(favela.getArquiteturaList() != null) {
            listItemFavela.addAll(favela.getArquiteturaList());
        }

        if(favela.getArteList() != null) {
            listItemFavela.addAll(favela.getArteList());
        }

    }

    private LinearLayout montarColuna() {
        LinearLayout coluna = new LinearLayout(homeActivity);
        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        coluna.setOrientation(LinearLayout.VERTICAL);

        Resources r = homeActivity.getResources();

        int marginRight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,r.getDisplayMetrics());

        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,r.getDisplayMetrics());

        ll.setMargins(0, marginTop, marginRight, 0);

        coluna.setLayoutParams(ll);

        return coluna;
    }

    private LinearLayout montarLayout() {

        LinearLayout layout = new LinearLayout(homeActivity);

        Resources r = homeActivity.getResources();
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140,r.getDisplayMetrics());

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER_HORIZONTAL);
        layout.setBackgroundResource(R.drawable.canto_arrendondado_bg_favela);


        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,r.getDisplayMetrics());
        int marginBottom = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10,r.getDisplayMetrics());

        layout.setPadding(padding, padding, padding, padding);

        ll.gravity = Gravity.LEFT;

        ll.setMargins(0, 0, 0, marginBottom);

        layout.setLayoutParams(ll);

        return layout;
    }

    private ImageView montarImagem(String path, int i) {
        ImageView image = new ImageView(homeActivity);
        image.setId(i);

        Resources r = homeActivity.getResources();

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 180,r.getDisplayMetrics());
        //int heigth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200,r.getDisplayMetrics());

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        image.setLayoutParams(ll);

        //imagem bg da favela
        InputStream is = null;

        AssetManager assetManager = homeActivity.getAssets();

        Drawable dBg = null;
        boolean imageDefault = false;

        try {

            //Log.v("path",path);
            is = assetManager.open(path);

            try {
                dBg = Drawable.createFromStream(is, "img");
            }catch (OutOfMemoryError e) {
                System.gc();

                try {
                    dBg = Drawable.createFromStream(is, "img");
                }catch (OutOfMemoryError f) {
                    System.gc();

                    try {
                        dBg = Drawable.createFromStream(is, "img");
                    }catch (OutOfMemoryError fg) {

                    }
                }
            }

            //redimensionando a imagem

        }catch (FileNotFoundException fnf){
            Log.v("Erro","Image nao encontrada:"+path);
            //dBg = FavelaUtil.getImageDefault();
            imageDefault = true;
        }catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e) {
            e.printStackTrace();
        }


        if(dBg != null) {

            if(!imageDefault) {


                int widthImage = dBg.getIntrinsicWidth();
                int heigthImage = dBg.getIntrinsicHeight();

                int heigthMod = heigthImage;

                Bitmap bitmap = ((BitmapDrawable) dBg).getBitmap();

                try {
                    heigthMod = heigthImage / (widthImage / width) -  60;


                    try {

                     Drawable d = new BitmapDrawable(Bitmap
                             .createScaledBitmap(bitmap, width, heigthMod, true));
                     image.setImageDrawable(d);
                     d = null;

                    }catch (OutOfMemoryError e) {
                        System.gc();
                        try {
                            Drawable d = new BitmapDrawable(Bitmap
                                    .createScaledBitmap(bitmap, width, heigthMod, true));
                            image.setImageDrawable(d);

                            d = null;

                        }catch (OutOfMemoryError ere) {

                        }
                    }

                } catch (ArithmeticException a) {


                    try {
                        image.setImageDrawable(new BitmapDrawable(Bitmap
                                .createScaledBitmap(bitmap, width, heigthImage, true)));
                    }catch (OutOfMemoryError e) {
                        System.gc();

                        try {
                        image.setImageDrawable(new BitmapDrawable(Bitmap
                                .createScaledBitmap(bitmap, width, heigthImage, true)));
                        }catch (OutOfMemoryError ee) {

                        }
                    }
                }

                ((BitmapDrawable) dBg).getBitmap().recycle();
                bitmap.recycle();
                bitmap = null;

            } else {
                image.setImageDrawable(dBg);
            }

            dBg = null;

            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    clearResource();

                    int i = view.getId();
                    ItemFavela itemFavela = listItemFavela.get(i);

                    //carregando os itens clicados da tela loca
                    if(itemFavela instanceof Mirante) {
                        Mirante mirante = (Mirante) itemFavela;
                        new TelaMirantes(homeActivity).carregaTelaMirantes(mirante, favela, true);
                    } else if(itemFavela instanceof OndeComer) {
                        OndeComer ondeComer = (OndeComer) itemFavela;
                        new TelaOndeComer(homeActivity).carregaTelaOndeComer(ondeComer, favela, true);
                    } else if(itemFavela instanceof OndeComprar) {
                        OndeComprar ondeComprar = (OndeComprar) itemFavela;
                        new TelaOndeComprar(homeActivity).carregaTelaOndeComprar(ondeComprar, favela, true);
                    } else if(itemFavela instanceof VivaFavela) {
                        VivaFavela vivaFavela = (VivaFavela) itemFavela;
                        new TelaVivaFavela(homeActivity).carregaTelaVivaFavela(vivaFavela, favela, true);
                    } else if(itemFavela instanceof OndeDancar) {
                        OndeDancar ondeDancar = (OndeDancar) itemFavela;
                        new TelaOndeDancar(homeActivity).carregaTelaOndeDancar(ondeDancar, favela, true);
                    } else if(itemFavela instanceof GuiaTuristico) {
                        GuiaTuristico guiaTuristico = (GuiaTuristico) itemFavela;
                        new TelaGuiasTuristicos(homeActivity).carregaTelaGuiaTuristico(guiaTuristico, favela, true);
                    } else if(itemFavela instanceof Arte) {
                        Arte arte = (Arte) itemFavela;
                        new TelaArte(homeActivity).carregaTelaArte(arte,favela, true);
                    } else if(itemFavela instanceof Arquitetura) {
                        Arquitetura arquitetura = (Arquitetura) itemFavela;
                        new TelaArquitetura(homeActivity).carregaTelaArquitetura(arquitetura, favela, true);
                    }
                }
            });

            //image.setBackgroundDrawable(dBg);
        }

        assetManager = null;
        listView.add(image);

        return image;
    }

    private TextView montarTexto(String texto) {
        TextView textView = new TextView(homeActivity);

        Resources r = homeActivity.getResources();

        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100,r.getDisplayMetrics());
        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,r.getDisplayMetrics());

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setText(texto);
        textView.setGravity(Gravity.CENTER);

        ll.setMargins(0, marginTop, 0, 0);

        textView.setLayoutParams(ll);

        r = null;
        ll = null;

        return textView;
    }
}
