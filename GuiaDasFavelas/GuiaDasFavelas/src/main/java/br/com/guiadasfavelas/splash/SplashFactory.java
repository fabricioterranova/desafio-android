package br.com.guiadasfavelas.splash;

import android.app.TabActivity;
import android.widget.TextView;

import br.com.guiadasfavelas.R;


/*
 * Usado para 'fabricar' splash de acordo com a necessidade
 * 
 * nesta classe que e definido todos os parametros de 
 * inicializacao do splash e iniciados os Views de cada
 * tipo de splash
 * 
 */

public abstract class SplashFactory {
	
	public static SplashImage instanceSplashImage(TabActivity activity) {
		
		TextView splash = (TextView) activity.findViewById(R.id.splash);
		
		return new SplashImage(splash, activity.getTabHost(), activity);
	}
	
}
