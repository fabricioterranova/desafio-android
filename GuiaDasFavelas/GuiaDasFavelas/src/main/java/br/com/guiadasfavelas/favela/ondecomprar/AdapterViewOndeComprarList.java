package br.com.guiadasfavelas.favela.ondecomprar;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.componentes.OptimizedAdapterView;
import br.com.guiadasfavelas.entities.OndeComprar;
import br.com.guiadasfavelas.util.FavelaUtil;
import br.com.guiadasfavelas.util.FontUtil;

//teste
public class AdapterViewOndeComprarList extends OptimizedAdapterView {

	public AdapterViewOndeComprarList(Context context, List<OndeComprar> itens) {
		
		super(context, itens);
	}
	

	public View getView(int position, View view) {

		view = hash.get(position);
		
		if(view == null) {
		
			// infla o layout para podermos preencher os dados
			view = mInflater.inflate(R.layout.item_onde_comer, null);
			
			// Pega o item de acordo com a posicao.
            OndeComprar item = (OndeComprar)itens.get(position);
			
			final ImageView iconeCurador = ((ImageView) view.findViewById(R.id.iconeCurador));

            iconeCurador.setImageDrawable(FavelaUtil.parseAssetImagePathToDrawableToList(iconeCurador, item.getFotoPrincipal(), R.drawable.sem_foto_ondecomprar));

            ((TextView) view.findViewById(R.id.itemNomeCurador)).setText(item.getNome());
            ((TextView) view.findViewById(R.id.itemNomeCurador)).setTypeface(FontUtil.HELVETICA_M);
			
			hash.put(position, view);
			
		}
    	
		return view;
	}
	
}
