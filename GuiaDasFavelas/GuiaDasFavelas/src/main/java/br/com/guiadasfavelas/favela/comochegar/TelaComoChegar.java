package br.com.guiadasfavelas.favela.comochegar;

import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Acesso;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaComoChegar extends Tela {

    public TelaComoChegar(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }


    private void carregaBarra(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText("Onde Comer");
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    public void carregaTelaOndeComer(Acesso ondeComer, Favela favela, boolean voltarTudo) {
        homeActivity.setContentView(R.layout.onde_comer);

        //para carregar a barra superior
        super.carregaTela(favela, false, ondeComer, Constantes.TIPO_COMO_CHEGAR);

        if(voltarTudo) {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LOCA);
        } else {
            //homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LIST_ONDE_);
        }

        carregaBarra(favela, R.drawable.icone_interno_comochegar);

        TextView especialidadeItem = (TextView) homeActivity.findViewById(R.id.especialidadeItem);
        TextView nomeItem = (TextView) homeActivity.findViewById(R.id.nomeItem);
        TextView enderecoItem = (TextView) homeActivity.findViewById(R.id.enderecoItem);
        TextView comoChegarItem = (TextView) homeActivity.findViewById(R.id.comoChegarItem);
        TextView diferencialItem= (TextView) homeActivity.findViewById(R.id.diferencialItem);
        ImageView imagemItem = (ImageView) homeActivity.findViewById(R.id.imagemItem);

        especialidadeItem.setTypeface(FontUtil.HELVETICA_M);
        nomeItem.setTypeface(FontUtil.HELVETICA_M);
        enderecoItem.setTypeface(FontUtil.HELVETICA_M);
        comoChegarItem.setTypeface(FontUtil.HELVETICA_M);
        diferencialItem.setTypeface(FontUtil.HELVETICA_M);

       // especialidadeItem.setText(Html.fromHtml("<b>Especialidade: </b>"+ondeComer.getEspecialidade()));
       // nomeItem.setText(ondeComer.getNome());
       // enderecoItem.setText(Html.fromHtml("<b>Endereço: </b>"+ondeComer.getEndereco()));
       // comoChegarItem.setText(Html.fromHtml("<b>Como Chegar: </b>"+ondeComer.getComoChegar()));
       // diferencialItem.setText(Html.fromHtml("<b>Diferencial: </b>"+ondeComer.getDiferencial()));

        Drawable d = null;

        AssetManager assetManager = homeActivity.getAssets();

        InputStream is = null;

        try {
            is = assetManager.open(ondeComer.getFotoPrincipal());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.v("Erro", "Image nao encontrada" + ondeComer.getFotoPrincipal());
        }


        try {


            d =  Drawable.createFromStream(is, "img");
        }catch (OutOfMemoryError e) {
            d = null;
            System.gc();

            try {
                d =  Drawable.createFromStream(is, "img");
            } catch (OutOfMemoryError ef) {
                System.gc();
                d =  Drawable.createFromStream(is, "img");
            }
        }

        imagemItem.setImageDrawable(d);
    }
}
