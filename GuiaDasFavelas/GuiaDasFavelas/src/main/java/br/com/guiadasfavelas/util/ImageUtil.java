package br.com.guiadasfavelas.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;


public class ImageUtil {



	public static Bitmap mergeImages(Bitmap c, Bitmap s) { // can add a 3rd parameter 'String loc' if you want to save the new image - left some code to do that at the bottom 
	    Bitmap cs = null; 
	 
	    int width, height = 0; 
	     
	    if(c.getWidth() > s.getWidth()) { 
	      width = c.getWidth(); 
	      height = c.getHeight() + s.getHeight(); 
	    } else { 
	      width = s.getWidth(); 
	      height = c.getHeight() + s.getHeight(); 
	    } 
	 
	    cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
	 
	    Canvas comboImage = new Canvas(cs);
	    
	    Matrix matrix1 = new Matrix();
	    
	    //redimensionando a ajeitando o frame geral
	    matrix1.postScale(1.35F, 1, ((c.getWidth() - c.getHeight()) * 2), 0);
	    comboImage.setMatrix(matrix1);
	 
	    //rodando a imagem principal que estava me trolando
	    Matrix matrix = new Matrix();
	    matrix.postRotate(90, c.getWidth()/2, c.getHeight()/2);
	    
	    comboImage.drawBitmap(c, matrix, new Paint());
	    
	    Bitmap b = Bitmap.createScaledBitmap(s, c.getWidth() - (c.getWidth() - c.getHeight()), 
	    		(s.getHeight()) , true);
	    // Bitmap b = Bitmap.createScaledBitmap(s, c.getWidth() - (c.getWidth() - c.getHeight()), 
		//(s.getHeight() - (s.getHeight() / 3)) , true);
	    
	    //comboImage.drawBitmap(c, 0f, 0f, null); 
	    comboImage.drawBitmap(b, (c.getWidth() - c.getHeight()) / 2, c.getHeight(), new Paint());
	    //comboImage.drawBitmap(b, (c.getWidth() - c.getHeight()) / 2, c.getHeight() + (s.getHeight() / 3), new Paint());
	    
	    return cs; 
	  } 
}
