package br.com.guiadasfavelas.dao;

public interface DaoTest {

	/* usado somente para realizar a primeira coneccao com o banco
	 * de dados para, caso necessite executar algum upgrade, o mesmo
	 * e executa num primeiro momento
	 */
	 void startDatabase();
}
