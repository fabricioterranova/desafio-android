package br.com.guiadasfavelas.util;

public class DataUtil {
	
	private static final String DE = " de ";
	
	private static final String AGOSTO = "Agosto";
	private static final String SETEMBRO = "Setembro";
	
	/*
	 * recebe data no formato DD/MM/AAAA 
	 * e transforma em DD de MMM de AAAA
	 */
	public static String parseDataToBeautifull(String data) {
		
		if(data==null || data.length() < 10)
			return data;
		
		String dia = data.substring(0,2);
		int mes = Integer.parseInt(data.substring(3,5));
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(dia);
		sb.append(DE);
		
		if(mes == 8) {
			sb.append(AGOSTO);
		} else if(mes == 9) {
			sb.append(SETEMBRO);
		}
		
		return sb.toString();
	}

}
