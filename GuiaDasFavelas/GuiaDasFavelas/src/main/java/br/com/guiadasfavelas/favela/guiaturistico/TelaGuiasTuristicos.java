package br.com.guiadasfavelas.favela.guiaturistico;

import android.graphics.Color;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.GuiaTuristico;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FavelaUtil;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaGuiasTuristicos extends Tela {

    public TelaGuiasTuristicos(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {

    }

    private void carregaBarra(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
        iconeSubBarra.setBackgroundResource(iconeImage);


        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.guias_turisticos_locais);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }
    }

    public void carregaTelaGuiaTuristico(GuiaTuristico guiaTuristico, Favela favela, boolean voltarTudo) {
        homeActivity.setContentView(R.layout.guiasturisticos);

        //para carregar a barra superior
        super.carregaTela(favela, true, guiaTuristico, Constantes.TIPO_GUIA_TURISTICO);

        if(voltarTudo) {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LOCA);
        } else {
            homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LIST_GUIA);
        }

        carregaBarra(favela, R.drawable.icone_interno_guiasturisticos);

        TextView especialidadeItem = (TextView) homeActivity.findViewById(R.id.especialidadeItem);
        TextView nomeItem = (TextView) homeActivity.findViewById(R.id.nomeItem);
        TextView enderecoItem = (TextView) homeActivity.findViewById(R.id.enderecoItem);
        //TextView comoChegarItem = (TextView) homeActivity.findViewById(R.id.comoChegarItem);
        TextView diferencialItem= (TextView) homeActivity.findViewById(R.id.diferencialItem);
        ImageView imagemItem = (ImageView) homeActivity.findViewById(R.id.imagemItem);
        ImageView imagemSecundariaItem = (ImageView) homeActivity.findViewById(R.id.imagemSecundariaItem);

        especialidadeItem.setTypeface(FontUtil.HELVETICA_M);
        nomeItem.setTypeface(FontUtil.HELVETICA_M);
        enderecoItem.setTypeface(FontUtil.HELVETICA_M);
//        comoChegarItem.setTypeface(FontUtil.HELVETICA_M);
        diferencialItem.setTypeface(FontUtil.HELVETICA_M);

        especialidadeItem.setText(Html.fromHtml("<b>"+ homeActivity.getResources().getString(R.string.nome) +"</b>"+guiaTuristico.getNome()));
        nomeItem.setText(guiaTuristico.getNome());

        enderecoItem.setText(Html.fromHtml("<b>"+ homeActivity.getResources().getString(R.string.telefone) +"</b>"+guiaTuristico.getTelefone()));
        //comoChegarItem.setText(Html.fromHtml("<b>Como Chegar: </b>"+guiaTuristico.get));
        //diferencialItem.setText(Html.fromHtml("<b>Experiência: </b>"+mirante.getExperiencia()));


        imagemItem.setImageDrawable(FavelaUtil.parseAssetImagePathToDrawable(imagemItem, guiaTuristico.getFotoPrincipal(), R.drawable.sem_foto_guia));
    }

}
