package br.com.guiadasfavelas.componentes;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.List;

import br.com.guiadasfavelas.R;


public abstract class OptimizedAdapterView extends BaseAdapter {

    protected LayoutInflater mInflater;
    protected List<Object> itens;
    protected Context context;

    //substitui a seguinte declaracao: HashMap<Integer, View>;
    protected SparseArray<View> hash;

    //usado por conta do bug de nao aparecer icone nem imagem no primeiro carregamento
    //private boolean animationOk;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public OptimizedAdapterView(Context context, List itens) {
		
		/*Thread t = new Thread(new Runnable(){

			public void run() {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				animationOk = true;
			}});
		
		
		t.start();*/

        hash = new SparseArray<View>();

        this.context = context;
        // Itens que preencheram o listview
        this.itens = itens;
        // responsavel por pegar o Layout do item.
        mInflater = LayoutInflater.from(context);
    }

    /**
     * Retorna a quantidade de itens
     *
     * @return
     */
    public int getCount() {

        if(itens != null && itens.size() > 0) {
            return itens.size();
        } else {
            return 0;
        }
    }

    /**
     * Retorna o item de acordo com a posicao dele na tela.
     *
     * @param position
     * @return
     */
    public Object getItem(int position) {
        return itens.get(position);
    }

    /**
     * Sem implementacao
     *
     * @param position
     * @return
     */
    public long getItemId(int position) {
        return position;
    }

    public abstract View getView(int position, View view);

    public final View getView(int position, View view, ViewGroup parent) {

        view = hash.get(position);

        if(view == null) {

            view = getView(position, view);

            hash.put(position, view);

        } /*else {
			if(animationOk) {
				Animation a = AnimationUtils.loadAnimation(
						context, android.R.anim.fade_in);
				
				a.setDuration(500);
				a.setStartOffset(300);
		    	((ImageView) view.findViewById(R.id.iconeCurador)).setAnimation(a);
			}
		}*/

        view.setAnimation(animation());


        return view;
    }

    public Animation animation() {
        Animation ani = AnimationUtils.loadAnimation(context, R.anim.slide_in_right);
        ani.setDuration(250);
        return ani;
    }

}
