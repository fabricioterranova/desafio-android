package br.com.guiadasfavelas.favela.comochegar;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.entities.Acesso;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.util.AndroidUtil;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.Tela;

import static android.view.animation.Animation.AnimationListener;

/**
 * Created by prenovato on 26/06/13.
 */
public class TelaListComoChegar extends Tela implements AdapterView.OnItemClickListener {

    private Favela favela;
    private ListView listaOndeComer;
    private WebView mapaLocal;
    private ProgressDialog waitingWebView;

    private Button btnComoChegar, btnComoChegarOut, btnRecarregar;
    private RelativeLayout containerComoChegar, containerBtn;
    private LinearLayout containerBotaoComoChegarOut, containerInformacao, containerConteudo;

    public TelaListComoChegar(HomeActivity homeActivity) {
        super(homeActivity);
    }

    @Override
    public void clearResource() {
        favela = null;
        mapaLocal.clearFocus();
        mapaLocal.clearAnimation();
        mapaLocal.clearView();
        mapaLocal = null;
        mapaLocal = null;
        if(waitingWebView != null) {
            waitingWebView.dismiss();
            waitingWebView = null;
        }
    }

    private void carregaBara(Favela favela, int iconeImage) {
        //carregando as fontes
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);
        TextView nomeFuncionalidade = (TextView) homeActivity.findViewById(R.id.nomeFuncionalidade);
        btnComoChegar = (Button) homeActivity.findViewById(R.id.btnComoChegar);
        btnComoChegarOut = (Button) homeActivity.findViewById(R.id.btnComoChegarOut);
        containerComoChegar = (RelativeLayout) homeActivity.findViewById(R.id.containerComoChegar);
        containerBotaoComoChegarOut = (LinearLayout) homeActivity.findViewById(R.id.containerBotaoComoChegarOut);
        containerConteudo = (LinearLayout) homeActivity.findViewById(R.id.containerConteudo);
        containerInformacao = (LinearLayout) homeActivity.findViewById(R.id.containerInformacao);
        containerInformacao.setVisibility(View.INVISIBLE);
        TextView comoChegarText = (TextView) homeActivity.findViewById(R.id.comoChegarText);
        comoChegarText.setTypeface(FontUtil.HELVETICA_B);
       // ImageView iconeSubBarra = (ImageView) homeActivity.findViewById(R.id.iconeSubBarra);
       // iconeSubBarra.setBackgroundResource(iconeImage);

        btnComoChegar.setOnClickListener(this);
        btnComoChegarOut.setOnClickListener(this);



        if(nomeFavelaTxt != null) {
            nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);
            nomeFavelaTxt.setText(favela.getNome());

            //caso o texto seja muito grande
            if(favela.getNome().length() > 25)
                nomeFavelaTxt.setTextSize(TypedValue.COMPLEX_UNIT_DIP,18f);
        }

        if(nomeFuncionalidade != null) {
            nomeFuncionalidade.setTypeface(FontUtil.HELVETICA_B);
            nomeFuncionalidade.setText(R.string.como_chegar);
        }

        //cor da barra
        View corFavela = (View) homeActivity.findViewById(R.id.corFavela);
        if(favela.getCor() != null && !favela.getCor().isEmpty()) {
            corFavela.setBackgroundColor(Color.parseColor(favela.getCor()));
        } else {
            corFavela.setBackgroundColor(Color.parseColor("#000000"));
        }

    }

    private void montarItensComoChegar() {

        for(Acesso acesso:favela.getAcessos()) {
            LinearLayout layout = new LinearLayout(homeActivity);

            Resources r = homeActivity.getResources();

            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layout.setOrientation(LinearLayout.VERTICAL);

            layout.setLayoutParams(ll);

            TextView nomeText = montarTexto1(acesso.getTitulo());
            nomeText.setTypeface(FontUtil.HELVETICA_B, Typeface.BOLD);
            nomeText.setTextSize(18,TypedValue.COMPLEX_UNIT_DIP);
            layout.addView(nomeText);

            String textoDescricao = acesso.getRota() +" - "+acesso.getTipo();
            layout.addView(montarTexto2(textoDescricao));

            containerConteudo.addView(layout);
        }
    }

    private TextView montarTexto1(String texto) {
        TextView textView = new TextView(homeActivity);

        Resources r = homeActivity.getResources();

        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,r.getDisplayMetrics());

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setText(texto);

        ll.setMargins(0, marginTop, 0, 0);

        textView.setLayoutParams(ll);

        return textView;
    }

    private TextView montarTexto2(String texto) {
        TextView textView = new TextView(homeActivity);

        Resources r = homeActivity.getResources();

        int marginTop = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5,r.getDisplayMetrics());
        int marginB = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15,r.getDisplayMetrics());

        LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        textView.setText(texto);

        ll.setMargins(0, marginTop, 0, marginB);

        textView.setLayoutParams(ll);

        return textView;
    }


    public void carregaTelaListOndeComer(Favela favela) {
        this.favela = favela;

        homeActivity.setGerenciadorTela(Constantes.TELA_LIST);

        homeActivity.setContentView(R.layout.comochegar);

        containerBtn = (RelativeLayout) homeActivity.findViewById(R.id.containerBtn);
        mapaLocal = (WebView) homeActivity.findViewById(R.id.mapaLocal);
        btnRecarregar = (Button) homeActivity.findViewById(R.id.btnRecarregar);
        btnRecarregar.setOnClickListener(this);

        //para carregar a barra superior
        super.carregaTela(favela, true, null, null);

        carregaBara(favela, R.drawable.icone_interno_comochegar);
        montarItensComoChegar();
        preCarregaTelaMapa();
    }

    private void preCarregaTelaMapa() {
        if(AndroidUtil.isOnline(homeActivity)) {

            mapaLocal.setVisibility(View.VISIBLE);
            btnRecarregar.setVisibility(View.INVISIBLE);
            //containerBtn.setVisibility(View.INVISIBLE);

            carregarMapa(favela);

        } else {
            AndroidUtil.exibeMensagem(homeActivity, homeActivity.getResources().getString(
                    R.string.semInternet));

            btnRecarregar.setVisibility(View.VISIBLE);
            mapaLocal.setVisibility(View.INVISIBLE);
            btnRecarregar.setOnClickListener(this);

            containerBtn.setVisibility(View.VISIBLE);
        }
    }

    private void carregarMapa(Favela favela) {
        mapaLocal.setInitialScale(10);
        mapaLocal.getSettings().setSupportZoom(true);
        mapaLocal.setWebViewClient(new WebViewClient());
        mapaLocal.getSettings().setJavaScriptEnabled(true);
        //mapaFeira.getSettings().setLoadWithOverviewMode(true);


        String mapaUrl="https://maps.google.com.br/maps?q="+favela.getLatitude()+","+favela.getLongitude();
        mapaUrl = mapaUrl+"&hl=pt-BR&t=h&z=13";

        mapaLocal.loadUrl(mapaUrl);
        mapaLocal.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        //final View zoom = this.mapaFeira.getZoomControls();
        //zoom.setVisibility(View.VISIBLE);
        mapaLocal.getSettings().setBuiltInZoomControls(true);

        mapaLocal.getSettings().setUseWideViewPort(true);

        mapaLocal.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                view.loadUrl(url);

                return false;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if(waitingWebView != null) {
                    waitingWebView.dismiss();
                }
            }

        });


        //carregando barra de progresso
        waitingWebView = new ProgressDialog(homeActivity);
        waitingWebView.setCancelable(true);
        waitingWebView.setMessage(homeActivity.getResources().getString(R.string.carregandoAguarde));

        waitingWebView.show();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == btnComoChegar.getId()) {
            exibeBarra();
        } else if(view.getId() == btnComoChegarOut.getId()) {
            escondeBarra();
        } else if(view.getId() == btnRecarregar.getId()) {
            preCarregaTelaMapa();
        }else {
            super.onClick(view);
        }
    }



    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //new TelaListComoChegar(homeActivity).carregaTelaOndeComer(favela.getOndeComerList().get(i), favela.getNome(), false);
    }

    private void exibeBarra() {


        Animation in = AnimationUtils.loadAnimation(homeActivity, R.anim.slide_in_top);
        in.setDuration(400);

        containerComoChegar.startAnimation(in);
        containerInformacao.setVisibility(View.VISIBLE);
        containerComoChegar.setVisibility(View.VISIBLE);
        containerBotaoComoChegarOut.setVisibility(View.VISIBLE);


        Animation out = AnimationUtils.loadAnimation(homeActivity, android.R.anim.fade_out);
        mapaLocal.startAnimation(out);
        mapaLocal.setVisibility(View.INVISIBLE);

        Animation outt = AnimationUtils.loadAnimation(homeActivity, android.R.anim.fade_out);
        containerBtn.startAnimation(outt);
        containerBtn.setVisibility(View.INVISIBLE);
    }

    private void escondeBarra() {

        Animation out = AnimationUtils.loadAnimation(homeActivity, R.anim.slide_out_down);
        out.setDuration(400);

        out.setAnimationListener(new AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                containerComoChegar.setVisibility(View.INVISIBLE);
                containerInformacao.setVisibility(View.INVISIBLE);
                containerBotaoComoChegarOut.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



            containerComoChegar.startAnimation(out);


            Animation inn = AnimationUtils.loadAnimation(homeActivity, R.anim.fade_in);
            containerBtn.startAnimation(inn);
            containerBtn.setVisibility(View.VISIBLE);

            if(AndroidUtil.isOnline(homeActivity)) {
                Animation in = AnimationUtils.loadAnimation(homeActivity, R.anim.fade_in);
                mapaLocal.startAnimation(in);
                mapaLocal.setVisibility(View.VISIBLE);
            }


    }
}
