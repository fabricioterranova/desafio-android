package br.com.guiadasfavelas.util;

import android.app.Activity;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropsUtil {
	
	private static Properties properties;
	
	public void loadProperties(Activity activity) throws IOException {
		
		if(properties == null) {
			InputStream inputStream = activity.getAssets().open(Constantes.APP_PROPERTIES);
			properties = new Properties();
			properties.load(inputStream);
		}
	}
	
	public static String getString(String key) {
		return properties.getProperty(key, key);
	}
	
	public static boolean getBoolean(String key) {
		
		String prop = properties.getProperty(key, key);
		
		if(prop == null || prop.length()==0) {
			return false;
		} else {
			return prop.equals("true");
		}
	}	

}
