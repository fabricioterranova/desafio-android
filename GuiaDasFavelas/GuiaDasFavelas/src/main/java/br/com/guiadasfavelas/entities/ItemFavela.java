package br.com.guiadasfavelas.entities;

/**
 * Created by Usuario on 6/23/13.
 */
public class ItemFavela {

    private int id;
    private String fotoPrincipal;
    private String fotoSecundaria;
    private String nome;
    private String urlImagem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFotoPrincipal() {
        return fotoPrincipal;
    }

    public void setFotoPrincipal(String fotoPrincipal) {
        this.fotoPrincipal = fotoPrincipal;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFotoSecundaria() {
        return fotoSecundaria;
    }

    public void setFotoSecundaria(String fotoSecundaria) {
        this.fotoSecundaria = fotoSecundaria;
    }

    public String getUrlImagem() {
        return urlImagem;
    }

    public void setUrlImagem(String urlImagem) {
        this.urlImagem = urlImagem;
    }
}
