package br.com.guiadasfavelas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.TextView;

import br.com.guiadasfavelas.util.FontUtil;

public class ProjetoActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.projeto);

        TextView textAbertura = (TextView) this.findViewById(R.id.textAbertura);
        textAbertura.setTypeface(FontUtil.HELVETICA_M);
        textAbertura.setText(Html.fromHtml(getResources().getString(R.string.textAbertura)));
	}

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        fecharAplicativo();

        return true;
    }

    public boolean fecharAplicativo() {
        //Handle the back button

        //Ask the user if they want to quit
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.sair)
                .setMessage(R.string.desejaSair)
                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        ProjetoActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.nao, null)
                .show();

        return true;

    }
}
