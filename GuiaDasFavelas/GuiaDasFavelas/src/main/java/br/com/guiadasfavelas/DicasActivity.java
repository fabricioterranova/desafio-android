package br.com.guiadasfavelas;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Arrays;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.favela.dicas.AdapterViewDicasList;
import br.com.guiadasfavelas.favela.dicas.TelaDica;
import br.com.guiadasfavelas.util.Constantes;

public class DicasActivity extends FragmentActivity implements AdapterView.OnItemClickListener{

    private int gerenciadorTela;
    private String[] dicas;
    private String[] dicasDescricao;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        carregaListDica();
	}

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        String titulo = dicas[i];
        String descricao = dicasDescricao[i];

        new TelaDica(this).carregaTelaDica(titulo, descricao);
    }

    public void carregaListDica() {
        setContentView(R.layout.list_dicas);

        ListView listaDicas = (ListView) findViewById(R.id.listaDicas);

        System.gc();

        dicas = getResources().getStringArray(R.array.dicasArray);
        dicasDescricao = getResources().getStringArray(R.array.dicasDescricao);

        AdapterViewDicasList adapterViewOndeComerList = null;
        adapterViewOndeComerList = new AdapterViewDicasList(getApplicationContext(), Arrays.asList(dicas));


        listaDicas.setOnItemClickListener(this);
        listaDicas.setAdapter(adapterViewOndeComerList);
    }

    public boolean voltar() {
        boolean retorno = true;

        switch(gerenciadorTela) {
            case Constantes.TELA_ITEM_LIST_DICA:carregaListDica();break;
            default:retorno = fecharAplicativo();break;
        }

        return retorno;
    }

    public void setGerenciadorTela(int gerenciadorTela) {
        this.gerenciadorTela = gerenciadorTela;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        boolean retorno = true;
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            retorno = voltar();
        }

        return retorno;
    }

    public boolean fecharAplicativo() {
        //Handle the back button

        //Ask the user if they want to quit
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.sair)
                .setMessage(R.string.desejaSair)
                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        DicasActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.nao, null)
                .show();

        return true;

    }


}
