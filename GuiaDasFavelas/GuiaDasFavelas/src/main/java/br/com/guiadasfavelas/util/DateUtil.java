package br.com.guiadasfavelas.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {
	
	private Date date;

	public DateUtil(Date date) {
		this.date = date;
	}
	
	//volta a hora no formato
	public String getHora() {
		
		try {
			//DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm");  
			DateFormat df = new SimpleDateFormat("HH:mm");
	
			return df.format(date);
		}catch(Exception e) {
			return null;
		}

	}
	
	public String getData() {
		
		try {
			DateFormat df = new SimpleDateFormat("dd/MM/yy");  
	
			return df.format(date);
		}catch(Exception e) {
			return null;
		}
	}
	
	public static Date getDateByGMT(String gmtDate) {
		gmtDate = gmtDate.replace(" GMT", "");
		
		SimpleDateFormat dateFormatLocal = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
		dateFormatLocal.setTimeZone(TimeZone.getTimeZone("GMT"));

		//Time in GMT
		try {
			return dateFormatLocal.parse(gmtDate);
		} catch (ParseException e) {
			Log.v("Teste", "Erro ao parsear a data");
		}

		return null;

	}
}
