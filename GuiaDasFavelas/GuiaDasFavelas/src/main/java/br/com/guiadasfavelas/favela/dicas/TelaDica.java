package br.com.guiadasfavelas.favela.dicas;

import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.guiadasfavelas.DicasActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;

/**
 * Created by prenovato on 27/06/13.
 */
public class TelaDica implements View.OnClickListener {

    private DicasActivity homeActivity;
    //private Button btnVoltar;

    public TelaDica(DicasActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    private void carregaBarra() {


        Button btnVoltar = (Button) homeActivity.findViewById(R.id.btnVoltar);
        btnVoltar.setOnClickListener(this);

    }

    public void carregaTelaDica(String titulo, String descricao) {
        homeActivity.setContentView(R.layout.detalhe_dica);

        carregaBarra();

        //para carregar a barra superior
        homeActivity.setGerenciadorTela(Constantes.TELA_ITEM_LIST_DICA);

        TextView tituloDica = (TextView) homeActivity.findViewById(R.id.tituloDica);
        TextView descricaoDica = (TextView) homeActivity.findViewById(R.id.descricaoDica);
        TextView nomeFavelaTxt = (TextView) homeActivity.findViewById(R.id.nomeFavelaTxt);

        tituloDica.setTypeface(FontUtil.HELVETICA_M);
        descricaoDica.setTypeface(FontUtil.HELVETICA_M);
        nomeFavelaTxt.setTypeface(FontUtil.HELVETICA_B);

        nomeFavelaTxt.setText(titulo);
        tituloDica.setText(Html.fromHtml(titulo));
        descricaoDica.setText(Html.fromHtml(descricao));

    }

    @Override
    public void onClick(View view) {
        homeActivity.voltar();
    }
}
