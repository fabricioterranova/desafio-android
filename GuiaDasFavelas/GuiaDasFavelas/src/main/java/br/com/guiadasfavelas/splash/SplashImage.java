package br.com.guiadasfavelas.splash;

import android.content.Context;
import android.view.View;

public class SplashImage extends Splash {

	
	public SplashImage(View splashView, View viewIn, Context context) {
		super(splashView, viewIn, context);
	}

	@Override
	public void carregaSplash() {
		
		//somente iniciando o splash
		startSplash();
	}

}
