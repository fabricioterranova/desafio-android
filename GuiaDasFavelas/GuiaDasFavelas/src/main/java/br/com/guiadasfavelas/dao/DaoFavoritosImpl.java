package br.com.guiadasfavelas.dao;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.guiadasfavelas.entities.Arquitetura;
import br.com.guiadasfavelas.entities.Arte;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.GuiaTuristico;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.entities.Mirante;
import br.com.guiadasfavelas.entities.OndeComer;
import br.com.guiadasfavelas.entities.OndeComprar;
import br.com.guiadasfavelas.entities.OndeDancar;
import br.com.guiadasfavelas.entities.VivaFavela;
import br.com.guiadasfavelas.util.Constantes;

public class DaoFavoritosImpl extends Dao implements DaoFavoritos {

	protected DaoFavoritosImpl(Context context) {
		super(context);
	}

	public List<ItemFavela> getFavoritos(Favela favela) {
		String q = "select idItemFavela, tipo from Favoritos where idFavela="+favela.getId();
		List<ItemFavela> palestrantesFavoritos = null;
		int ids[] = null;
        String tipos[] = null;

		
		Cursor c = getReadableDatabase().rawQuery(q, null);
		
		if(c.getCount() > 0) {
			
			ids = new int[c.getCount()];
            tipos = new String[c.getCount()];
			
			c.moveToFirst();
			
			for(int i = 0;i<c.getCount();i++) {
				ids[i] = c.getInt(0);
                tipos[i] = c.getString(1);
				
				c.moveToNext();
			}
		}
		
		c.close();
		
		getReadableDatabase().close();
		
		//buscando os palestrantes que contem algum dos ids retornados
		
		if(ids != null) {

            List<ItemFavela> listItemFavela =  new ArrayList<ItemFavela>();


            //listItemFavela.addAll(favela.getAcessos());
            listItemFavela.addAll(favela.getGuiasTuristicos());
            listItemFavela.addAll(favela.getMirantes());
            listItemFavela.addAll(favela.getOndeComerList());
            listItemFavela.addAll(favela.getOndeComprarList());
            listItemFavela.addAll(favela.getOndeDancarList());
            listItemFavela.addAll(favela.getVivaFavelaList());

            if(favela.getArquiteturaList() != null) {
                listItemFavela.addAll(favela.getArquiteturaList());
            }

            if(favela.getArteList() != null) {
                listItemFavela.addAll(favela.getArteList());
            }

			//List<ItemFavela> listPalestrantesFull = DaoFactory.getFavoritos(context).getFavoritos(favela);
			palestrantesFavoritos = new ArrayList<ItemFavela>();
			
			for(int i=0;i<ids.length;i++) {
				for(ItemFavela p:listItemFavela) {
					if(ids[i] == p.getId()) {

                        Log.v("TIPO", tipos[i]);
                        Log.v("NOME", p.getNome());

                        if((tipos[i].equals(Constantes.TIPO_ARQUITETURA) && p instanceof Arquitetura) ||
                           (tipos[i].equals(Constantes.TIPO_ARTE) && p instanceof Arte) ||
                           (tipos[i].equals(Constantes.TIPO_GUIA_TURISTICO) && p instanceof GuiaTuristico) ||
                           (tipos[i].equals(Constantes.TIPO_MIRANTE) && p instanceof Mirante) ||
                           (tipos[i].equals(Constantes.TIPO_ONDE_COMER) && p instanceof OndeComer) ||
                           (tipos[i].equals(Constantes.TIPO_ONDE_COMPRAR) && p instanceof OndeComprar) ||
                           (tipos[i].equals(Constantes.TIPO_ONDE_DANCAR) && p instanceof OndeDancar) ||
                           (tipos[i].equals(Constantes.TIPO_VIVA_FAVELA) && p instanceof VivaFavela)) {

						    palestrantesFavoritos.add(p);
                        }
					}
				}
			}
		}
		
		
		return palestrantesFavoritos;
	}

	public void insertFavorito(ItemFavela itemFavela, String tipo, Favela favela) {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into Favoritos values(null,");
        sb.append(favela.getId());
        sb.append(",");
		sb.append(itemFavela.getId());
        sb.append(",'");
        sb.append(tipo);
		sb.append("')");
		
		getWritableDatabase().execSQL(sb.toString());
		
		getWritableDatabase().close();

        Log.v("INSERT",sb.toString());
    }

	public void deleteFavorito(ItemFavela itemFavela, String tipo) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from Favoritos where idItemFavela = ");
		sb.append(itemFavela.getId());
        sb.append(" AND tipo ='");
        sb.append(tipo);
        sb.append("';");

        Log.v("DELETE",sb.toString());

		getWritableDatabase().execSQL(sb.toString());	
		
		getWritableDatabase().close();
	}

	public boolean isFavorito(ItemFavela itemFavela, String tipo) {
		StringBuilder sb = new StringBuilder();
		sb.append("select idItemFavela from Favoritos where idItemFavela = ");
		sb.append(itemFavela.getId());
        sb.append(" AND tipo ='");
        sb.append(tipo);
        sb.append("';");

		Cursor c = getReadableDatabase().rawQuery(sb.toString(), null);

        Log.v("IS",sb.toString());
        Log.v("IS", String.valueOf(c.getCount()));
		//se possuir algum registro
		if(c.getCount() > 0) {
			c.close();
			getReadableDatabase().close();
			return true;
		} else {
			c.close();
			getReadableDatabase().close();
			return false;
		}
	}
}
