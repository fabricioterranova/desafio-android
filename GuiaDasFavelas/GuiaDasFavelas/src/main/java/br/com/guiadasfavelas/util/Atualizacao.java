package br.com.guiadasfavelas.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Atualizacao {

	//private Context context;

	private String atualVersion;
	
	public static String CACHE_DIR;
	private static AssetManager assetManager;
	private static final String STARTUP_CACHE_DIR = "startupcache";
	private static final String CACHE_TEMP_FOLDER_NAME = "cache_temp";
	private static final String CACHE_FOLDER_NAME = "cache";
	
	
	public Atualizacao(Context context) {
		//this.context = context;
		
		CACHE_DIR = context.getCacheDir().getAbsolutePath();
		
		assetManager = context.getAssets();
	}
	
	public boolean isFirstTime() {
		File cacheDir = new File(CACHE_DIR);
		
		if(cacheDir.list().length > 0) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * recupera um arquivo de cache caso o mesmo nao esteja
	 * na pasta de caches e nem na pasta temp
	 */
	public static void recoverCacheFile(String fileName) {
		
		try {
			InputStream is = assetManager.open(STARTUP_CACHE_DIR+"/"+fileName);
				
	    	OutputStream out = new FileOutputStream(new File(CACHE_DIR+"/"+fileName));
	     
	    	int read = 0;
	    	byte[] bytes = new byte[1024];
	     
	    	while ((read = is.read(bytes)) != -1) {
	    		out.write(bytes, 0, read);
	    	}
	     
	    	is.close();
	    	out.flush();
	    	out.close();
	    	
		} catch (Exception e) {

		}
	}
	
	//copia todos os  arquivos do cache inicial para o aparelho
	public static void firstTimeAppStart() {
		
	    String assets[] = null;
	    
		try {
			assets = assetManager.list(STARTUP_CACHE_DIR);
		} catch (IOException e) {
			e.printStackTrace();
		}

		for(String file:assets) {
			recoverCacheFile(file);
		}
	}
}
