package br.com.guiadasfavelas;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

import java.io.IOException;

import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.splash.SplashFactory;
import br.com.guiadasfavelas.util.AndroidUtil;
import br.com.guiadasfavelas.util.Atualizacao;
import br.com.guiadasfavelas.util.Constantes;
import br.com.guiadasfavelas.util.FontUtil;
import br.com.guiadasfavelas.util.PropsUtil;

public class MainActivity extends TabActivity implements OnTabChangeListener{
	
    
    //private static TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //carregandoProperties
        try {
            new PropsUtil().loadProperties(this);
        } catch(IOException e) {
            Log.v("Erro", "Arquivo de propriedades nao existe");
        }

        SplashFactory.instanceSplashImage(this).carregaSplash();

        new Atualizacao(this);

        new FontUtil(this).carregarFontes();

        //barra de patrocinadores
        LinearLayout patrocinadores = (LinearLayout) findViewById(R.id.patrocinadores);
        patrocinadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AndroidUtil.carregaSite(MainActivity.this, PropsUtil.getString(Constantes.LINK_PROPAGANDA));
            }
        });
        
        //tabbar
        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        tabHost.setOnTabChangedListener(this);

        Drawable d = null;

        // Projetos
        d = getResources().getDrawable(R.drawable.tabbar_icone_projeto);
        Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
        d = new BitmapDrawable(Bitmap
                .createScaledBitmap(bitmap,
                        bitmap.getWidth()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_projeto),
                        bitmap.getHeight()   - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_projeto), true));


        intent = new Intent().setClass(this, ProjetoActivity.class);
        spec = tabHost.newTabSpec("0").setIndicator(getString(R.string.projetos), d)
                .setContent(intent);

        tabHost.addTab(spec);


        // Dicas
        d = getResources().getDrawable(R.drawable.tabbar_icone_dicas);
        bitmap = ((BitmapDrawable) d).getBitmap();
        d = new BitmapDrawable(Bitmap
                .createScaledBitmap(bitmap,
                        bitmap.getWidth()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_dicas),
                        bitmap.getHeight()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_dicas), true));

        intent = new Intent().setClass(this, DicasActivity.class);
        spec = tabHost.newTabSpec("1").setIndicator(getString(R.string.dicas), d)
                .setContent(intent);

        tabHost.addTab(spec);


        // Favelas
        d = getResources().getDrawable(R.drawable.tabbar_icone_favelas);
        bitmap = ((BitmapDrawable) d).getBitmap();
        d = new BitmapDrawable(Bitmap
                .createScaledBitmap(bitmap,
                        bitmap.getWidth() - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_favela),
                        bitmap.getHeight() - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_favela), true));

        intent = new Intent().setClass(this, HomeActivity.class);
        spec = tabHost.newTabSpec("2").setIndicator(getString(R.string.favelas), d)
                .setContent(intent);

        tabHost.addTab(spec);

        // Mapa
        d = getResources().getDrawable(R.drawable.tabbar_icone_mapa);
        bitmap = ((BitmapDrawable) d).getBitmap();
        d = new BitmapDrawable(Bitmap
                .createScaledBitmap(bitmap,
                        bitmap.getWidth()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_mapa_width),
                        bitmap.getHeight()   - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_mapa_height), true));

        intent = new Intent().setClass(this, MapaActivity.class);
        spec = tabHost.newTabSpec("3").setIndicator(getString(R.string.mapa), d)
                .setContent(intent);

        tabHost.addTab(spec);


        // Contato
        d = getResources().getDrawable(R.drawable.tabbar_icone_contato);
        bitmap = ((BitmapDrawable) d).getBitmap();
        d = new BitmapDrawable(Bitmap
                .createScaledBitmap(bitmap,
                        bitmap.getWidth()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_contato),
                        bitmap.getHeight()  - (int) getResources().getDimension(R.dimen.offset_minus_size_icon_tab_contato), true));

        intent = new Intent().setClass(this, ContatoActivity.class);
        spec = tabHost.newTabSpec("4").setIndicator(getString(R.string.contato), d)
                .setContent(intent);

        tabHost.addTab(spec);

        //alterando a cor das tabs
        tabHost.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.tab_bg_selector);
        tabHost.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.tab_bg_selector);
        tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.tab_bg_selector);
        tabHost.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.tab_bg_selector);
        tabHost.getTabWidget().getChildAt(4).setBackgroundResource(R.drawable.tab_bg_selector);

        //texto
        ((TextView) tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title)).setTextColor(Color.parseColor("#ffffff"));
        ((TextView) tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title)).setTextColor(Color.parseColor("#ffffff"));
        ((TextView) tabHost.getTabWidget().getChildAt(2).findViewById(android.R.id.title)).setTextColor(Color.parseColor("#ffffff"));
        ((TextView) tabHost.getTabWidget().getChildAt(3).findViewById(android.R.id.title)).setTextColor(Color.parseColor("#ffffff"));
        ((TextView) tabHost.getTabWidget().getChildAt(4).findViewById(android.R.id.title)).setTextColor(Color.parseColor("#ffffff"));

        tabHost.setCurrentTab(2);

        tabHost.getTabWidget().setStripEnabled(false);

       // AndroidUtil.showDeviceLayoutInfo(this);
    }

    @Override
    public void onTabChanged(String s) {

        //Log.v("Teste", String.valueOf(tabHost.getTabWidget().getTabCount()));

       // int c = Color.parseColor("#272727");

        //tabHost.getTabWidget().getChildAt(0).setBackgroundColor(c);
        //tabHost.getTabWidget().getChildAt(1).setBackgroundColor(c);

        //tabHost.getTabWidget().getChildAt(2).setBackgroundColor(c);
        //tabHost.getTabWidget().getChildAt(3).setBackgroundColor(c);
        //tabHost.getTabWidget().getChildAt(4).setBackgroundColor(c);

        if(s!=null) {
           // int index = Integer.parseInt(s);

            //tabHost.getTabWidget().getChildAt(index).setBackgroundColor(Color.parseColor("#131313"));

            //final TextView tv = (TextView) tabHost.getTabWidget().getChildAt(index).findViewById(android.R.id.title);
            //tv.setTextColor(Color.parseColor("#ffffff"));
        }
    }
}
