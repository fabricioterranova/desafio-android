package br.com.guiadasfavelas.util;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import br.com.guiadasfavelas.HomeActivity;
import br.com.guiadasfavelas.R;
import br.com.guiadasfavelas.dao.DaoFactory;
import br.com.guiadasfavelas.entities.Arquitetura;
import br.com.guiadasfavelas.entities.Arte;
import br.com.guiadasfavelas.entities.Favela;
import br.com.guiadasfavelas.entities.GuiaTuristico;
import br.com.guiadasfavelas.entities.ItemFavela;
import br.com.guiadasfavelas.entities.Mirante;
import br.com.guiadasfavelas.entities.OndeComer;
import br.com.guiadasfavelas.entities.OndeComprar;
import br.com.guiadasfavelas.entities.OndeDancar;
import br.com.guiadasfavelas.entities.VivaFavela;
import br.com.guiadasfavelas.sliding.SlidingMenu;
import br.com.guiadasfavelas.util.facebook.FacebookUtil;

/**
 * Created by prenovato on 27/06/13.
 */
public abstract class Tela implements View.OnClickListener {

    private Button btnVoltar, btnOpcoes, btnFacebook, btnFavoritar;
    private SlidingMenu menu;

    protected HomeActivity homeActivity;

    private ItemFavela itemFavela;
    private Favela favela;
    private String tipo;

    //private FacebookUtil facebookUtil;
    //private Dialog dialogAuth;

    public Tela(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;

    }

    public abstract void clearResource();

    public void carregaTela(final Favela favela, boolean facebook, ItemFavela itemFavela, String tipo) {
        btnVoltar = (Button) homeActivity.findViewById(R.id.btnVoltar);
        btnOpcoes = (Button) homeActivity.findViewById(R.id.btnOpcoes);
        btnFacebook = (Button) homeActivity.findViewById(R.id.btnFacebook);
        btnFavoritar = (Button) homeActivity.findViewById(R.id.btnFavoritar);

        if(btnFavoritar != null) {
            btnFavoritar.setBackgroundResource(R.drawable.icone_favoritar_on);
        }

        if(itemFavela != null) {
            if(DaoFactory.getFavoritos(homeActivity).isFavorito(itemFavela, tipo)) {
                btnFavoritar.setBackgroundResource(R.drawable.icone_favoritar_on);
            } else {
                btnFavoritar.setBackgroundResource(R.drawable.icone_favoritar_off);
            }
        }

        this.itemFavela = itemFavela;
        this.favela = favela;
        this.tipo = tipo;

        if(btnVoltar!=null) {
            btnVoltar.setOnClickListener(this);
        }

        if(btnOpcoes != null) {
            btnOpcoes.setOnClickListener(this);
        }

        if(btnFacebook!=null) {
            if(facebook) {
                btnFacebook.setOnClickListener(this);
                btnFacebook.setVisibility(View.VISIBLE);

                btnFavoritar.setOnClickListener(this);
                btnFavoritar.setVisibility(View.VISIBLE);

            } else {
                btnFacebook.setVisibility(View.INVISIBLE);
                btnFavoritar.setVisibility(View.INVISIBLE);
            }
        }


        menu = new SlidingMenu(homeActivity);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(homeActivity, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);

        ListView list = (ListView)  homeActivity.findViewById(android.R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                limparMenu();

                switch (i) {
                    case 0:homeActivity.carregTelaFavela();break;
                    case 1:homeActivity.carregaTelaFavoritos(favela);break;
                    case 2:homeActivity.carregaTelaHistoria(favela);break;
                    case 3:homeActivity.carregaTelaComoChegar(favela);break;
                    case 4:homeActivity.carregaTelaListGuiaTuristico(favela);break;
                    case 5:homeActivity.carregaTelaListMirantes(favela);break;
                    case 6:homeActivity.carregaTelaListOndeComer(favela);break;
                    case 7:homeActivity.carregaTelaListOndeComprar(favela);break;
                    case 8:homeActivity.carregaTelaListOndeDancar(favela);break;
                    case 9:homeActivity.carregaTelaListVivaFavela(favela);break;
                    case 10:homeActivity.carregaTelaListArte(favela);break;
                    case 11:homeActivity.carregaTelaListArquitetura(favela);break;
                }
            }
        });
    }

    private void limparMenu() {

        menu.destroyDrawingCache();
        menu.clearIgnoredViews();
        menu.clearFocus();

        menu = null;
    }



    @Override
    public void onClick(View view) {

        if(view == btnVoltar) {
            clearResource();
            limparMenu();
            homeActivity.voltar();
        } else  if(view == btnOpcoes) {
            if(!menu.isMenuShowing()) {
                menu.showMenu(true);
            } else {
                //fodase
            }
        } else if(view == btnFavoritar) {
            if(DaoFactory.getFavoritos(homeActivity).isFavorito(itemFavela, tipo)) {
                DaoFactory.getFavoritos(homeActivity).deleteFavorito(itemFavela, tipo);
                btnFavoritar.setBackgroundResource(R.drawable.icone_favoritar_off);
            } else {
                DaoFactory.getFavoritos(homeActivity).insertFavorito(itemFavela, tipo, favela);
                btnFavoritar.setBackgroundResource(R.drawable.icone_favoritar_on);
            }


        }  else if(view == btnFacebook) {
            facebookar();
        }
    }

    private void facebookar() {
        if(AndroidUtil.isOnline(homeActivity)) {
            FacebookUtil facebookUtil = new FacebookUtil(homeActivity);

            //String name = itemFavela.getNome();
            //String link = shareable.getFacebookPost().getLink();
            String link = "http://www.guiadasfavelas.com";
            //String caption = shareable.getFacebookPost().getCaption();
            //String description = itemFavela.get
            //String picture = shareable.getFacebookPost().getPicture();

            //facebookUtil.requestAutenticaoFacebook(name, link, caption, description, picture);

            //facebookUtil.requestAutenticaoFacebook(name, link, name, name, name);


            //carregando os itens clicados da tela loca
            if(itemFavela instanceof Mirante) {
                Mirante mirante = (Mirante) itemFavela;

                facebookUtil.requestAutenticaoFacebook(mirante.getNome(), link, mirante.getLocalizacao(), mirante.getRota(), mirante.getUrlImagem());
            } else if(itemFavela instanceof OndeComer) {
                OndeComer ondeComer = (OndeComer) itemFavela;
                facebookUtil.requestAutenticaoFacebook(ondeComer.getNome(), link, ondeComer.getDiferencial(), ondeComer.getDiferencial(), ondeComer.getUrlImagem());
            } else if(itemFavela instanceof OndeComprar) {
                OndeComprar ondeComprar = (OndeComprar) itemFavela;
                facebookUtil.requestAutenticaoFacebook(ondeComprar.getNome(), link, ondeComprar.getDiferencial(), ondeComprar.getServico(), ondeComprar.getUrlImagem());
            } else if(itemFavela instanceof VivaFavela) {
                VivaFavela vivaFavela = (VivaFavela) itemFavela;
                facebookUtil.requestAutenticaoFacebook(vivaFavela.getNome(), link, vivaFavela.getAtracao(), vivaFavela.getExperiencia(), vivaFavela.getUrlImagem());
            } else if(itemFavela instanceof OndeDancar) {
                OndeDancar ondeDancar = (OndeDancar) itemFavela;
                facebookUtil.requestAutenticaoFacebook(ondeDancar.getNome(), link, ondeDancar.getAtracao(), ondeDancar.getExperiencia(), ondeDancar.getUrlImagem());
            } else if(itemFavela instanceof GuiaTuristico) {
                GuiaTuristico guiaTuristico = (GuiaTuristico) itemFavela;
                facebookUtil.requestAutenticaoFacebook(guiaTuristico.getNome(), link, "", guiaTuristico.getTelefone(), guiaTuristico.getUrlImagem());
            } else if(itemFavela instanceof Arte) {
                Arte arte = (Arte) itemFavela;
                facebookUtil.requestAutenticaoFacebook(arte.getNome(), link, arte.getDiferencial(), arte.getAtracao(), arte.getUrlImagem());
            } else if(itemFavela instanceof Arquitetura) {
                Arquitetura arquitetura = (Arquitetura) itemFavela;
                facebookUtil.requestAutenticaoFacebook(arquitetura.getNome(), link, arquitetura.getAtracao(), arquitetura.getExperiencia(), arquitetura.getUrlImagem());
            } else {
                facebookUtil.requestAutenticaoFacebook(itemFavela.getNome(), link, itemFavela.getNome(), itemFavela.getNome(), itemFavela.getUrlImagem());
            }
        } else {
            AndroidUtil.exibeMensagem(homeActivity, homeActivity.getResources().getString(R.string.semInternet));
        }
    }
}
