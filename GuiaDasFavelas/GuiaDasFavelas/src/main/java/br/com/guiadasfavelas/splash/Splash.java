package br.com.guiadasfavelas.splash;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import br.com.guiadasfavelas.util.Constantes;

public abstract class Splash {
	
	protected View splashView;
	protected View viewIn;
	protected Context context;
	
	public Splash(View splashView, View viewIn, Context context) {
		this.splashView = splashView;
		this.context = context;
		this.viewIn = viewIn;
	}
	
	protected void startSplash() {
		splashView.setVisibility(View.VISIBLE);
		
		Thread splashTime = new Thread(new Runnable() {

			public void run() {

				try {
					Thread.sleep(Constantes.SPLASH_TIMEOUT);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				// enviando a mensagem de fechanto do splash
				splashOff.sendEmptyMessage(0);

			}
		});
		
		splashTime.start();
	}
	
	protected void hideSplah() {
		
		splashOff.sendEmptyMessage(0);
	}
	
	// fechando o splash
	public Handler splashOff = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Animation in = AnimationUtils.loadAnimation(context,
					android.R.anim.fade_in);
			in.setDuration(2000);

			viewIn.startAnimation(in);
			viewIn.setVisibility(View.VISIBLE);

			Animation out = AnimationUtils.loadAnimation(context,
					android.R.anim.fade_out);
			out.setInterpolator(context,
					android.R.anim.accelerate_decelerate_interpolator);
			out.setDuration(2000);

            out.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {}

                @Override
                public void onAnimationEnd(Animation animation) {
                    BitmapDrawable bd = ((BitmapDrawable)splashView.getBackground());
                    splashView.setBackgroundDrawable(null);
                    bd.getBitmap().recycle();
                    bd = null;
                    splashView = null;

                }

                @Override
                public void onAnimationRepeat(Animation animation) {}
            });

			splashView.startAnimation(out);
			splashView.setVisibility(View.INVISIBLE);

            out = null;
            in = null;
            viewIn = null;
		}
	};
	
	public abstract void carregaSplash();

}
