package br.com.guiadasfavelas;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import br.com.guiadasfavelas.util.AndroidUtil;

public class MapaActivity extends FragmentActivity implements View.OnClickListener{

    private ProgressDialog waitingWebView;

    //para sanar o fechamento da progress bar com o redirecionamento do site para outro
    private int redirectCount;

    private void carregaTelaMapa() {

        Button btnRecarregar = (Button )this.findViewById(R.id.btnRecarregar);
        WebView mapa = (WebView) this.findViewById(R.id.mapa);

        if(AndroidUtil.isOnline(this)) {

            mapa.setVisibility(View.VISIBLE);
            btnRecarregar.setVisibility(View.INVISIBLE);

            mapa.setInitialScale(10);
            mapa.getSettings().setSupportZoom(true);
            mapa.setWebViewClient(new WebViewClient());
            mapa.getSettings().setJavaScriptEnabled(true);
            //mapaFeira.getSettings().setLoadWithOverviewMode(true);
            mapa.loadUrl("http://goo.gl/maps/SASnI");
            mapa.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
            //final View zoom = this.mapaFeira.getZoomControls();
            //zoom.setVisibility(View.VISIBLE);
            mapa.getSettings().setBuiltInZoomControls(true);

            mapa.getSettings().setUseWideViewPort(true);

            mapa.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    view.loadUrl(url);

                    return false;
                }

                @Override
                public void onPageFinished(WebView view, String url) {

                    if(redirectCount == 1) {
                        waitingWebView.dismiss();
                    }

                    redirectCount++;
                }

            });

            //carregando barra de progresso
            waitingWebView = new ProgressDialog(this);
            waitingWebView.setCancelable(true);
            waitingWebView.setMessage(this.getResources().getString(R.string.carregandoAguarde));

            waitingWebView.show();

        } else {
            AndroidUtil.exibeMensagem(this, this.getResources().getString(
                    R.string.semInternet));

            btnRecarregar.setVisibility(View.VISIBLE);
            mapa.setVisibility(View.INVISIBLE);
            btnRecarregar.setOnClickListener(this);
        }
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mapa);

        carregaTelaMapa();

        //mapaFeira.setImageBitmap(BitmapFactory.decodeResource(homeActivity.getResources(), R.drawable.mapa_evento2));
        //mapa.setMaxZoom(4f);

	}

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        fecharAplicativo();

        return true;
    }

    public boolean fecharAplicativo() {
        //Handle the back button

        //Ask the user if they want to quit
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.sair)
                .setMessage(R.string.desejaSair)
                .setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {


                    public void onClick(DialogInterface dialog, int which) {

                        //Stop the activity
                        MapaActivity.this.finish();
                    }

                })
                .setNegativeButton(R.string.nao, null)
                .show();

        return true;

    }

    @Override
    public void onClick(View view) {
        carregaTelaMapa();
    }
}
