package br.com.guiadasfavelas.entities;

import java.util.List;

public class Favela {

	private int id;
	private String nome;
	private String sobrenome;
	private String fotoPrincipal;
	private String historia;
    private String cor;
	private List<Mirante> mirantes;
	private List<GuiaTuristico> guiasTuristicos;
	private List<Acesso> acessos;
	private List<OndeDancar> ondeDancarList;
	private List<OndeComer> ondeComerList;
	private List<VivaFavela> vivaFavelaList;
	private List<OndeComprar> ondeComprarList;
    private List<Arte> arteList;
    private List<Arquitetura> arquiteturaList;
    private String latitude;
    private String longitude;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getHistoria() {
		return historia;
	}

	public void setHistoria(String historia) {
		this.historia = historia;
	}

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<Mirante> getMirantes() {
		return mirantes;
	}

	public void setMirantes(List<Mirante> mirantes) {
		this.mirantes = mirantes;
	}

	public String getFotoPrincipal() {
		return fotoPrincipal;
	}

	public void setFotoPrincipal(String fotoPrincipal) {
		this.fotoPrincipal = fotoPrincipal;
	}

    public List<Arte> getArteList() {
        return arteList;
    }

    public void setArteList(List<Arte> arteList) {
        this.arteList = arteList;
    }

    public List<Arquitetura> getArquiteturaList() {
        return arquiteturaList;
    }

    public void setArquiteturaList(List<Arquitetura> arquiteturaList) {
        this.arquiteturaList = arquiteturaList;
    }

    public List<GuiaTuristico> getGuiasTuristicos() {
		return guiasTuristicos;
	}

	public void setGuiasTuristicos(List<GuiaTuristico> guiasTuristicos) {
		this.guiasTuristicos = guiasTuristicos;
	}

	public List<Acesso> getAcessos() {
		return acessos;
	}

	public void setAcessos(List<Acesso> acessos) {
		this.acessos = acessos;
	}

	public List<OndeDancar> getOndeDancarList() {
		return ondeDancarList;
	}

	public void setOndeDancarList(List<OndeDancar> ondeDancarList) {
		this.ondeDancarList = ondeDancarList;
	}

	public List<OndeComer> getOndeComerList() {
		return ondeComerList;
	}

	public void setOndeComerList(List<OndeComer> ondeComerList) {
		this.ondeComerList = ondeComerList;
	}

	public List<VivaFavela> getVivaFavelaList() {
		return vivaFavelaList;
	}

	public void setVivaFavelaList(List<VivaFavela> vivaFavelaList) {
		this.vivaFavelaList = vivaFavelaList;
	}

	public List<OndeComprar> getOndeComprarList() {
		return ondeComprarList;
	}

	public void setOndeComprarList(List<OndeComprar> ondeComprarList) {
		this.ondeComprarList = ondeComprarList;
	}

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }
}
