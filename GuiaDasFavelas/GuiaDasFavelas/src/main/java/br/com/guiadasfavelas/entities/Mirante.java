package br.com.guiadasfavelas.entities;

public class Mirante extends ItemFavela{

	private String localizacao;
	private String rota;

	public String getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public String getRota() {
		return rota;
	}

	public void setRota(String rota) {
		this.rota = rota;
	}

}
