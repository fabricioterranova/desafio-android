package br.com.guiadasfavelas.util;

public class StringUtil {

	
	public static final String SLASH = "/";
	
	public static String escape(String text) {
		if (text == null)
			return null;

		return text.replaceAll("\\%", "%25").replaceAll("\\<", "%3C")
				.replaceAll("\\>", "%3E").replaceAll("\\#", "%23")
				.replaceAll("\\ ", "%20").replaceAll("\\{", "%7B")
				.replaceAll("\\}", "%7D").replaceAll("\\|", "%7C")
				.replaceAll("\\\\", "%5C").replaceAll("\\^", "%5E")
				.replaceAll("\\~", "%7E").replaceAll("\\[", "%5B")
				.replaceAll("\\]", "%5D").replaceAll("\\`", "%60")
				.replaceAll("\\;", "%3B").replaceAll("\\/", "%2F")
				.replaceAll("\\:", "%3A").replaceAll("\\@", "%40")
				.replaceAll("\\$", "%24").replaceAll("ã", "a")
				.replaceAll("é", "e").replaceAll("í", "i").replaceAll("ó", "o")
				.replaceAll("ú", "u").replaceAll("á", "a").replaceAll("ô", "o")
				.replaceAll("ç", "c");
	}
	

}
