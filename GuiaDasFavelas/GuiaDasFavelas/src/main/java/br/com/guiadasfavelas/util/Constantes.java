package br.com.guiadasfavelas.util;


public class Constantes {
	
	//milisegundos
	public static final int SPLASH_TIMEOUT = 3000;
	public static int DELAY_PATROCINADOR = 3000;
	
	public static final String APP_PROPERTIES = "app.properties";
	
	// informacoes do banco
	public static final String DATABASE_NAME = "guiadasfavelas2.db";
	public static final int DATABASE_VERSION = 1;
	
	public static final String QUERY_TEST = "SELECT date('now')";
    public static final String LINK_PROPAGANDA = "link.propaganda";

	
	//telas
	public static final byte TELA_MENU_PRINCIPAL = 1;
    public static final byte TELA_LOCA = 2;
    public static final byte TELA_LIST = 3;

    public static final byte TELA_ITEM_LIST_ONDE_COMER = 4;
    public static final byte TELA_ITEM_LIST_ONDE_COMPRAR = 6;
    public static final byte TELA_ITEM_LIST_VIVA_FAVELA = 7;
    public static final byte TELA_ITEM_LIST_MIRANTES = 8;
    public static final byte TELA_ITEM_LIST_GUIA = 9;
    public static final byte TELA_ITEM_LIST_ONDE_DANCAR = 10;
    public static final byte TELA_ITEM_LIST_DICA = 11;
    public static final byte TELA_ITEM_LIST_FAVORITOS = 12;
    public static final byte TELA_PRINCIPAL = 13;

    public static final String TIPO_ONDE_COMER = "comer";
    public static final String TIPO_ONDE_DANCAR = "dancar";
    public static final String TIPO_VIVA_FAVELA = "vivafavela";
    public static final String TIPO_ARQUITETURA = "arquitetura";
    public static final String TIPO_ARTE = "arte";
    public static final String TIPO_FAVORITO = "favorito";
    public static final String TIPO_MIRANTE = "mirante";
    public static final String TIPO_COMO_CHEGAR = "comochegar";
    public static final String TIPO_GUIA_TURISTICO= "guia";
    public static final String TIPO_ONDE_COMPRAR = "comprar";


    public static final byte TELA_ITEM_LOCA = 5;
	
	//arquivos json de cache
	public final static String CACHE_FAVELAS = "CACHE_FAVELAS";



}
