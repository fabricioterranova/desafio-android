package br.com.guiadasfavelas.dao;

import android.content.Context;

import java.util.List;

import br.com.guiadasfavelas.entities.Favela;

public interface DaoFavelas {
	
	public List<Favela> getFavelas(Context context);

}
