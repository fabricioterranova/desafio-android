package br.com.guiadasfavelas.dao;

import android.content.Context;

/*
 * 
 * Utilizando o padrao factory para
 * produzir objs Dao
 * 
 */

public abstract class DaoFactory {
	
	public static DaoTest getDaoTest(Context context) {
		return new DaoTestImpl(context);
	}
	
	public static DaoFavelas getFavelas(Context context) {
		return new DaoFavelasImpl();
	}

    public static DaoFavoritos getFavoritos(Context context) {
        return new DaoFavoritosImpl(context);
    }
	

}
