package com.fabriciooliveira.concretedesafio.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by fabriciooliveira on 11/24/15.
 */
public class Shot implements Parcelable {

    private int id;

    private String title;

    private String description;

    @SerializedName("views_count")
    private long viewsCount;

    private int commentsCount;

    @SerializedName("created_at")
    private String createdAt;

    private String urlImage;

    private User user;

    public Shot(){

    }

    public Shot(String title, String description, long viewsCount, String urlImage, String createdAt, int commentsCount, User user) {
        this.title = title;
        this.description = description;
        this.viewsCount = viewsCount;
        this.urlImage = urlImage;
        this.createdAt = createdAt;
        this.commentsCount = commentsCount;
        this.user = user;
    }

    public Shot(Parcel input) {
        this.title = input.readString();
        this.description = input.readString();
        this.viewsCount = input.readLong();
        this.urlImage = input.readString();
        this.createdAt = input.readString();
        this.commentsCount = input.readInt();
        this.user = input.readParcelable(User.class.getClassLoader());
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeLong(this.viewsCount);
        dest.writeString(this.urlImage);
        dest.writeString(this.createdAt);
        dest.writeInt(this.commentsCount);
        dest.writeParcelable(this.user, flags);
    }

    public static final Creator<Shot> CREATOR = new Creator<Shot>() {
        @Override
        public Shot createFromParcel(Parcel source) {
            return new Shot(source);
        }

        @Override
        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };
}
