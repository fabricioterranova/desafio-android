package com.fabriciooliveira.concretedesafio.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fabriciooliveira.concretedesafio.R;
import com.fabriciooliveira.concretedesafio.domain.Shot;
import com.fabriciooliveira.concretedesafio.network.VolleySingleton;
import com.fabriciooliveira.concretedesafio.ui.ShotDetailsActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 11/24/15.
 */
public class AdapterShot extends RecyclerView.Adapter<AdapterShot.ShotsViewHolder> {

    public static final String ID_USER = "id_user";

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Shot> mListShots = new ArrayList<Shot>();
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader = VolleySingleton.getInstance().getImageLoader();
    //private DateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public AdapterShot(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListShots(List<Shot> listShots) {
        mListShots = listShots;

        notifyItemChanged(0, mListShots.size());
    }

    @Override
    public ShotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.shot_line, parent, false);
        ShotsViewHolder shotsViewHolder = new ShotsViewHolder(view);

        return shotsViewHolder;
    }

    @Override
    public void onBindViewHolder(ShotsViewHolder holder, int position) {
        final Shot shot = mListShots.get(position);

        if (mImageLoader == null) {
            mImageLoader = VolleySingleton.getInstance().getImageLoader();
        }

        holder.mImageShot.setImageUrl(shot.getUrlImage(), mImageLoader);

        holder.mTitle.setText("Title: " + shot.getTitle());
        holder.mViewCount.setText("View Counts: " + String.valueOf(shot.getViewsCount()));
        holder.mCreatedAt.setText("Created at: " + shot.getCreatedAt());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ShotDetailsActivity.class);
                intent.putExtra(ID_USER, shot.getId());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListShots.size();
    }

    static class ShotsViewHolder extends RecyclerView.ViewHolder {

        NetworkImageView mImageShot;
        TextView mTitle;
        TextView mViewCount;
        TextView mCreatedAt;
        View mView;

        public ShotsViewHolder(View view) {
            super(view);

            mImageShot = (NetworkImageView) view.findViewById(R.id.imageShot);
            mTitle =  (TextView) view.findViewById(R.id.txtTitle);
            mViewCount = (TextView) view.findViewById(R.id.txt_view_count);
            mCreatedAt = (TextView) view.findViewById(R.id.txt_created_at);
            mView =  view.findViewById(R.id.cell);

        }
    }

}
