package com.fabriciooliveira.concretedesafio.domain;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by fabriciooliveira on 11/26/15.
 */
public class User implements Parcelable {

    private String name;

    private String username;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public User() {

    }

    public User(String name, String username, String avatarUrl) {
        this.name = name;
        this.username = username;
        this.avatarUrl = avatarUrl;
    }

    protected User(Parcel in) {
        name = in.readString();
        username = in.readString();
        avatarUrl = in.readString();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(name);
        dest.writeString(avatarUrl);
    }
}
