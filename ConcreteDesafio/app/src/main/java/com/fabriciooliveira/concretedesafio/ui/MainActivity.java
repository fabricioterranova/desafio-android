package com.fabriciooliveira.concretedesafio.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fabriciooliveira.concretedesafio.AppController;
import com.fabriciooliveira.concretedesafio.R;
import com.fabriciooliveira.concretedesafio.adapter.AdapterShot;
import com.fabriciooliveira.concretedesafio.commom.Constants;
import com.fabriciooliveira.concretedesafio.domain.Shot;
import com.fabriciooliveira.concretedesafio.network.VolleySingleton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String SHOT_STATE = "shot_state";

    private RecyclerView mRecyclerViewShots;
    private AdapterShot mAdapterShot;
    private ArrayList<Shot> mListShots = new ArrayList<Shot>();
    private VolleySingleton mVolleySingleton;
    private RequestQueue mRequestQueue;

    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private ProgressBar mProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mRecyclerViewShots = (RecyclerView) findViewById(R.id.recyclerview_id);
        mRecyclerViewShots.setLayoutManager(new LinearLayoutManager(this));

        mAdapterShot = new AdapterShot(this);
        mRecyclerViewShots.setAdapter(mAdapterShot);

        if (savedInstanceState != null) {
            mListShots = savedInstanceState.getParcelableArrayList(SHOT_STATE);
            mAdapterShot.setListShots(mListShots);
            mProgressBar.setVisibility(View.INVISIBLE);
        } else {
            sendRequestForShots();
        }

    }

    private void sendRequestForShots() {
        mListShots.clear();

        mProgressBar.setVisibility(View.VISIBLE);

        JsonArrayRequest requestShots = new JsonArrayRequest(Constants.URL_SHOTS, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                mProgressBar.setVisibility(View.INVISIBLE);
                mListShots = parseJSONRequest(jsonArray);
                mAdapterShot.setListShots(mListShots);

            }
        }, new Response.ErrorListener(){
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.INVISIBLE);

                }

            });

        AppController.getInstance().addToRequestQueue(requestShots);
    }

    private ArrayList<Shot> parseJSONRequest(JSONArray jsonArray) {

        if (jsonArray == null || jsonArray.length() == 0) {
            return null;
        }

        Gson gson = new Gson();

        for(int i = 0; i<jsonArray.length(); i++){
            try{
                JSONObject currentShot = jsonArray.getJSONObject(i);

                Shot shot = gson.fromJson(currentShot.toString(), Shot.class);

                if(currentShot.has("images") && !currentShot.isNull("images")){
                    JSONObject images = currentShot.getJSONObject("images");
                    if(images.has("teaser") && images != null){
                        shot.setUrlImage(images.getString("teaser"));
                    }
                }

                Date date = null;
                String createdDate = currentShot.getString("created_at");

                try{
                    date = mDateFormat.parse(createdDate);
                }catch(ParseException e){
                    e.printStackTrace();
                }

                shot.setCreatedAt(mDateFormat.format(date));

                mListShots.add(shot);

            }catch(JSONException e){
                e.printStackTrace();
            }
        }

        return mListShots;

    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putParcelableArrayList(SHOT_STATE, mListShots);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
