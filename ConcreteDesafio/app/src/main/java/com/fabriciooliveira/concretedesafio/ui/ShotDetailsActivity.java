package com.fabriciooliveira.concretedesafio.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.fabriciooliveira.concretedesafio.AppController;
import com.fabriciooliveira.concretedesafio.R;
import com.fabriciooliveira.concretedesafio.adapter.AdapterShot;
import com.fabriciooliveira.concretedesafio.commom.Constants;
import com.fabriciooliveira.concretedesafio.domain.Shot;
import com.fabriciooliveira.concretedesafio.domain.User;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import static com.fabriciooliveira.concretedesafio.commom.Constants.*;

/**
 * Created by fabriciooliveira on 11/24/15.
 */
public class ShotDetailsActivity extends AppCompatActivity {

    private int mIdUser;
    private ProgressDialog mProgressDialog;
    private TextView mName, mUsername;
    private NetworkImageView mImageShot;

    private ImageLoader mImageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        mIdUser = intent.getIntExtra(AdapterShot.ID_USER, 0);

        setupViewElements();

        mImageLoader = AppController.getInstance().getImageLoader();

        mProgressDialog = new ProgressDialog(this);

        mProgressDialog.setMessage(getString(R.string.loading));
        mProgressDialog.show();

        sendRequest();
    }

    private void sendRequest() {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET,
                URL_FOR_LIST + DASH + mIdUser + QUESTION_MARK + ACCESS + TOKEN,
                "",
                new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                hideDialog();

                Gson gson = new Gson();
                Shot shot = gson.fromJson(response.toString(), Shot.class);

                populateValuesOnUserView(shot);

                hideDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_SHORT).show();

                hideDialog();
            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void hideDialog(){
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        hideDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void setupViewElements(){
        mName = (TextView) findViewById(R.id.text_name);
        mUsername = (TextView) findViewById(R.id.text_username);
        mImageShot = (NetworkImageView) findViewById(R.id.imageShot);
    }

    private void populateValuesOnUserView(Shot shot){
        mName.setText("Name: " + shot.getUser().getName());
        mUsername.setText("Username: " + shot.getUser().getUsername());
        mImageShot.setImageUrl(shot.getUser().getAvatarUrl(), mImageLoader);
    }

}
