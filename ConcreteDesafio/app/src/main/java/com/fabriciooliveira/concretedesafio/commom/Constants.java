package com.fabriciooliveira.concretedesafio.commom;

/**
 * Created by fabriciooliveira on 11/24/15.
 */
public class Constants {

    public static final String URL_FOR_LIST = "https://api.dribbble.com/v1/shots";

    public static final String TOKEN = "04b3ee0419f6e14802cd2610ce71b7c16e5b4bc8883432cb2e01821c2dd0d7f7";

    public static final String MAXIMUM_SHOT_LIST_RESULTS = "&page=1&per_page=30";

    public static final String QUESTION_MARK = "?";

    public static final String CONCATENATION = "&";

    public static final String ACCESS = "access_token=";

    public static final String DASH = "/";


    public static final String URL_SHOTS = URL_FOR_LIST + QUESTION_MARK + ACCESS + TOKEN + MAXIMUM_SHOT_LIST_RESULTS;

    //https://api.dribbble.com/v1/shots/2370009?access_token=04b3ee0419f6e14802cd2610ce71b7c16e5b4bc8883432cb2e01821c2dd0d7f7



}
