package com.fabriciooliveira.concretedesafio;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.fabriciooliveira.concretedesafio.util.BitMapCache;

/**
 * Created by fabriciooliveira on 11/24/15.
 */
public class AppController extends Application {

    private static final String TAG = AppController.class.getSimpleName();

    private static AppController mAppController;

    private RequestQueue mRequestQueue;

    private ImageLoader mImageLoader;

    @Override
    public void onCreate(){
        super.onCreate();
        mAppController = this;
    }

    public static synchronized AppController getInstance(){
        return mAppController;
    }

    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null){
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        getRequestQueue();

        if(mImageLoader == null){
            mImageLoader = new ImageLoader(mRequestQueue, new BitMapCache());
        }

        return mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> request, String tag){
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    public <T> void addToRequestQueue(Request<T> request){
        request.setTag(TAG);
        getRequestQueue().add(request);
    }


}
