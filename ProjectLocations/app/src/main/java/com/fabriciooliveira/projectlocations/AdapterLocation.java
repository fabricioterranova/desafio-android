package com.fabriciooliveira.projectlocations;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by fabriciooliveira on 11/16/15.
 */
public class AdapterLocation extends RecyclerView.Adapter<AdapterLocation.ViewHolderLocations> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Place> mListLocation = new ArrayList<Place>();

    public AdapterLocation(Context context) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);

    }

    public void setListLocation(ArrayList<Place> listLocation) {
        mListLocation = listLocation;

        notifyItemChanged(0, listLocation.size());
    }


    @Override
    public ViewHolderLocations onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_places, parent, false);
        ViewHolderLocations viewHolderLocations = new ViewHolderLocations(view);

        return viewHolderLocations;
    }

    @Override
    public void onBindViewHolder(ViewHolderLocations holder, int position) {
        final Place place = mListLocation.get(position);

        holder.mTextviewPlace.setText(place.getName());

    }

    @Override
    public int getItemCount() {
        return mListLocation.size();
    }

    static class ViewHolderLocations extends RecyclerView.ViewHolder{

        TextView mTextviewPlace;

        public ViewHolderLocations(View itemView) {
            super(itemView);

            mTextviewPlace = (TextView) itemView.findViewById(R.id.place_name);
        }
    }
}
