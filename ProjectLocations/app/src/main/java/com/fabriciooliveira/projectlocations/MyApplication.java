package com.fabriciooliveira.projectlocations;

import android.app.Application;
import android.content.Context;

/**
 * Created by fabriciooliveira on 11/20/15.
 */
public class MyApplication extends Application {

    public static MyApplication sInstance;

    public static Context getAppContext(){

        Context context = sInstance.getApplicationContext();

        return sInstance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }

    public static Context getContext() {
        return sInstance;
    }

}
