package com.fabriciooliveira.projectlocations;

import java.io.Serializable;

/**
 * Created by fabriciooliveira on 11/16/15.
 */
public class Place implements Serializable{

    private String name;
    private String city;
    private String category;

    public Place() {
        name = "";
        city = "";
        this.setCategory("");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        if (city.length() > 0) {
            return city;
        }
        return city;
    }

    public void setCity(String city) {
        if (city != null) {
            this.city = city.replaceAll("\\(", "").replaceAll("\\)", "");

        }
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


}
