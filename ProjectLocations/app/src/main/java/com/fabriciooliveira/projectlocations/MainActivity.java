package com.fabriciooliveira.projectlocations;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final String CLIENT_ID="1DUEZQGT1ONJZZLBC0SLV3A5LDYAHPJPRDEGZKHVEHOHJ33A";
    public static final String CLIENT_SECRET="GP1NFXQ4W1TIRSS2TVMOQ3A2S1OP3H30OOILSX23TMQKQNXT";

    public static final String latitude = "40.7463956";
    public static final String longtitude = "-73.9852992";

    private RequestQueue mRequestQueue;
    private VolleySingleton mVolleySingleton;


    private ArrayList<Place> mListPlaces = new ArrayList<>();
    private AdapterLocation mAdapter;
    private RecyclerView mRecyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_id);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new AdapterLocation(this);

        mRecyclerView.setAdapter(mAdapter);

        sendJSONRequest();

    }

    private void sendJSONRequest() {

        JSONObject jsonObject = new JSONObject();
        String url = "https://api.foursquare.com/v2/venues/search?client_id=1DUEZQGT1ONJZZLBC0SLV3A5LDYAHPJPRDEGZKHVEHOHJ33A&client_secret=GP1NFXQ4W1TIRSS2TVMOQ3A2S1OP3H30OOILSX23TMQKQNXT&v=20130815%20&ll=-22.862206,%20-43.306853";

        JsonObjectRequest requestPlaces = new JsonObjectRequest(Request.Method.GET,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mListPlaces = parseJSONListResponse(response);
                        mAdapter.setListLocation(mListPlaces);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

        });

        mRequestQueue.add(requestPlaces);
    }

    private ArrayList<Place> parseJSONListResponse(JSONObject response) {

        if (response == null && response.length() == 0) {
            return null;
        }

        try{


           JSONArray currentSquare = response.getJSONObject("response").getJSONArray("venues");
            Gson gson = new Gson();

            for (int i = 0; i < currentSquare.length(); i++){

               JSONObject object = currentSquare.getJSONObject(i);
               Place place = gson.fromJson(object.toString(), Place.class);

               mListPlaces.add(place);
            }


        }catch (Exception ex) {
            ex.printStackTrace();
        }

        return mListPlaces;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
