package com.fabriciooliveira.alurix.modelo;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class SubCardapio {

    private int id;
    private String item;
    private String conteudo;

    public SubCardapio(int id, String item, String conteudo){

        this.id = id;
        this.item = item;
        this.conteudo = conteudo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }
}
