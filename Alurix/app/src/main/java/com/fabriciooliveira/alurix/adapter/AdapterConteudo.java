package com.fabriciooliveira.alurix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fabriciooliveira.alurix.R;
import com.fabriciooliveira.alurix.modelo.Cardapio;

import java.util.List;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class AdapterConteudo extends BaseAdapter {

    private Context ctx;
    private List<Cardapio> listaCardapio;

    public AdapterConteudo(Context ctx, List<Cardapio> listaCardapio){
        this.ctx = ctx;
        this.listaCardapio = listaCardapio;
    }

    @Override
    public int getCount(){
        return listaCardapio.size();
    }

    @Override
    public Object getItem(int posicao){
        return listaCardapio.get(posicao);
    }

    @Override
    public long getItemId(int posicao){
        return listaCardapio.get(posicao).getId();
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent){
        Cardapio cardapio = listaCardapio.get(posicao);

        View view = LayoutInflater.from(ctx).inflate(R.layout.layout_lista, null);
        TextView texto = (TextView) view.findViewById(R.id.textViewConteudo);
        texto.setText(cardapio.getTipo());

        return view;
    }
}
