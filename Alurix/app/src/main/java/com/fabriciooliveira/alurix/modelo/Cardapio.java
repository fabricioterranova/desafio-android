package com.fabriciooliveira.alurix.modelo;

import java.io.Serializable;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class Cardapio implements Serializable{

    private int id;
    private String tipo;

    public Cardapio(int id, String tipo){
        this.id = id;
        this.tipo = tipo;
    }

    public Cardapio(String tipo){
        this.tipo = tipo;
    }

    public int getId(){
        return id;
    }

    public String getTipo(){
        return tipo;
    }
}
