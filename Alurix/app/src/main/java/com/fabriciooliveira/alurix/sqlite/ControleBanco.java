package com.fabriciooliveira.alurix.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.fabriciooliveira.alurix.modelo.Cardapio;

/**
 * Created by fabriciooliveira on 2/5/15.
 */
public class ControleBanco {

    private Context ctx;

    private final String NOME_BANCO = "CARDAPIO";
    private final String NOME_TABELA = "TIPO_CARDAPIO";
    private final String CAMPO_TIPO = "TIPO";

    private SQLiteDatabase bancoDados = null;

    public ControleBanco(Context ctx){
        this.ctx = ctx;
    }

    //Criar banco caso nao exista
    public void criarBancoTipo(){
        try{
            bancoDados = ctx.openOrCreateDatabase(NOME_BANCO, Context.MODE_WORLD_WRITEABLE, null);
            String sql = "CREATE TABLE IF NOT EXISTS " + NOME_TABELA + "(_ID INTEGER PRIMARY KEY AUTOINCREMENT, " + CAMPO_TIPO + " TEXT);";
            bancoDados.execSQL(sql);
        }catch(Exception exception){
            exception.printStackTrace();
        }finally{
            if(bancoDados != null){
                bancoDados.close();
            }
        }
    }

    public int inserirTipo(Cardapio cardapio){
        int retorno = 0;
        try{
            criarBancoTipo();
            bancoDados = ctx.openOrCreateDatabase(NOME_BANCO, Context.MODE_WORLD_WRITEABLE, null);
            String sql = "INSERT INTO " + NOME_TABELA + "( " + CAMPO_TIPO + ") VALUES (" + cardapio.getTipo() + ");";
        }catch(Exception exception){
            exception.printStackTrace();
            retorno = 1;
        }finally{
            if(bancoDados != null){
                bancoDados.close();
            }
        }

        return retorno;
    }
}
