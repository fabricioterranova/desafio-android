package com.fabriciooliveira.alurix;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

import com.fabriciooliveira.alurix.adapter.AdapterItemCardapio;
import com.fabriciooliveira.alurix.modelo.Cardapio;
import com.fabriciooliveira.alurix.modelo.SubCardapio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class ListaSubCardapio extends Activity {

    private ListView listaViewSub;
    private Cardapio cardapioSelecionado;
    private List<SubCardapio> listaSubCardapio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sub_itens_cardapio);

        listaViewSub = (ListView)findViewById(R.id.listViewSubCardapio);

        cardapioSelecionado = (Cardapio)getIntent().getSerializableExtra("CARDAPIO");

        preencheSubLista();

    }

    private void preencheSubLista(){
        listaSubCardapio = new ArrayList<SubCardapio>();
        SubCardapio sub1;
        SubCardapio sub2;
        SubCardapio sub3;

        if(cardapioSelecionado.getId() == 1){
            sub1 = new SubCardapio(1, "Prato Feito", "Arroz, feijao e carne");
            sub2 = new SubCardapio(2, "Macarronada", "Macarrao");
            sub3 = new SubCardapio(3, "Porçao 1", "Carne, Batata, Cebola");

        }else if(cardapioSelecionado.getId() == 2){
            sub1 = new SubCardapio(1, "Cerveja", "Cerveja");
            sub2 = new SubCardapio(2, "Caipirinha", "Pinga e Limao");
            sub3 = new SubCardapio(3, "Suco", "Leite e Fruta");

        }else if(cardapioSelecionado.getId() == 3){
            sub1 = new SubCardapio(1, "Sobremesa 1", "descricao 1");
            sub2 = new SubCardapio(2, "Sobremesa 2", "descricao 2");
            sub3 = new SubCardapio(3, "Sobremesa 3", "descricao 3");
        }else{
            sub1 = null;
            sub2 = null;
            sub3 = null;
        }

        listaSubCardapio.add(sub1);
        listaSubCardapio.add(sub2);
        listaSubCardapio.add(sub3);

        AdapterItemCardapio adapter = new AdapterItemCardapio(ListaSubCardapio.this, listaSubCardapio);
        listaViewSub.setAdapter(adapter);

    }
}
