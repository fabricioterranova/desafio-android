package com.fabriciooliveira.alurix.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.fabriciooliveira.alurix.R;
import com.fabriciooliveira.alurix.modelo.Cardapio;
import com.fabriciooliveira.alurix.modelo.SubCardapio;

import java.util.List;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class AdapterItemCardapio extends BaseAdapter {

    private Context ctx;
    private List<SubCardapio> listaSubCardapio;

    public AdapterItemCardapio(Context ctx, List<SubCardapio> listaSubCardapio){
        this.ctx = ctx;
        this.listaSubCardapio = listaSubCardapio;
    }

    @Override
    public int getCount(){
        return listaSubCardapio.size();
    }

    @Override
    public Object getItem(int posicao){
        return listaSubCardapio.get(posicao);
    }

    @Override
    public long getItemId(int posicao){
        return listaSubCardapio.get(posicao).getId();
    }

    @Override
    public View getView(int posicao, View convertView, ViewGroup parent){
        SubCardapio subCardapio = listaSubCardapio.get(posicao);

        View view = LayoutInflater.from(ctx).inflate(R.layout.layout_sub_lista, null);
        TextView item = (TextView) view.findViewById(R.id.textViewItemSub);
        TextView conteudo = (TextView) view.findViewById(R.id.textViewConteudoSub);

        item.setText(subCardapio.getItem());
        conteudo.setText(subCardapio.getConteudo());

        return view;
    }
}
