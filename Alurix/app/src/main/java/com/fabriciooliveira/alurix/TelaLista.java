package com.fabriciooliveira.alurix;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fabriciooliveira.alurix.adapter.AdapterConteudo;
import com.fabriciooliveira.alurix.modelo.Cardapio;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class TelaLista extends Activity {

    private List<Cardapio> listaCardapio;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela_lista);

        listView = (ListView)findViewById(R.id.listViewCardapio);


        preencheLista();
        clickLista();

    }

    private void preencheLista(){

        listaCardapio = new ArrayList<Cardapio>();
        Cardapio cardapio1 = new Cardapio(1, "Comida");
        Cardapio cardapio2 = new Cardapio(2, "Bebida");
        Cardapio cardapio3 = new Cardapio(3, "Sobremesa");

        listaCardapio.add(cardapio1);
        listaCardapio.add(cardapio2);
        listaCardapio.add(cardapio3);

        AdapterConteudo adapterConteudo = new AdapterConteudo(TelaLista.this, listaCardapio);
        listView.setAdapter(adapterConteudo);

    }

    private void clickLista(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cardapio cardapio = (Cardapio)parent.getAdapter().getItem(position);

                Intent intent = new Intent(TelaLista.this, ListaSubCardapio.class);
                intent.putExtra("CARDAPIO", cardapio);
                startActivity(intent);
            }
        });
    }
}
