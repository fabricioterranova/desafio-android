package com.fabriciooliveira.alurix.login;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fabriciooliveira.alurix.R;
import com.fabriciooliveira.alurix.TelaLista;


public class MainActivity extends ActionBarActivity {

    private EditText editTextNome;
    private Button buttonOK, btnChamaTela2, btnChamaLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        editTextNome = (EditText)findViewById(R.id.editText1);
//
//        buttonOK = (Button)findViewById(R.id.buttonOK);
//        btnChamaTela2 = (Button)findViewById(R.id.btnChamaTela2);
//        btnChamaLista = (Button)findViewById(R.id.btnChamaLista);
//
//        clickButton();
//
//        clickTela2();
//
//        clickChamaTelaLista();
    }

    private void clickButton(){
        buttonOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Ola " + editTextNome.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void clickTela2(){
        btnChamaTela2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this, Tela2.class);
                intent.putExtra("VALOR", 666);
               startActivityForResult(intent, 123);
            }
        });
    }

    private void clickChamaTelaLista(){
        btnChamaLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TelaLista.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 123){
            if(resultCode == 1){
               //toast("Botao 1");
            }else if(resultCode == 2){
                //toast("Botao 2");
            }
        }
    }

    private void toast(String texto){
        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
