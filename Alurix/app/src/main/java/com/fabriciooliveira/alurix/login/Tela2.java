package com.fabriciooliveira.alurix.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fabriciooliveira.alurix.R;
import com.fabriciooliveira.alurix.modelo.Cardapio;
import com.fabriciooliveira.alurix.sqlite.ControleBanco;

/**
 * Created by fabriciooliveira on 2/4/15.
 */
public class Tela2 extends Activity {

    Button btnVoltar, btnSalvar;
    EditText editTextTipo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela2);

        btnVoltar = (Button)findViewById(R.id.btnVoltar);
        btnSalvar = (Button)findViewById(R.id.btnSalvar);

        editTextTipo = (EditText)findViewById(R.id.editTextTipo);

       // int resultado = getIntent().getIntExtra("VALOR", 0);

        //Toast.makeText(this, "O valor recebido = " + resultado, Toast.LENGTH_SHORT).show();

        clickVoltar();
        clickbtnSalvar();
    }

    private void clickVoltar(){
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(1);
                finish();
            }
        });
    }

    private void clickbtnSalvar(){
        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String texto = editTextTipo.getText().toString();

                if(texto != null && !texto.equals("") ){
                    Cardapio cardario = new Cardapio(texto);

                    if(new ControleBanco(Tela2.this).inserirTipo(cardario) == 0){
                        mostraMensagem("Tipo", "Inserido com sucesso");
                        editTextTipo.setText("");
                    }else{
                        mostraMensagem("Erro", "Nao foi possivel inserir");
                    }
                }else{
                    mostraMensagem("Campo vazio", "Preencha algum tipo");
                }
            }
        });
    }

    private void mostraMensagem(String titulo, String msg){
        AlertDialog.Builder mensagem = new AlertDialog.Builder(Tela2.this);
        mensagem.setTitle(titulo);
        mensagem.setMessage(msg);
        mensagem.setNeutralButton("OK", null);
        mensagem.show();
    }


}
