package com.fabriciooliveira.ubookprova;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fabriciooliveira on 3/19/15.
 */
public class Produto implements Serializable{

    private String titulo;
    private String urlImg;
    private int qtde;
    private double valor;

    public Produto() {

    }

    public Produto(String titulo, String urlImg, int qtde, double valor) {
        this.titulo = titulo;
        this.urlImg = urlImg;
        this.qtde = qtde;
        this.valor = valor;

    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public int getQtde() {
        return qtde;
    }

    public void setQtde(int qtde) {
        this.qtde = qtde;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
