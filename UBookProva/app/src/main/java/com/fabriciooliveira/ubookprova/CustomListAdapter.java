package com.fabriciooliveira.ubookprova;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by fabriciooliveira on 3/19/15.
 */
public class CustomListAdapter extends BaseAdapter {

    private Activity activity;

    private List<Produto> produtos = new ArrayList<Produto>();

    private LayoutInflater inflater;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public CustomListAdapter(Activity activity, List<Produto> produtos) {
        this.activity = activity;
        this.produtos = produtos;
    }

    @Override
    public int getCount() {
        return produtos.size();
    }

    @Override
    public Object getItem(int position) {
        return produtos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater== null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView = inflater.inflate(R.layout.linha_lista, null);

        }

        if(imageLoader == null){
            imageLoader = AppController.getInstance().getImageLoader();
        }

        NetworkImageView imageProd = (NetworkImageView) convertView.findViewById(R.id.imgProduto);

        TextView txtTitulo = (TextView) convertView.findViewById(R.id.txtTitulo);
        TextView txtQTDE = (TextView) convertView.findViewById(R.id.txtQTDE);
        //TextView txtValor = (TextView) convertView.findViewById(R.id.txtValor);

        //Recuperando o produto atual da iteracao
        Produto prod = produtos.get(position);

        //imageProd.setImageUrl(prod.getUrlImg(), imageLoader);

        txtTitulo.setText(prod.getTitulo());
        txtQTDE.setText("Qtde Vendidos: " + prod.getUrlImg());

        return convertView;
    }

}
