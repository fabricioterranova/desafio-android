package com.fabriciooliveira.ubookprova;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String URL_SHOTS ="https://api.dribbble.com/v1/shots?access_token=04b3ee0419f6e14802cd2610ce71b7c16e5b4bc8883432cb2e01821c2dd0d7f7";



    private ListView lstProd;
    private CustomListAdapter adapter;
    private List<Produto> produtos = new ArrayList<Produto>();

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        lstProd = (ListView) findViewById(R.id.listaProd);
        adapter = new CustomListAdapter(this, produtos);

        lstProd.setAdapter(adapter);

        progressDialog = new ProgressDialog(this);

        progressDialog.setMessage("Carregando...");
        progressDialog.show();

        //getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#1b1b1b")));


        JsonArrayRequest requestProds = new JsonArrayRequest(URL_SHOTS, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                esconderDialog();

                for(int i = 0; i<jsonArray.length(); i++){
                    try{
                        JSONObject jsonObj = jsonArray.getJSONObject(i);

                        Produto produto = new Produto();
                        produto.setTitulo(jsonObj.getString("title"));
                        produto.setUrlImg(jsonObj.getString("created_at"));

                        produtos.add(produto);

                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                }

                adapter.notifyDataSetChanged();

            }
        },

                new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Erro: " + error.getMessage());
                        esconderDialog();
                    }

                });

        AppController.getInstance().addToRequestQueue(requestProds);

    }

    private void esconderDialog(){
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        esconderDialog();
    }



}
