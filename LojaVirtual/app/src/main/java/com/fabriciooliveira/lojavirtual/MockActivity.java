package com.fabriciooliveira.lojavirtual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.fabriciooliveira.lojavirtual.bo.MockBO;
import com.fabriciooliveira.lojavirtual.comum.Profissao;
import com.fabriciooliveira.lojavirtual.dominio.ValidacaoMock;
import com.fabriciooliveira.lojavirtual.dto.PessoaDTO;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/10/15.
 */
public class MockActivity extends ActionBarActivity {

    EditText edtNome;
    EditText edtEndereco;
    EditText edtCPF;
    Spinner spnProfissao;
    RadioGroup rgpSexo;
    RadioButton rbtMasc, rbtFem;
    private MockBO mockBO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_mock);

        mockBO = new MockBO(this);

        edtNome = (EditText) findViewById(R.id.edt_nome);
        edtEndereco = (EditText) findViewById(R.id.edt_endereco);
        edtCPF = (EditText) findViewById(R.id.edt_cpf);
        spnProfissao = (Spinner) findViewById(R.id.spn_profissao);
        //Sexo
        rgpSexo = (RadioGroup) findViewById(R.id.rgp_sexo);
        rbtMasc = (RadioButton) findViewById(R.id.rbt_masculino);
        rbtFem = (RadioButton) findViewById(R.id.rbt_feminino);

        List<String> valores = new ArrayList<String>();
        for(Profissao p : Profissao.values()){
            valores.add(p.getDescricao());
        }

        ArrayAdapter adapter = new ArrayAdapter(MockActivity.this, android.R.layout.simple_spinner_item, new String[]{"professor", "padeiro", "jornalista", "motorista"});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProfissao.setAdapter(adapter);


    }

    public void cadastrar(View view){
        PessoaDTO pessoaDTO = new PessoaDTO();
        pessoaDTO.setNome(edtNome.getText().toString());
        pessoaDTO.setEndereco(edtEndereco.getText().toString());
        pessoaDTO.setCpf(Long.parseLong(edtCPF.getText().toString()));
        pessoaDTO.setProfissao(spnProfissao.getSelectedItemPosition()+1);
        pessoaDTO.setSexo(rbtMasc.isChecked() ? 'M' : 'F' );

        ValidacaoMock resultado = mockBO.cadastrarPessoa(pessoaDTO);
        MensagemUtil.addMsg(this, resultado.getMensagem());

        Intent i = new Intent(this, MockListActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
          //menu.add(0, 1, 1, "Lista");
//        menu.add(0, 2, 2, "info");
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
