package com.fabriciooliveira.lojavirtual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.fabriciooliveira.lojavirtual.bo.MockBO;
import com.fabriciooliveira.lojavirtual.comum.Profissao;
import com.fabriciooliveira.lojavirtual.dto.PessoaDTO;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/10/15.
 */
public class MockEditActivity extends ActionBarActivity {

    private EditText edtNome;
    private EditText edtEndereco;
    private EditText edtCPF;
    private Spinner spnProfissao;
    private RadioGroup rgpSexo;
    private RadioButton rbtMasc, rbtFem;
    private Button btnAtualizar;
    private MockBO mockBO;
    private PessoaDTO pessoa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_mock);

        setTitle("Atualizar Pessoa");

        mockBO = new MockBO(this);

        initEdit();
        montarTelaEdicao();

    }

    private void initEdit(){
        edtNome = (EditText) findViewById(R.id.edt_nome);
        edtEndereco = (EditText) findViewById(R.id.edt_endereco);
        edtCPF = (EditText) findViewById(R.id.edt_cpf);
        spnProfissao = (Spinner) findViewById(R.id.spn_profissao);
        //Sexo
        rgpSexo = (RadioGroup) findViewById(R.id.rgp_sexo);
        rbtMasc = (RadioButton) findViewById(R.id.rbt_masculino);
        rbtFem = (RadioButton) findViewById(R.id.rbt_feminino);

        btnAtualizar = (Button) findViewById(R.id.btn_cadastrar);
        btnAtualizar.setText("Atualizar");

        btnAtualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atualizar();
            }
        });

        List<String> valores = new ArrayList<String>();
        for(Profissao p : Profissao.values()){
            valores.add(p.getDescricao());
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, new String[]{"professor", "padeiro", "jornalista", "motorista"});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProfissao.setAdapter(adapter);
    }

    private void montarTelaEdicao(){
        pessoa = (PessoaDTO) getIntent().getExtras().getSerializable("pessoa");

        edtNome.setText(pessoa.getNome());
        edtEndereco.setText(pessoa.getEndereco());
        edtCPF.setText(pessoa.getCpf().toString());
        spnProfissao.setSelection(pessoa.getProfissao() - 1);

        if(pessoa.getSexo() == 'M'){
            rbtMasc.setChecked(true);
        }else{
            rbtFem.setChecked(true);
        }
    }

    private void atualizar(){
        PessoaDTO pessoaDTO = new PessoaDTO();
        pessoaDTO.setIdPessoa(pessoa.getIdPessoa());
        pessoaDTO.setNome(edtNome.getText().toString());
        pessoaDTO.setEndereco(edtEndereco.getText().toString());
        pessoaDTO.setCpf(Long.parseLong(edtCPF.getText().toString()));
        pessoaDTO.setProfissao(spnProfissao.getSelectedItemPosition()+1);
        pessoaDTO.setSexo(rbtMasc.isChecked() ? 'M' : 'F' );

        mockBO.atualizarPessoa(pessoaDTO);

        MensagemUtil.addMsg(this, "Pessoa atualizada com sucesso!");

        Intent i = new Intent(MockEditActivity.this, MockListActivity.class);
        startActivity(i);
    }


}
