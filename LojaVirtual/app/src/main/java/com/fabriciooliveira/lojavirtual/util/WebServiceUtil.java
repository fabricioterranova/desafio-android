package com.fabriciooliveira.lojavirtual.util;

import android.util.Log;

import com.fabriciooliveira.lojavirtual.custom.model.Usuario;
import com.google.gson.Gson;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Created by fabriciooliveira on 2/15/15.
 */
public class WebServiceUtil {

    private static String URL = "http://192.168.1.4:9306/LoginWebService/services/Login?wsdl";
    private static String NAMESPACE = "http://login.devmedia.edu.br/";

    /**
     * Metodo usando SOAP
     */
    public static boolean validarLogin(String usuario, String senha){
        boolean status = false;
        SoapObject soapObject = new SoapObject(NAMESPACE, "logar");

        PropertyInfo userPI = new PropertyInfo();
        PropertyInfo passPI = new PropertyInfo();

        userPI.setName("user");

        userPI.setValue(usuario);
        userPI.setType(String.class);

        passPI.setName("pass");
        passPI.setValue(senha);
        passPI.setType(String.class);

        soapObject.addProperty(userPI);
        soapObject.addProperty(passPI);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(soapObject);

        HttpTransportSE transportSE = new HttpTransportSE(URL);
        try {
            String resultado = NAMESPACE + "logar";
            Log.i("RESULTADO", resultado);

            transportSE.call(NAMESPACE + "logar", envelope);

            SoapPrimitive resp = (SoapPrimitive) envelope.getResponse();
            status = Boolean.parseBoolean(resp.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    /**
     * Metodos para Restful
     *
     * @param login
     * @param senha
     * @return
     */
    public static Usuario validarLoginRest(String login, String senha){
        Usuario resultado = null;
        try {
            java.net.URL url = new java.net.URL("http://192.168.1.4:8080/LoginRestful/login/logar?usuario=" + login + "&senha=" + senha);
            Log.i("RESULTADO ===>>>", url.toString());

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedInputStream buffer = new BufferedInputStream(connection.getInputStream());

            String str = converterToString(buffer);

            resultado = new Gson().fromJson(str, Usuario.class);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultado;
    }

    public static int contarProdutos(){
        int  qtde = 0;
        try {
            java.net.URL url = new java.net.URL("http://192.168.1.4:8080/LoginRestful/produto/cont");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedInputStream buffer = new BufferedInputStream(connection.getInputStream());

            String str = converterToString(buffer);

            if(str != null){
                qtde = Integer.parseInt(str);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return qtde;
    }

    private static String converterToString(BufferedInputStream in){
        BufferedReader buffer = new BufferedReader(new InputStreamReader(in));
        StringBuilder builder = new StringBuilder();
        String linha = null;
        try {
            while((linha=buffer.readLine()) != null){
                builder.append(linha);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
}
