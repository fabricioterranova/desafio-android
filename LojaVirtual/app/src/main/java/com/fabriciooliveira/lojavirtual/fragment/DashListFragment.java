package com.fabriciooliveira.lojavirtual.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.fabriciooliveira.lojavirtual.R;
import com.fabriciooliveira.lojavirtual.custom.AdapterListViewCustom;
import com.fabriciooliveira.lojavirtual.custom.ItemDash;
import com.fabriciooliveira.lojavirtual.dominio.DashBoardListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashListFragment extends Fragment {


    public DashListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_dashboard_list, container, false);
        setHasOptionsMenu(true);

        ListView lstDash = (ListView)rootView.findViewById(android.R.id.list);


        List<ItemDash> listaItems = new ArrayList<ItemDash>();

        for(DashBoardListItem boardListItem : DashBoardListItem.values()){
            ItemDash itemDash = new ItemDash(boardListItem.getIdImg(), boardListItem.getTitulo());
            listaItems.add(itemDash);
        }

        AdapterListViewCustom adapter = new AdapterListViewCustom(getActivity(), R.layout.item_listview_dash, listaItems);
        lstDash.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menu.add(0,2,1,"Em quadros");
    }


}
