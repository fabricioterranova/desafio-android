package com.fabriciooliveira.lojavirtual.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fabriciooliveira.lojavirtual.dto.PessoaDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 2/11/15.
 */
public class MockOpenHelper extends SQLiteOpenHelper{

    //cria aqui pois nao deve ser recuperado antes do construtor
    //private static ResourceBundle config = ResourceBundle.getBundle(Constantes.DB_CONFIG_PROPS, Locale.getDefault());

    public MockOpenHelper(Context context){
        //Context context, String name, CursorFactory factory, int version
//        super(context, config.getString(Constantes.DB_CONFIG_NOME),
//                null, Integer.parseInt(config.getString(Constantes.DB_CONFIG_VERSAO)));
        super(context, "db_lojavirtual", null, 5);

    }
    //Ser'a chamado somente uma vez
    @Override
    public void onCreate(SQLiteDatabase db) {
//        StringBuilder sql = new StringBuilder();
//        sql.append("CREATE TABLE TB_PESSOA (");
//        sql.append(" ID_PESSOA INTEGER PRIMARY KEY AUTOINCREMENT,");
//        sql.append(" NOME TEXT NOT NULL,");
//        sql.append(" ENDERECO TEXT NOT NULL,");
//        sql.append(" CPF TEXT NOT NULL,");
//        sql.append(" PROFISSAO INT NOT NULL,");
//        sql.append(" SEXO CHAR NOT NULL)");
//
//        db.execSQL(sql.toString());
        //db.close();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);

    }

    public void cadastrar(PessoaDTO pessoaDTO){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NOME", pessoaDTO.getNome());
        values.put("ENDERECO", pessoaDTO.getEndereco());
        values.put("CPF", pessoaDTO.getCpf());
        values.put("PROFISSAO", pessoaDTO.getProfissao());
        values.put("SEXO", String.valueOf(pessoaDTO.getSexo()));

        db.insert("TB_PESSOA", null, values);

    }

    public List<PessoaDTO> listar(){
        List<PessoaDTO> lista = new ArrayList<PessoaDTO>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(true, "TB_PESSOA", null, null, null, null, null, "ID_PESSOA", null, null);

       // boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit, CancellationSignal cancellationSignal

        while(cursor.moveToNext()){
            PessoaDTO pessoaDTO = new PessoaDTO();
            pessoaDTO.setIdPessoa(cursor.getInt(0));
            pessoaDTO.setNome(cursor.getString(1));
            pessoaDTO.setEndereco(cursor.getString(2));
            pessoaDTO.setCpf(cursor.getLong(3));
            pessoaDTO.setProfissao(cursor.getInt(4));
            pessoaDTO.setSexo(cursor.getString(5).charAt(0));

            lista.add(pessoaDTO);
        }

        return lista;
    }

    public PessoaDTO consultarPessoaPorID(Integer idPessoa){
        PessoaDTO pessoaDTO = new PessoaDTO();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query("TB_PESSOA", null, "ID_PESSOA =?", new String[]{idPessoa.toString()}, null, null, "ID_PESSOA");

        while(cursor.moveToNext()){
            pessoaDTO.setIdPessoa(cursor.getInt(0));
            pessoaDTO.setNome(cursor.getString(cursor.getColumnIndex("NOME")));
            //pessoaDTO.setNome(cursor.getString(1));
            pessoaDTO.setEndereco(cursor.getString(2));
            pessoaDTO.setCpf(cursor.getLong(3));
            pessoaDTO.setProfissao(cursor.getInt(4));
            pessoaDTO.setSexo(cursor.getString(5).charAt(0));
        }

        return pessoaDTO;
    }

    public void removerPessoaPorId(Integer idPessoa){
        SQLiteDatabase db = getWritableDatabase();

        db.delete("TB_PESSOA", "ID_PESSOA=?", new String[]{idPessoa.toString()});
    }

    public void atualizarPessoaPorID(PessoaDTO pessoaDTO){
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("NOME", pessoaDTO.getNome());
        values.put("ENDERECO", pessoaDTO.getEndereco());
        values.put("CPF", pessoaDTO.getCpf());
        values.put("PROFISSAO", pessoaDTO.getProfissao());
        values.put("SEXO", String.valueOf(pessoaDTO.getSexo()));

        db.update("TB_PESSOA", values, "ID_PESSOA=?", new String[]{pessoaDTO.getIdPessoa().toString()});
    }



}
