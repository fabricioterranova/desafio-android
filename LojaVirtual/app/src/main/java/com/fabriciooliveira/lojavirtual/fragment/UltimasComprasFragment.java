package com.fabriciooliveira.lojavirtual.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.fabriciooliveira.lojavirtual.ActivityPrincipal;
import com.fabriciooliveira.lojavirtual.ListaProdutoActivity;
import com.fabriciooliveira.lojavirtual.R;
import com.fabriciooliveira.lojavirtual.custom.UTF8ParseJson;
import com.fabriciooliveira.lojavirtual.custom.adapter.ListAdapterListenerProduto;
import com.fabriciooliveira.lojavirtual.custom.adapter.UltimasComprasCustomListAdapter;
import com.fabriciooliveira.lojavirtual.custom.app.AppController;
import com.fabriciooliveira.lojavirtual.custom.model.Produto;
import com.fabriciooliveira.lojavirtual.custom.model.Venda;
import com.fabriciooliveira.lojavirtual.paypal.PayPalConfig;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;
import com.google.gson.Gson;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by fabriciooliveira on 5/31/15.
 */
public class UltimasComprasFragment extends Fragment {

    private static final String TAG = ListaProdutoActivity.class.getSimpleName();

    private ListView lstUltimasCompras;
    private UltimasComprasCustomListAdapter adapter;
    private List<Venda> vendas = new ArrayList<Venda>();

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ultimas_compras_lista, container, false);
        setHasOptionsMenu(true);

        lstUltimasCompras = (ListView) rootView.findViewById(R.id.listaUltimasCompras);
        adapter = new UltimasComprasCustomListAdapter(getActivity(), vendas);

        lstUltimasCompras.setAdapter(adapter);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Carregando...");
        progressDialog.show();

        //VALOR CONFIGURADO NO LOGIN_ACTIVITY
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int usuario = preferences.getInt("usuario_id", 0);

        executarRequestProdutos(String.valueOf(usuario));

        return rootView;

    }

    private void executarRequestProdutos(String param) {
        vendas.clear();

        //evitar espaços em branco na url
        if(param != null){
            try {
                param = URLEncoder.encode(param, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        JsonArrayRequest requestProds = new UTF8ParseJson(param != null ? PayPalConfig.URL_ULTIMAS_COMPRAS + "?idUsuario="+param : PayPalConfig.URL_ULTIMAS_COMPRAS, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray jsonArray) {
                esconderDialog();

                for(int i = 0; i<jsonArray.length(); i++){
                    try{
                        JSONObject jsonObj = jsonArray.getJSONObject(i);

                        Gson gson = new Gson();
                        Venda venda = gson.fromJson(jsonObj.toString(), Venda.class);

                        vendas.add(venda);

                    }catch(JSONException e){
                        e.printStackTrace();
                    }
                }

                adapter.notifyDataSetChanged();

            }
        },

                new Response.ErrorListener(){

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Erro: " + error.getMessage());
                        esconderDialog();
                    }

                });

        AppController.getInstance().addToRequestQueue(requestProds);
    }

    private void esconderDialog(){
        if(progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        esconderDialog();
    }

}
