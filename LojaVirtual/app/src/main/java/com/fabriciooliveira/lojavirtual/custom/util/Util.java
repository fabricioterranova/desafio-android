package com.fabriciooliveira.lojavirtual.custom.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by fabriciooliveira on 7/5/15.
 */
public class Util {

    public static String formatarMoeda(double valor){
        DecimalFormat fmt = (DecimalFormat) NumberFormat.getInstance(new Locale("pt_BR"));
        fmt.setGroupingUsed(true);
        fmt.setPositivePrefix("R$ ");
        fmt.setNegativePrefix("R$ -");
        fmt.setMinimumFractionDigits(2);
        fmt.setMaximumFractionDigits(2);

        return fmt.format(30382.50);
    }
}
