package com.fabriciooliveira.lojavirtual.custom.model;

/**
 * Created by fabriciooliveira on 4/13/15.
 */
public class Response {

    private String msg;

    private boolean erro;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isErro() {
        return erro;
    }

    public void setErro(boolean erro) {
        this.erro = erro;
    }
}
