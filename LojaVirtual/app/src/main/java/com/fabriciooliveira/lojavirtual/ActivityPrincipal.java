package com.fabriciooliveira.lojavirtual;

import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fabriciooliveira.lojavirtual.custom.adapter.NavDrawerListAdapter;
import com.fabriciooliveira.lojavirtual.custom.model.NavDrawerItem;
import com.fabriciooliveira.lojavirtual.fragment.DashListFragment;
import com.fabriciooliveira.lojavirtual.fragment.HomeFragment;
import com.fabriciooliveira.lojavirtual.fragment.ProdutosFragment;
import com.fabriciooliveira.lojavirtual.fragment.UltimasComprasFragment;
import com.fabriciooliveira.lojavirtual.util.MensagemUtil;
import com.fabriciooliveira.lojavirtual.util.WebServiceUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 5/31/15.
 */
public class ActivityPrincipal extends ActionBarActivity {

    private List<NavDrawerItem> navDrawerItens;

    private CharSequence titulo;

    private String[] navTitulosMenu;

    private TypedArray navMenuIcones;

    private DrawerLayout drawerLayout;

    private ListView lstSliderMenu;

    private NavDrawerListAdapter adapter;

    private ActionBarDrawerToggle drawerToggle;

    private ActionBar actionBar;

    private Bundle savedInstanceState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        actionBar = getSupportActionBar();

        titulo = actionBar.getTitle();

        navTitulosMenu = getResources().getStringArray(R.array.array_opcoes_menu);

        navMenuIcones = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        lstSliderMenu = (ListView) findViewById(R.id.lst_slidermenu);

        this.savedInstanceState = savedInstanceState;

        new MainAsync().execute();

    }

    public void setTitulo(CharSequence titulo){
        this.titulo = titulo;
        actionBar.setTitle(titulo);
    }


    private void exibirView(int posicao){
        Fragment fragment = null;

        switch (posicao){
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new DashListFragment();
                break;
            case 3:
                fragment = new ProdutosFragment();
                break;
            case 4:
                fragment = new UltimasComprasFragment();
                break;
        }

        if(fragment != null){
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.frame_principal, fragment).commit();
            lstSliderMenu.setItemChecked(posicao, true);
            lstSliderMenu.setSelection(posicao);
            setTitulo(navTitulosMenu[posicao]);
            drawerLayout.closeDrawer(lstSliderMenu);
        }else{
            Log.e("Activity Principal == ", "Erro ao criar um fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dash_board_list, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }

        switch (item.getItemId()){

            case R.id.versao:
                MensagemUtil.addMsg(this, "Loja Virtual DevMedia v2.1.0");
                return true;
            case 1:
                Fragment fragment = new DashListFragment();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.frame_principal, fragment).commit();
                return true;
            case 2:
                fragment = new HomeFragment();
                manager = getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.frame_principal, fragment).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawerLayout.isDrawerOpen(lstSliderMenu);
        menu.findItem(R.id.versao).setVisible(!drawerOpen);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private class SlideMenuClickListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            exibirView(position);
        }
    }

    private class MainAsync extends AsyncTask<Void, Void, String> {

        ProgressDialog progressDialog = new ProgressDialog(ActivityPrincipal.this);

        @Override
        protected void onPreExecute(){
            //super.onPreExecute();
            progressDialog.setMessage("Carregando...");
            progressDialog.show();

        }

        @Override
        protected String doInBackground(Void...params){

            return String.valueOf(WebServiceUtil.contarProdutos());
        }

        @Override
        protected void onPostExecute(String qtde){
            //super.onPostExecute(validacao);
            progressDialog.dismiss();

            navDrawerItens = new ArrayList<NavDrawerItem>();

            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[0], navMenuIcones.getResourceId(0, -1)));
            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[1], navMenuIcones.getResourceId(1, -1)));
            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[2], navMenuIcones.getResourceId(2, -1)));
            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[3], navMenuIcones.getResourceId(3, -1), qtde, true));
            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[4], navMenuIcones.getResourceId(4, -1)));
            navDrawerItens.add(new NavDrawerItem(navTitulosMenu[5], navMenuIcones.getResourceId(5, -1)));


            navMenuIcones.recycle();

            adapter = new NavDrawerListAdapter(navDrawerItens, getApplicationContext());
            lstSliderMenu.setAdapter(adapter);
            lstSliderMenu.setOnItemClickListener(new SlideMenuClickListener());

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            drawerToggle = new ActionBarDrawerToggle(ActivityPrincipal.this, drawerLayout, null,
                    R.string.app_name, R.string.app_name){
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    actionBar.setTitle(titulo);
                    invalidateOptionsMenu();

                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    actionBar.setTitle(titulo);
                    invalidateOptionsMenu();

                }
            };

            drawerLayout.setDrawerListener(drawerToggle);

            if(savedInstanceState == null){
                exibirView(0);
            }

            drawerToggle.syncState();

        }
    }

}
