package com.fabriciooliveira.lojavirtual.custom.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fabriciooliveira.lojavirtual.R;
import com.fabriciooliveira.lojavirtual.custom.model.NavDrawerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabriciooliveira on 5/31/15.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private List<NavDrawerItem> itens;

    public NavDrawerListAdapter(){

    }

    public NavDrawerListAdapter(List<NavDrawerItem> itens, Context context) {
        this.itens = itens;
        this.context = context;
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Object getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.drawer_lista_item, null);
        }

        ImageView icone = (ImageView) convertView.findViewById(R.id.icone);
        TextView txtTitulo = (TextView) convertView.findViewById(R.id.txtTituloDrawer);
        TextView txtCont = (TextView) convertView.findViewById(R.id.cont);

        icone.setImageResource(itens.get(position).getIcone());
        txtTitulo.setText(itens.get(position).getTitulo());

        if(itens.get(position).isCountVisible()){
            txtCont.setText(itens.get(position).getCont());
        }else{
            txtCont.setVisibility(View.GONE);
        }

        return convertView;
    }
}
