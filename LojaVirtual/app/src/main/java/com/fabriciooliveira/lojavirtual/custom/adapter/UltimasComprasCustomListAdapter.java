package com.fabriciooliveira.lojavirtual.custom.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.fabriciooliveira.lojavirtual.R;
import com.fabriciooliveira.lojavirtual.custom.app.AppController;
import com.fabriciooliveira.lojavirtual.custom.model.Produto;
import com.fabriciooliveira.lojavirtual.custom.model.Venda;
import com.fabriciooliveira.lojavirtual.custom.util.Util;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by fabriciooliveira on 2/19/15.
 */
public class UltimasComprasCustomListAdapter extends BaseAdapter {

    private Activity activity;

    private List<Venda> vendas = new ArrayList<Venda>();

    private LayoutInflater inflater;

    private ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public UltimasComprasCustomListAdapter(Activity activity, List<Venda> vendas) {
        this.activity = activity;
        this.vendas = vendas;
    }

    @Override
    public int getCount() {
        return vendas.size();
    }

    @Override
    public Object getItem(int position) {
        return vendas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater== null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView = inflater.inflate(R.layout.ultimas_compras_items, null);

        }

        if(imageLoader == null){
            imageLoader = AppController.getInstance().getImageLoader();
        }

        NetworkImageView imageProd = (NetworkImageView) convertView.findViewById(R.id.imgProduto);

        TextView txtTitulo = (TextView) convertView.findViewById(R.id.txtTitulo);
        TextView txtQTDE = (TextView) convertView.findViewById(R.id.txtQTDE);
        TextView txtValor = (TextView) convertView.findViewById(R.id.txtValor);

        //Recuperando o produto atual da iteracao
        final Venda venda = vendas.get(position);

        imageProd.setImageUrl(venda.getProduto().getUrlImg(), imageLoader);

        txtTitulo.setText(venda.getProduto().getTitulo());
        txtQTDE.setText("Qtde: " + String.valueOf(venda.getQtde()));
        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("pt"));
        txtValor.setText(Util.formatarMoeda(venda.getPreco()));

        return convertView;
    }
}
