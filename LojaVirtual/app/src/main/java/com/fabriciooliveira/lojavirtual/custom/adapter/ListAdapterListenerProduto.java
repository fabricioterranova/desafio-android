package com.fabriciooliveira.lojavirtual.custom.adapter;

import com.fabriciooliveira.lojavirtual.custom.model.Produto;

/**
 * Created by fabriciooliveira on 4/13/15.
 */
public interface ListAdapterListenerProduto {

    public void onAddCarrinhoPressed(Produto produto);

}
