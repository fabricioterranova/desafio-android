package com.fabriciooliveira.lojavirtual.dto;

import java.io.Serializable;

/**
 * Created by fabriciooliveira on 2/12/15.
 */
public class PessoaDTO implements Serializable {

    private Integer idPessoa;
    private String nome;
    private String endereco;
    private Long cpf;
    private Integer profissao;
    private char sexo;

    public Integer getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(Integer idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Long getCpf() {
        return cpf;
    }

    public void setCpf(Long cpf) {
        this.cpf = cpf;
    }

    public Integer getProfissao() {
        return profissao;
    }

    public void setProfissao(Integer profissao) {
        this.profissao = profissao;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }
}
