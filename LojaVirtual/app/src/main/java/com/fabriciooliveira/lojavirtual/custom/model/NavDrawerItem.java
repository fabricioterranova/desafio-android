package com.fabriciooliveira.lojavirtual.custom.model;

/**
 * Created by fabriciooliveira on 5/31/15.
 */
public class NavDrawerItem {

    private String titulo;

    private int icone;

    private String cont = "0";

    private boolean isCountVisible = false;

    public NavDrawerItem(){

    }

    public NavDrawerItem(String titulo, int icone) {
        this.titulo = titulo;
        this.icone = icone;
    }

    public NavDrawerItem(String titulo, int icone, String cont, boolean isCountVisible) {
        this.titulo = titulo;
        this.icone = icone;
        this.cont = cont;
        this.isCountVisible = isCountVisible;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIcone() {
        return icone;
    }

    public void setIcone(int icone) {
        this.icone = icone;
    }

    public String getCont() {
        return cont;
    }

    public void setCont(String cont) {
        this.cont = cont;
    }

    public boolean isCountVisible() {
        return isCountVisible;
    }

    public void setCountVisible(boolean isCountVisible) {
        this.isCountVisible = isCountVisible;
    }
}
