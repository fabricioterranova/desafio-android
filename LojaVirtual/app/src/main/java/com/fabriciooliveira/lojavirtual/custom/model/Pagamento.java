package com.fabriciooliveira.lojavirtual.custom.model;

public class Pagamento {
	
	private int id;
	
	private String pgtoPaypalId;
	
	private String estado;
	
	private double valor;
	
	private String moeda;
	
	private Usuario usuario;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPgtoPaypalId() {
		return pgtoPaypalId;
	}

	public void setPgtoPaypalId(String pgtoPaypalId) {
		this.pgtoPaypalId = pgtoPaypalId;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public String getMoeda() {
		return moeda;
	}

	public void setMoeda(String moeda) {
		this.moeda = moeda;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
