package com.fabriciooliveira.lojavirtual;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

/**
 * Created by fabriciooliveira on 2/10/15.
 */
public class DashBoardActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard);

        //String msg = getIntent().getExtras().getString("msg");
        //MensagemUtil.addMsg(DashBoardActivity.this, msg);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
          menu.add(0, 1, 1, "Lista");
//        menu.add(0, 2, 2, "info");
        //getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        switch(id){
            case 1:
                Intent intent = new Intent(DashBoardActivity.this, DashBoardListActivity.class);
                startActivity(intent);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void cadastrar(View view){
        Intent intent = new Intent(DashBoardActivity.this, MockActivity.class);
        startActivity(intent);
    }

    public void listarProdutos(View view){
        Intent intent = new Intent(this, ListaProdutoActivity.class);
        startActivity(intent);
    }
}
