package com.fabriciooliveira.lojavirtual.dominio;

import com.fabriciooliveira.lojavirtual.custom.model.Usuario;

/**
 * Created by fabriciooliveira on 2/12/15.
 */
public class ValidacaoLogin {

    private boolean valido;

    private String mensagem;

    private Usuario usuario;

    public boolean isValido() {
        return valido;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
