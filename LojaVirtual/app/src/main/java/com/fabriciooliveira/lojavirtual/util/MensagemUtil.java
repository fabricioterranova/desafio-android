package com.fabriciooliveira.lojavirtual.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by fabriciooliveira on 2/9/15.
 */
public class MensagemUtil {

    /**
     * Metodo de criaçao de mensagens rapidas
     * @param activity
     * @param msg
     *
     */
    public static void addMsg(Activity activity, String msg){
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Metodo de criacao de mensagens Dialog com botao OK
     *
     * @param activity
     * @param titulo
     * @param mensagem
     * @param icone
     */
    public static void addMsgOk(Activity activity, String titulo, String mensagem, int icone){
        AlertDialog.Builder builderDialog = new AlertDialog.Builder(activity);
        builderDialog.setTitle(titulo);
        builderDialog.setMessage(mensagem);
        builderDialog.setNeutralButton("OK", null);
        builderDialog.setIcon(icone);
        builderDialog.show();
    }

    /**
     * Metodo para criacao de uma mensagem de dialogo com opcoes sim e nao
     *
     * @param activity
     * @param titulo
     * @param mensagem
     * @param icone
     * @param listener
     */
    public static void addMsgConfirm(Activity activity, String titulo,
                                     String mensagem, int icone, DialogInterface.OnClickListener listener){
        AlertDialog.Builder builderDialog = new AlertDialog.Builder(activity);
        builderDialog.setTitle(titulo);
        builderDialog.setMessage(mensagem);
        builderDialog.setPositiveButton("Sim", listener);
        builderDialog.setNegativeButton("Nao", null);
        builderDialog.setIcon(icone);
        builderDialog.show();
    }


}
